# TP-POO

Trabajo práctico final para la cátedra "Programación orientada a objetos" de la UNL.

**Introducción**

Este proyecto es un programa el cual simula una página web que le permite a sus usuarios (profesionales y pacientes) administrar turnos relacionados a la salud.

**Ayuda rápida**

Ventana pricipal de un paciente:

-Los pacientes pueden ver el perfil del profesional seleccionado pulsando el botón "Ver perfil".

Ventana pricipal de un profesional:

-Para buscar y mostrar los datos de un paciente, ingrese el DNI del cliente y luego haga click en "Buscar paciente".

-Si desea mostrar un listado de turnos buscando por fecha o por obra social, haga click en el boton "Búsqueda avanzada".

-Para cambiar los horarios de trabajo, haga click en "Cambiar horarios", luego seleccione un día y un itervalo (primer y útlima hora). También puede agregar, abajo, qué días no trabaja.

**Testing**

Datos de inicio de sesión:

Contraseñas: asd123
