#include <wx/msgdlg.h>
#include "Hija_GUI_login.h"
#include "Hija_GUI_sign_up.h"
#include "Hija_GUI_newpass.h"
#include "Hija_main_GUI_paciente.h"
#include "Hija_main_GUI_profesional.h"
#include "Utils/string_conv.h"
#include "Utils/digit_check.h"

Hija_GUI_login::Hija_GUI_login (Server* server) 
: Base_GUI_login(nullptr), m_server(server)
{
	/*m_server->add_especialidad("Dermatologia");
	m_server->add_especialidad("Psicologia");
	m_server->add_especialidad("Infectologia");
	m_server->add_especialidad("Ginecologia");*/
}

void Hija_GUI_login::OnClick_ingresar ( wxCommandEvent& event )
{
	std::map<long, std::string> passwords = m_server->request_passwords();
	
	
	std::string dni_ingresado = wx_to_std(m_textCtrl_DNI->GetValue());
	std::string pass = wx_to_std(m_textCtrl_pass->GetValue());
	
	if (es_digito(dni_ingresado))
	{
	long dni; m_textCtrl_DNI->GetValue().ToLong(&dni);
	
	/*std::map<long, Paciente> pacientes = m_server->request_pacientes();
	Paciente *paciente = new Paciente();
	*paciente = pacientes[41268246];
	Hija_main_GUI_paciente* win_main_paciente = new Hija_main_GUI_paciente(m_server, paciente);
	win_main_paciente->Show();
	Close();*/

	/*Profesional* profesional = new Profesional; 
	*profesional = m_server->request_profesional(21268246);
	
	Hija_main_GUI_profesional* win_main_profesional = new Hija_main_GUI_profesional(m_server, profesional);
	win_main_profesional->Show();
	Close();*/
	
		if (passwords.find(dni) == passwords.end())
		{
			wxMessageBox("Usuario no registrado");
		}
		else
		{
			if (pass != passwords[dni])
				wxMessageBox(_T("Contrase\u00F1a incorrecta"));
			
			else
			{
				int index_rol = m_choice_rol->GetSelection();

				switch (index_rol)
				{
				case 0:{
					std::map<long, Paciente> pacientes = m_server->request_pacientes();

					if (pacientes.find(dni) != pacientes.end())
					{
						Paciente* paciente = new Paciente();
						*paciente = pacientes[dni];
						Hija_main_GUI_paciente* win_main_paciente = new Hija_main_GUI_paciente(m_server, paciente);
						win_main_paciente->Show();
						Close();
					}
					else
					{
						wxMessageBox("Usuario no encontrado en la base de datos. Vuelva a llenar sus datos");
					}
					break;}
				case 1:{
					std::map<long, Profesional> profesionales = m_server->request_profesionales();

					if (profesionales.find(dni) != profesionales.end())
					{
						Profesional* profesional = new Profesional();
						*profesional = profesionales[dni];
						Hija_main_GUI_profesional* win_main_profesional = new Hija_main_GUI_profesional(m_server, profesional);
						win_main_profesional->Show();
						Close();
					}
					else
					{
						wxMessageBox("Usuario no encontrado en la base de datos. Vuelva a llenar sus datos");
					}
					break;}
				}
			}
		}
	}
	else wxMessageBox(_T("DNI no v\u00E1lido"));
}
void Hija_GUI_login::OnClick_crear    ( wxCommandEvent& event )
{
	Hija_GUI_sign_up win_sign_up(this, m_server);
	win_sign_up.ShowModal();
}

void Hija_GUI_login::OnClick_cambiar_pass( wxCommandEvent& event )
{
	std::string dni_ingresado = wx_to_std(m_textCtrl_DNI->GetValue());

	if(es_digito(dni_ingresado))
	{
		std::map<long, std::string> passwords = m_server->request_passwords();
		int index_rol = m_choice_rol->GetSelection();
		long dni; m_textCtrl_DNI->GetValue().ToLong(&dni);
		std::string pass = wx_to_std(m_textCtrl_pass->GetValue());
		
		if (m_textCtrl_DNI->IsEmpty() or pass.empty()) wxMessageBox("Ingrese sus datos de inicio");
		
		else if (passwords.find(dni) == passwords.end())
		{
			wxMessageBox("Usuario no registrado");
		}
		else
		{
			if (pass != passwords[dni])
				wxMessageBox(_T("Contrase\u00F1a incorrecta"));
			
			else
			{
				int index_rol = m_choice_rol->GetSelection();
				
				Hija_GUI_newpass win_new_pass(this, m_server, dni, index_rol);
				win_new_pass.ShowModal();
				
					m_textCtrl_pass->Clear();
			}
		}
	}
	else wxMessageBox(_T("DNI no v\u00E1lido"));
}
