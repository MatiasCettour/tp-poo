#include"paciente.h"

Paciente::Paciente()
    : User("", "")
{}

Paciente::Paciente(std::string _nombre, std::string _apellido)
    : User(_nombre, _apellido)
{}

void Paciente::set_obra_social (std::string _obra_social)
{
    m_obra_social = _obra_social;
}
void Paciente::set_credencial(std::string _credencial)
{
    m_credencial = _credencial;
}

void Paciente::add_diagnostico(std::string _diagnostico)
{
    m_diagnosticos.push_back(_diagnostico);
}

void Paciente::eliminar_diagnostico(size_t _position)
{
    m_diagnosticos.erase(m_diagnosticos.begin() + _position);
}

std::string Paciente::get_obra_social() const
{
    return m_obra_social;
}

std::string Paciente::get_credencial() const
{
    return m_credencial;
}

void Paciente::remover_diagnostico(size_t i)
{
    if (i < m_diagnosticos.size()) m_diagnosticos.erase(m_diagnosticos.begin()+i);
}

std::vector<std::string> Paciente::get_diagnosticos() const
{
    return m_diagnosticos;
}

std::string Paciente::get_rol()
{
    return "Paciente";
}
