#ifndef HIJA_GUI_DIALOG_BUSQUEDA_AVANZADA_H
#define HIJA_GUI_DIALOG_BUSQUEDA_AVANZADA_H
#include "wx/msgdlg.h"
#include "Utils/string_conv.h"
#include "paciente.h"
#include "profesional.h"
#include "server.h"
#include "Bases_GUI/GUI_profesional.h"

class Hija_GUI_dialog_busqueda_avanzada : public Base_GUI_dialog_busqueda_avanzada
{
public:
	Hija_GUI_dialog_busqueda_avanzada(wxWindow* _parent, Server* _server, Profesional* _profesional);
	
	void OnClick_buscar_mes( wxCommandEvent& event );
	void OnChoice_mes_adv2( wxCommandEvent& event );
	void OnClick_buscar_dia( wxCommandEvent& event ); 
	void OnClick_buscar_obra_social( wxCommandEvent& event ); 
	
private:
	Server* m_server            = nullptr;
	Profesional* m_profesional  = nullptr;
	void refresh(const std::vector<Turno> &turnos);
};

#endif

