#ifndef HIJA_GUI_DIALOG_ESPECIALDIAD_H
#define HIJA_GUI_DIALOG_ESPECIALDIAD_H
#include "Bases_GUI/GUI_profesional.h"
#include "server.h"

class Server;
class Profesional;


class Hija_GUI_dialog_especialdiad : public Base_GUI_dialog_especialdiad
{
public:
	Hija_GUI_dialog_especialdiad (wxWindow* _parent, Server* _server, Profesional* profesional);
	
	void OnClick_aceptar_especialidad    ( wxCommandEvent& event );
		
private:
	Server* m_server           = nullptr;
	Profesional* m_profesional = nullptr;
};

#endif

