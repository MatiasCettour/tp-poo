#include <wx/msgdlg.h>
#include "Hija_GUI_sign_up.h"
#include "Utils/string_conv.h"
#include "paciente.h"
#include "profesional.h"
#include "server.h"
#include "Utils/digit_check.h"

Hija_GUI_sign_up::Hija_GUI_sign_up (wxWindow* parent, Server* server)
	: Base_GUI_sign_up(parent), m_server(server)
{
	for (int i = get_year_now() - 20; i > 1920; --i)
	{
		m_choice_year->Append(std::to_string(i));
	}
}
void Hija_GUI_sign_up::OnChoice_year ( wxCommandEvent& event )
{
	m_choice_mes->Clear();

		for (int i = 1; i <= 12; ++i)
		{
			m_choice_mes->Append(std::to_string(i));
		}
}
void Hija_GUI_sign_up::OnChoice_mes ( wxCommandEvent& event )
{
	int year_nacimiento = std::stoi(wx_to_std( m_choice_year->GetString(m_choice_year->GetSelection())));
	int mes             = std::stoi(wx_to_std( m_choice_mes->GetString(m_choice_mes->GetSelection())));
	int cantidad_dias_mes = -1;
	
	switch (mes)
	{
	case 0:
		cantidad_dias_mes = 31; break;
		
	case 1:
		
		if (year_nacimiento % 400 == 0 || (year_nacimiento % 4 == 0 && year_nacimiento % 100 != 0) )
			cantidad_dias_mes = 29;
		else
			cantidad_dias_mes = 28;
		break;
	case 2:
		cantidad_dias_mes = 31; break;
		
	case 3:
		cantidad_dias_mes = 30; break;
		
	case 4:
		cantidad_dias_mes = 31; break;
		
	case 5:
		cantidad_dias_mes = 30; break;
		
	case 6:
		cantidad_dias_mes = 31; break;
		
	case 7:
		cantidad_dias_mes = 30; break;
		
	case 8:
		cantidad_dias_mes = 30; break;
		
	case 9:
		cantidad_dias_mes = 31; break;
		
	case 10:
		cantidad_dias_mes = 30; break;
		
	case 11:
		cantidad_dias_mes = 31; break;
	}
	
	m_choice_dia->Clear();
	
	if (cantidad_dias_mes != -1)
		for (int i = 1; i <= cantidad_dias_mes; ++i)
			m_choice_dia->Append(std::to_string(i));
}

void Hija_GUI_sign_up::OnClick_crear_cuenta ( wxCommandEvent& event )
{
	std::string dni_ingresado = wx_to_std(m_textCtrl_dni->GetValue());
	
	if (es_digito(dni_ingresado))
	{
		long dni; m_textCtrl_dni->GetValue().ToLong(&dni);
		
		if (!m_server->buscar_paciente(dni) and !m_server->buscar_profesional(dni))
		{
			std::string nombre = wx_to_std(m_textCtrl_nombre->GetValue());
			std::string apellido = wx_to_std(m_textCtrl_apellido->GetValue());
			
			if (nombre.length() < 2 or apellido.length() < 2) wxMessageBox(_T("Nombre o apellido no v\u00E1lidos"));
			
			else
			{
				int dia_nacimiento, mes_nacimiento, year_nacimiento;
				if (m_choice_dia->GetSelection() > -1 and m_choice_mes->GetSelection() > -1 and m_choice_year->GetSelection() > -1)
				{
					year_nacimiento = std::stoi(wx_to_std( m_choice_year->GetString(m_choice_year->GetSelection())));
					mes_nacimiento  = std::stoi(wx_to_std( m_choice_mes->GetString(m_choice_mes->GetSelection())));
					dia_nacimiento  = std::stoi(wx_to_std( m_choice_dia->GetString(m_choice_dia->GetSelection())));
					
					int index_rol = m_choice_rol->GetSelection();
					
					std::string pass1 = wx_to_std(m_textCtrl_pass1->GetValue());
					std::string pass2 = wx_to_std(m_textCtrl_pass2->GetValue());
					
					if (pass1 != pass2) wxMessageBox(_T("Las contrase\u00F1as no coinciden"));
					
					else if (pass1.length() < 5) wxMessageBox(_T("La contrase\u00F1a debe tener al menos 5 caracteres"));
					
					else
					{
						switch (index_rol)
						{
						case 0:{
							Paciente un_paciente(nombre, apellido);
							un_paciente.set_password(pass1);
							un_paciente.set_dni(dni); 
							un_paciente.set_year_nacimiento(year_nacimiento);
							un_paciente.set_mes_nacimiento(mes_nacimiento);
							un_paciente.set_dia_nacimiento(dia_nacimiento);
							
							m_server->subir_datos(un_paciente);
							
							break;}
						case 1:
								Profesional un_profesional(nombre, apellido);
								un_profesional.set_password(pass1);
								un_profesional.set_dni(dni);
								un_profesional.set_year_nacimiento(year_nacimiento);
								un_profesional.set_mes_nacimiento(mes_nacimiento);
								un_profesional.set_dia_nacimiento(dia_nacimiento);
								
								m_server->subir_datos(un_profesional);
								break;
						}
						wxMessageBox("Usuario creado");
						EndModal(0);
					}
					
				}
				else wxMessageBox("Seleccione una fecha de nacimiento");
			}
		}
		else wxMessageBox("Usuario ya registrado");
	}
	else wxMessageBox(_T("DNI no v\u00E1lido"));
}
