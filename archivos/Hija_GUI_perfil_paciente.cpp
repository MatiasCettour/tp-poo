#include <wx/msgdlg.h>
#include "Utils/string_conv.h"
#include "paciente.h"
#include "profesional.h"
#include "server.h"
#include "Hija_GUI_perfil_paciente.h"

Hija_GUI_perfil_paciente::Hija_GUI_perfil_paciente(wxWindow *parent, Server *_server, Paciente *_paciente) 
	: Base_GUI_perfil_paciente(parent), m_server(_server), m_paciente(_paciente)
{
	m_textCtrl_nombre->ChangeValue  (std_to_wx( m_paciente->get_nombre() ));
	m_textCtrl_apellido->ChangeValue(std_to_wx( m_paciente->get_apellido() ));
	
	m_choice_year->Clear();
	int year_nacimiento = m_paciente->get_year_nacimiento();
	int mes_nacimiento = m_paciente->get_mes_nacimiento();
	int dia_nacimiento = m_paciente->get_dia_nacimiento();
	
	int aux = 0, index = 0;
	for (int i = get_year_now() - 20; i > 1920; --i)
	{
		m_choice_year->Append(std::to_string(i));
		if (i == year_nacimiento)
		{
			index = aux;
		}
		++aux;
	}
	
	m_choice_year->SetSelection(index);
	m_choice_mes->SetSelection(mes_nacimiento - 1);
	
	int cantidad_dias_mes;
		
		switch (mes_nacimiento - 1)
		{
		case 0:
			cantidad_dias_mes = 31; break;
			
		case 1:
			
			if (year_nacimiento % 400 == 0 || (year_nacimiento % 4 == 0 && year_nacimiento % 100 != 0) )
				cantidad_dias_mes = 29;
			else
				cantidad_dias_mes = 28;
			break;
		case 2:
			cantidad_dias_mes = 31; break;
			
		case 3:
			cantidad_dias_mes = 30; break;
			
		case 4:
			cantidad_dias_mes = 31; break;
			
		case 5:
			cantidad_dias_mes = 30; break;
			
		case 6:
			cantidad_dias_mes = 31; break;
			
		case 7:
			cantidad_dias_mes = 30; break;
			
		case 8:
			cantidad_dias_mes = 30; break;
			
		case 9:
			cantidad_dias_mes = 31; break;
			
		case 10:
			cantidad_dias_mes = 30; break;
			
		case 11:
			cantidad_dias_mes = 31; break;
		}
		
		if (cantidad_dias_mes != -1)
			for (int i = 1; i <= cantidad_dias_mes; ++i)
				m_choice_dia->Append(std::to_string(i));
	
		m_choice_dia->SetSelection(dia_nacimiento - 1);
		
	std::vector<std::string> obras_sociales = m_server->request_obras_sociales();
	
	for (size_t i = 0; i < obras_sociales.size(); ++i)
	{
		m_choice_obra_social->Append(obras_sociales[i]);
			
			if (obras_sociales[i] == m_paciente->get_obra_social())
				index = i;
	}
				m_choice_obra_social->SetSelection(index);
	
	std::string sexo = m_paciente->get_sexo();
	
	if 		(sexo == "Femenino" ) m_choice_sexo->Select(0);
	else if (sexo == "Masculino") m_choice_sexo->Select(1);
	else						  m_choice_sexo->Select(2);
	
	m_textCtrl_credencial->SetValue(m_paciente->get_credencial());
}

void Hija_GUI_perfil_paciente::OnClick_aceptar( wxCommandEvent& event )
{
	std::string error;
	std::string nombre = wx_to_std(m_textCtrl_nombre->GetValue());
	std::string apellido = wx_to_std(m_textCtrl_apellido->GetValue());
	
	if (nombre == "" or nombre == "-") error += "nombre ";
	else m_paciente->set_nombre(nombre);
	
	if (apellido == "" or apellido == "-") error += "apellido ";
	else m_paciente->set_apellido(apellido);
	
	switch (m_choice_sexo->GetSelection())
	{
		case 0:
			m_paciente->set_sexo("Femenino");
	break;
		case 1: m_paciente->set_sexo("Masculino");
	break;
		case 2: m_paciente->set_sexo("Otro");
	}
	
	/*m_paciente->set_fecha_nacimiento(wx_to_std( m_choice_dia->GetString (  m_choice_dia->GetSelection() )) + "/" +
									 std::to_string( m_choice_mes->GetSelection()+1 )                      + "/" +
									 wx_to_std( m_choice_year->GetString(  m_choice_year->GetSelection() )) ) ;*/
									 
	m_paciente->set_obra_social(wx_to_std( m_choice_obra_social->GetString(m_choice_obra_social->GetSelection())) );
	
	m_paciente->set_credencial(wx_to_std( m_textCtrl_credencial->GetValue()) );
	
		m_server->subir_datos(*m_paciente);
	
			if (!error.empty()) wxMessageBox("Revise: " + error);
			else EndModal(0);
}
void Hija_GUI_perfil_paciente::OnClick_cancelar( wxCommandEvent& event )
{
	EndModal(0);
}

void Hija_GUI_perfil_paciente::OnChoice_mes( wxCommandEvent& event )
{
m_choice_dia->Clear();


	int year;
	if (m_choice_year->GetSelection() != -1)
		 year = std::stoi(wx_to_std( m_choice_year->GetString(m_choice_year->GetSelection())));
	int cantidad_dias_mes = -1;
	
	switch (m_choice_mes->GetSelection())
	{
	case 0:
		cantidad_dias_mes = 31; break;
		
	case 1:
		
		if (year % 400 == 0 || (year % 4 == 0 && year % 100 != 0) )
			cantidad_dias_mes = 29;
		else
			cantidad_dias_mes = 28;
		break;
	case 2:
		cantidad_dias_mes = 31; break;
		
	case 3:
		cantidad_dias_mes = 30; break;
		
	case 4:
		cantidad_dias_mes = 31; break;
		
	case 5:
		cantidad_dias_mes = 30; break;
		
	case 6:
		cantidad_dias_mes = 31; break;
		
	case 7:
		cantidad_dias_mes = 30; break;
		
	case 8:
		cantidad_dias_mes = 30; break;
		
	case 9:
		cantidad_dias_mes = 31; break;
		
	case 10:
		cantidad_dias_mes = 30; break;
		
	case 11:
		cantidad_dias_mes = 31; break;
	}

			if (cantidad_dias_mes != -1)
				for (int i = 1; i <= cantidad_dias_mes; ++i)
					m_choice_dia->Append(std::to_string(i));
	
}
