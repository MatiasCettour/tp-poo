#include "Hija_GUI_dialog_unPaciente.h"
#include "Hija_GUI_dialog_add_diagnostico.h"
#include "Hija_GUI_dialog_unPaciente_turnos.h"
	
Hija_GUI_dialog_unPaciente::Hija_GUI_dialog_unPaciente(wxWindow *parent, Server *_server, Profesional* _profesional, Paciente *_un_paciente)
	: Base_GUI_dialog_unPaciente (parent), m_server(_server), m_profesional(_profesional), m_paciente(_un_paciente)
{
	refresh();
}

void Hija_GUI_dialog_unPaciente::OnClick_borrar_diagnostico( wxCommandEvent& event )
{
	int seleccion = m_grilla_diagnosticos->GetGridCursorRow();
	
		m_paciente->eliminar_diagnostico(seleccion);
		
			m_server->subir_datos(*m_paciente);
	
				refresh();
}
void Hija_GUI_dialog_unPaciente::OnClick_agregar_diagnostico( wxCommandEvent& event )
{
	Hija_GUI_dialog_add_diagnostico win_diag(this, m_paciente, m_server);
	win_diag.ShowModal();
	refresh();
}

void Hija_GUI_dialog_unPaciente::OnClick_ver_turnos( wxCommandEvent& event )
{
	std::vector<Turno> turnos = m_server->request_turnos(m_profesional->get_dni(), m_paciente->get_dni());
	
	Hija_GUI_dialog_unPaciente_turnos win_turnos(this, turnos, m_server);
	win_turnos.ShowModal();
}

void Hija_GUI_dialog_unPaciente::refresh()
{
	m_textCtrl_nombre_paciente->     ChangeValue (std_to_wx( m_paciente->get_nombre()));
	m_textCtrl_apellido_paciente->   ChangeValue (std_to_wx( m_paciente->get_apellido()));
	m_textCtrl_sexo_paciente->       ChangeValue (m_paciente->get_sexo());
	m_textCtrl_dia_pacietne->        ChangeValue (std::to_string(m_paciente->get_dia_nacimiento()));
	m_textCtrl_mes_paciente->        ChangeValue (std::to_string(m_paciente->get_mes_nacimiento()));
	m_textCtrl_year_paciente->       ChangeValue (std::to_string(m_paciente->get_year_nacimiento()));
	m_textCtrl_obra_social_paciente->ChangeValue (m_paciente->get_obra_social());
	m_textCtrl_credencial_paciente-> ChangeValue (m_paciente->get_credencial());

	
		//m_grilla_diagnosticos->ClearGrid();
		int filas = m_grilla_diagnosticos->GetNumberRows();
		if (filas > 0) m_grilla_diagnosticos->DeleteRows(0,filas,true);
		
		std::vector<std::string> diagnosticos = m_paciente->get_diagnosticos();
	
		m_grilla_diagnosticos->AppendRows( diagnosticos.size());
		m_grilla_diagnosticos->SetSelectionMode(wxGrid::wxGridSelectRows);
		
		for (size_t i = 0; i < diagnosticos.size(); ++i)
			m_grilla_diagnosticos->SetCellValue(i,0, std_to_wx(diagnosticos[i]));
}
