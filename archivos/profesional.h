#ifndef PROFESIONAL_H
#define PROFESIONAL_H
#include "user.h"
#include "server.h"
#include "paciente.h"
#include <string>
#include <vector>
#include <algorithm>

class Server;
class Paciente;


class Profesional : public User
{
public:
    Profesional();
    Profesional(std::string _nombre, std::string _apellido);

    void set_profesion(std::string _profesion);
    void set_matricula(std::string _num_matricula);
    void set_universidad(std::string _universidad);
    int  add_especialidad(std::string _especialidad);
    int eliminar_especialidad(size_t _position);
	int add_obra_social (std::string _obra_social);
	int eliminar_obra_social(size_t _position);

    void add_horario   (int _dia_semana, std::vector<int> _horas);
    bool check_horario (int _numero_dia, int _numero_mes);
    void add_dia_no_laboral (int _numero_dia, int _numero_mes);
    void eliminar_dia_no_laboral (size_t _position);

    std::string get_profesion() const;
    std::string get_matricula() const;
    std::string get_universiad() const;
    std::vector<std::string> get_especialidades() const;
    std::vector<std::string> get_obras_sociales() const;
    std::string get_rol() override;

    std::vector<std::vector<int>> get_horarios();
	std::vector<int> get_horarios(int index_semana);
    std::vector<Dia_libre> get_dias_no_laborales() const;

private:
    std::string m_profesion;
    std::string m_matricula;
    std::string m_universidad;
    std::vector<std::string> m_especialidades, m_obras_sociales;

    std::vector<std::vector<int>> m_horarios;
    std::vector<Dia_libre> m_dias_no_laborales;
};

#endif
