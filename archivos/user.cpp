#include <string>
#include "user.h"

User::User(std::string _nombre, std::string _apellido)
: m_nombre(_nombre), m_apellido(_apellido)
{}

void User::set_nombre(std::string _nombre)
{
    m_nombre = _nombre;
}
void User::set_apellido(std::string _apellido)
{
    m_apellido = _apellido;
}

int User::set_password(std::string _password)
{
    if (_password.length() > 32) return -1;

    else m_password = _password;
return 0;
}

int User::set_dni(long _dni) 
{
    if (_dni < 0) return -1;

    else m_dni = _dni;
	
return 0;
}

void User::set_dia_nacimiento (int _dia)
{
    m_dia_nacimiento = _dia;
}
void User::set_mes_nacimiento (int _mes)
{
    m_mes_nacimiento = _mes;
}
void User::set_year_nacimiento(int _year)
{
    m_year_nacimiento = _year;
}

void User::set_sexo(std::string _sexo)
{
    m_sexo = _sexo;
}

std::string User::get_nombre() const
{
    return m_nombre;
}

std::string User::get_apellido() const
{
    return m_apellido;    
}

long User::get_dni() const
{
    return m_dni;
}

std::string User::get_password() const
{
    if (m_password.empty()) return "null";

    else return m_password;
}

int User::get_dia_nacimiento() const
{
    return m_dia_nacimiento;
}
int User::get_mes_nacimiento() const
{
    return m_mes_nacimiento;
}
int User::get_year_nacimiento() const
{
    return m_year_nacimiento;
}

int User::get_edad() const
{
        //funcion get_fecha()

    return 2020 - m_year_nacimiento;
}

std::string User::get_sexo() const
{
    return m_sexo;
}
