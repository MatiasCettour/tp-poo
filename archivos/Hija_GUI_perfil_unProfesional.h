#ifndef HIJA_GUI_PERFIL_UNPROFESIONAL_H
#define HIJA_GUI_PERFIL_UNPROFESIONAL_H
#include "wx/msgdlg.h"
//#include "Utils/string_conv.h"
#include "profesional.h"
#include "Bases_GUI/GUI_paciente.h"

class Hija_GUI_perfil_unProfesional : public Base_GUI_perfil_unProfesional 
{
public:
	Hija_GUI_perfil_unProfesional(wxWindow *parent, Profesional* _profesional);
private:
	Profesional *m_profesional = nullptr;
};

#endif

