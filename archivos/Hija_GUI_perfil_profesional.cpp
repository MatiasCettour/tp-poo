#include <wx/msgdlg.h>
#include "Utils/string_conv.h"
#include "Utils/fecha.h"
#include "Hija_GUI_perfil_profesional.h"
#include "Hija_GUI_dialog_especialdiad.h"
#include "Hija_GUI_dialog_obra_social.h"

Hija_GUI_perfil_profesional::Hija_GUI_perfil_profesional(wxWindow *parent, Server *_server, Profesional *_profesional) 
	: Base_GUI_perfil_profesional(parent), m_server(_server), m_profesional(_profesional)
{
	refresh();
}

void Hija_GUI_perfil_profesional::Onbutton_eliminar_obra_social( wxCommandEvent& event )
{
	int seleccion = m_choice_obras_sociales->GetSelection();
	
	if(seleccion > 0) 
	{
		m_profesional->eliminar_obra_social( seleccion );
		wxMessageBox("Obra social eliminada");
			refresh();
	}
	else wxMessageBox("No hay una obra social seleccionada");
}
void Hija_GUI_perfil_profesional::Onbutton_agregar_obra_social( wxCommandEvent& event )
{
	Hija_GUI_dialog_obra_social win_obras_sociales(this, m_server, m_profesional);
	win_obras_sociales.ShowModal();
	
		refresh();
}

void Hija_GUI_perfil_profesional::OnChoice_eliminar_especialidad( wxCommandEvent& event )
{
	int seleccion = m_choice_especialdiades->GetSelection();
	
	if (seleccion > 0 and m_profesional->eliminar_especialidad( seleccion ) == 0) 
	{
		wxMessageBox("Especialidad eliminada");
			refresh();
	}
	else wxMessageBox("No hay una especialidad seleccionada");
}
void Hija_GUI_perfil_profesional::OnChoice_agregar_especialidad( wxCommandEvent& event )
{
	Hija_GUI_dialog_especialdiad win_especialidades(this, m_server, m_profesional);
	win_especialidades.ShowModal();
	
	refresh();
}

void Hija_GUI_perfil_profesional::OnClick_aceptar_perfil ( wxCommandEvent& event )
{
	std::string error;
	std::string nombre = wx_to_std(m_textCtrl_nombre->GetValue());
	std::string apellido = wx_to_std(m_textCtrl_apellido->GetValue());

	if (nombre == "" or nombre == "-") error += "nombre ";
	else m_profesional->set_nombre(nombre);
	
	if (apellido == "" or apellido == "-") error += "apellido ";
	else m_profesional->set_apellido(apellido);

			if (!error.empty()) wxMessageBox("Revise: " + error);
			else EndModal(0);
	
	switch (m_choice_sexo->GetSelection())
	{
	case 0: m_profesional->set_sexo("Femenino"); break;
	case 1: m_profesional->set_sexo("Masculino"); break;
	case 2: m_profesional->set_sexo("Otro"); break;
	}
			
	std::string matricula   = wx_to_std(m_textCtrl_matricula->GetValue());
	std::string universidad = wx_to_std(m_textCtrl_universidad->GetValue());
	
	m_profesional->set_matricula(matricula);
	m_profesional->set_universidad(universidad);
}

void Hija_GUI_perfil_profesional::refresh()
{
	m_textCtrl_nombre->ChangeValue  (std_to_wx( m_profesional->get_nombre() ));
	m_textCtrl_apellido->ChangeValue(std_to_wx( m_profesional->get_apellido() ));
	
	m_choice_year->Clear();
	int year_nacimiento = m_profesional->get_year_nacimiento();
	int mes_nacimiento  = m_profesional->get_mes_nacimiento();
	int dia_nacimiento  = m_profesional->get_dia_nacimiento();
	
	int aux = 0, index = 0;
	for (int i = get_year_now() - 20; i > 1920; --i)
	{
		m_choice_year->Append(std::to_string(i));
		if (i == year_nacimiento)
		{
			index = aux;
		}
		++aux;
	}
	
	m_choice_year->SetSelection(index);
	m_choice_mes->SetSelection(mes_nacimiento - 1);
	
	int cantidad_dias_mes;
	
	switch (mes_nacimiento - 1)
	{
	case 0:
		cantidad_dias_mes = 31; break;
		
	case 1:
		
		if (year_nacimiento % 400 == 0 || (year_nacimiento % 4 == 0 && year_nacimiento % 100 != 0) )
			cantidad_dias_mes = 29;
		else
			cantidad_dias_mes = 28;
		break;
	case 2:
		cantidad_dias_mes = 31; break;
		
	case 3:
		cantidad_dias_mes = 30; break;
		
	case 4:
		cantidad_dias_mes = 31; break;
		
	case 5:
		cantidad_dias_mes = 30; break;
		
	case 6:
		cantidad_dias_mes = 31; break;
		
	case 7:
		cantidad_dias_mes = 30; break;
		
	case 8:
		cantidad_dias_mes = 30; break;
		
	case 9:
		cantidad_dias_mes = 31; break;
		
	case 10:
		cantidad_dias_mes = 30; break;
		
	case 11:
		cantidad_dias_mes = 31; break;
	}
	
	m_choice_dia->Clear();
	
	if (cantidad_dias_mes != -1)
		for (int i = 1; i <= cantidad_dias_mes; ++i)
			m_choice_dia->Append(std::to_string(i));
	
	m_choice_dia->SetSelection(dia_nacimiento - 1);
	
	m_choice_obras_sociales->Clear();
	std::vector<std::string> vstr = m_profesional->get_obras_sociales();
	for (auto &x :vstr )
	{
		m_choice_obras_sociales->Append(x);
	}
	m_choice_obras_sociales->SetSelection(0);
	
	std::string sexo = m_profesional->get_sexo();
	
	if 		(sexo == "Femenino" ) m_choice_sexo->Select(0);
	else if (sexo == "Masculino") m_choice_sexo->Select(1);
	else						  m_choice_sexo->Select(2);
	
	m_textCtrl_universidad->SetValue (std_to_wx( m_profesional->get_universiad()));
	
	m_textCtrl_matricula  ->SetValue (m_profesional->get_matricula());
	
	m_choice_especialdiades->Clear();
	vstr = m_profesional->get_especialidades();
	for (auto &x : vstr)
	{
		m_choice_especialdiades->Append(x);
	}
	m_choice_especialdiades->SetSelection(0);
}
