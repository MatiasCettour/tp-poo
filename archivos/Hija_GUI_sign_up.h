#ifndef HIJA_SIGN_UP_H
#define HIJA_SIGN_UP_H
#include "Bases_GUI/GUI_login.h"
#include "server.h"

class Hija_GUI_sign_up : public Base_GUI_sign_up 
{
public:
	Hija_GUI_sign_up(wxWindow* parent, Server* server);
	
	void OnChoice_year        ( wxCommandEvent& event );
	void OnChoice_mes         ( wxCommandEvent& event );
	void OnClick_crear_cuenta ( wxCommandEvent& event );
	
private:
	Server* m_server = nullptr;
};

#endif

