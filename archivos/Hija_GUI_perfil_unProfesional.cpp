#include "Hija_GUI_perfil_unProfesional.h"
#include "Utils/string_conv.h"

Hija_GUI_perfil_unProfesional::Hija_GUI_perfil_unProfesional(wxWindow *parent, Profesional* _profesional)
	: Base_GUI_perfil_unProfesional (parent), m_profesional(_profesional)
{
	m_textCtrl_nombre->ChangeValue  (std_to_wx( m_profesional->get_nombre()));
	m_textCtrl_apellido->ChangeValue(std_to_wx( m_profesional->get_apellido()));
	
	m_textCtrl_sexo->ChangeValue (m_profesional->get_sexo());
	
	m_textCtrl_year_nacimiento-> ChangeValue (std::to_string(m_profesional->get_year_nacimiento()));
	m_textCtrl_mes_nacimiento -> ChangeValue (std::to_string(m_profesional->get_mes_nacimiento()));
	m_textCtrl_dia_nacimiento -> ChangeValue (std::to_string(m_profesional->get_dia_nacimiento()));

	std::vector<std::string> vstr = m_profesional->get_obras_sociales();
	for (auto &x :vstr )
	{
		m_choice_obras_sociales->Append(x);
	}
	m_choice_obras_sociales->SetSelection(0);
	
	m_textCtrl_universidad->SetValue (std_to_wx( m_profesional->get_universiad()));
	
	m_textCtrl_matricula  ->SetValue (std_to_wx( m_profesional->get_matricula()));
	
	m_choice_especialdiades->Clear();
	vstr = m_profesional->get_especialidades();
	for (auto &x : vstr)
	{
		m_choice_especialdiades->Append(x);
	}
	m_choice_especialdiades->SetSelection(0);
}

