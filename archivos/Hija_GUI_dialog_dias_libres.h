#ifndef HIJA_GUI_DIALOG_DIAS_LIBRES_H
#define HIJA_GUI_DIALOG_DIAS_LIBRES_H
#include "Bases_GUI/GUI_profesional.h"
#include "server.h"

class Hija_GUI_dialog_dias_libres : public Base_GUI_dialog_dias_libres 
{
public:
	Hija_GUI_dialog_dias_libres(wxWindow *parent, Server* server, Profesional* profesional);
	
	void OnClick_add_free_dia( wxCommandEvent& event );
	void OnChoice_mes( wxCommandEvent& event );
		
private:
	Server* m_server            = nullptr;
	Profesional* m_profesional  = nullptr;
};

#endif

