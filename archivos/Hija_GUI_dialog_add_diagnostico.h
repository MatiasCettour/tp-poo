#ifndef HiJA_GUI_DIALOG_ADD_DIAGNOSTICO_H
#define HiJA_GUI_DIALOG_ADD_DIAGNOSTICO_H
#include "Bases_GUI/GUI_profesional.h"
#include "paciente.h"

class Server;
class Paciente;

class Hija_GUI_dialog_add_diagnostico : public Base_GUI_dialog_add_diagnostico
{
public:
	Hija_GUI_dialog_add_diagnostico(wxWindow* _parent, Paciente* _paciente, Server* _server);
	
	void OnClick_aceptar_diagnostico( wxCommandEvent& event );
	
private:
	Paciente* m_paciente = nullptr;
	Server* m_server     = nullptr;
};

#endif
