#ifndef HIJA_GUI_PACIENTE_H
#define HIJA_GUI_PACIENTE_H
#include "Bases_GUI/GUI_paciente.h"

class Server;
class Paciente;

class Hija_main_GUI_paciente : public Base_main_GUI_paciente 
{
public:
	Hija_main_GUI_paciente( Server* server, Paciente *paciente );
	
	void OnChoice_especialidades  		( wxCommandEvent& event );
	void OnChoice_profesionales   		( wxCommandEvent& event );
	void OnChoice_year			  		( wxCommandEvent& event );
	void OnChoice_mes             		( wxCommandEvent& event );
	void OnChoice_dia             		( wxCommandEvent& event );
	void OnClick_ver_perfil_profesional ( wxCommandEvent& event );
	void OnClick_reservar               ( wxCommandEvent& event );
	void OnClick_ver_turnos             ( wxCommandEvent& event );
	void OnClick_configurar_perfil      ( wxCommandEvent& event );
	void OnClick_cerrar_sesion          ( wxCommandEvent& event );
	
	~Hija_main_GUI_paciente();
	
private:
	Server*  	 m_server      = nullptr;
	Paciente*	 m_paciente    = nullptr;
	Profesional* m_profesional = nullptr;
	std::map<std::string, std::vector<long>> m_especialidades_etiquetadas;
	std::map<long, Profesional>              m_profesionales;
	std::vector<Turno>                       m_turnos;
};

#endif
