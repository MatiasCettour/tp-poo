#ifndef PACIENTE_H
#define PACIENTE_H
#include "user.h"
#include "server.h"
#include <string>
#include <vector>
#include <fstream>
#include <sstream>

class Server;

class Paciente : public User
{
public:
    Paciente();
    Paciente(std::string _nombre, std::string _apellido);
    
    void set_obra_social (std::string _obra_social);
    void set_credencial  (std::string _credencial);
    void add_diagnostico (std::string _diagnostico);
    void eliminar_diagnostico (size_t _position);

    std::string get_obra_social() const;
    std::string get_credencial() const;
    void remover_diagnostico (size_t i);
    std::vector<std::string> get_diagnosticos() const;
    std::string get_rol() override;

private:
    std::string m_password;
    std::string m_obra_social = "-", m_credencial = "-";
    std::vector<std::string> m_diagnosticos;
};

#endif
