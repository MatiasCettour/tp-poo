#ifndef HIJA_GUI_NEWPASS_H
#define HIJA_GUI_NEWPASS_H
#include "Bases_GUI/GUI_login.h"
#include "server.h"

class Hija_GUI_newpass : public Base_GUI_newpass
{
public:
	Hija_GUI_newpass(wxWindow* _parent, Server* _server, long _dni, int _index_rol);
	
	void OnClick_aceptar_newpass ( wxCommandEvent& event );
	void OnClick_cancelar_newpass( wxCommandEvent& event );
	
private:
	Server* m_server  = nullptr;
	int m_dni;
	int m_index_rol;
};

#endif

