#include "profesional.h"

Profesional::Profesional()
    : User("", "")
{
    m_horarios.resize(7);
}

Profesional::Profesional(std::string _nombre, std::string _apellido) 
: User (_nombre, _apellido)
{
	m_horarios.resize(7);
}

void Profesional::set_profesion(std::string _profesion) 
{
    m_profesion = _profesion;
}

void Profesional::set_matricula(std::string _num_matricula)
{
    m_matricula = _num_matricula;    
}

void Profesional::set_universidad(std::string _universidad)
{
    m_universidad = _universidad;    
}

int Profesional::add_especialidad(std::string _especialidad)
{
	if (find(m_especialidades.begin(), m_especialidades.end(), _especialidad) == m_especialidades.end())
	{	
		m_especialidades.push_back(_especialidad);
	return 0;
	}
	else return -1;
}
int Profesional::eliminar_especialidad(size_t _position)
{
    if(_position < m_especialidades.size())
	{
		m_especialidades.erase(m_especialidades.begin() + _position);
			return 0;
	}
	else return -1;
}

int Profesional::add_obra_social (std::string _obra_social)
{
	if (find(m_obras_sociales.begin(), m_obras_sociales.end(), _obra_social) == m_obras_sociales.end())
	{	
		m_obras_sociales.push_back(_obra_social);
		return 0;
	}
	else return -1;
}
int Profesional::eliminar_obra_social(size_t _position)
{
	if(_position < m_especialidades.size())
	{
		m_obras_sociales.erase(m_obras_sociales.begin() + _position);
		return 0;
	}
	else return -1;
}


void Profesional::add_horario(int _dia_semana, std::vector<int> _horas)
{
    if (_dia_semana > -1 and _dia_semana < 7)
    {
        m_horarios[_dia_semana] = _horas;
    }
}

bool Profesional::check_horario(int _numero_dia, int _numero_mes)
{
    Dia_libre new_dia = {_numero_dia, _numero_mes};
    for (auto &un_dia : m_dias_no_laborales)
    {
        if (un_dia.dia == new_dia.dia and un_dia.mes == new_dia.mes)
            return false;
    }
return true;   
}

void Profesional::add_dia_no_laboral(int _numero_dia, int _numero_mes)
{
   Dia_libre dia = {_numero_dia, _numero_mes};

    m_dias_no_laborales.push_back(dia);
}

void Profesional::eliminar_dia_no_laboral(size_t _position)
{
    m_dias_no_laborales.erase(m_dias_no_laborales.begin() + _position);
}

std::string Profesional::get_profesion() const
{
    return m_profesion;    
}

std::string Profesional::get_matricula() const
{
    return m_matricula;
}

std::string Profesional::get_universiad() const
{
    return m_universidad;    
}

std::vector<std::string> Profesional::get_especialidades() const
{
    return m_especialidades;
}

std::vector<std::string> Profesional::get_obras_sociales() const
{
    return m_obras_sociales;
}

std::string Profesional::get_rol()
{
    return "Profesional";
}

std::vector<std::vector<int>> Profesional::get_horarios()
{
    return m_horarios;
}

std::vector<int> Profesional::get_horarios(int index_semana)
{
	return m_horarios[index_semana];
}

std::vector<Dia_libre> Profesional::get_dias_no_laborales() const
{
    return m_dias_no_laborales;
}
