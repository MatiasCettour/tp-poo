#ifndef FECHA_H
#define FECHA_H
#include <ctime>

inline int get_year_now()
{ 
	time_t now = time(0);
	tm *ltm = localtime(&now);
	
return 1900 + ltm->tm_year;
}

inline int get_mes_now()
{ 
	time_t now = time(0);
	tm *ltm = localtime(&now);
	
return 1 + ltm->tm_mon;
}

inline int get_dia_now()
{ 
	time_t now = time(0);
	tm *ltm = localtime(&now);
	
return ltm->tm_mday;
}

inline int get_hora_now()
{ 
	time_t now = time(0);
	tm *ltm = localtime(&now);
	
return 5+ltm->tm_hour;
}

inline int get_minutos_now()
{ 
	time_t now = time(0);
	tm *ltm = localtime(&now);
	
	return 30+ltm->tm_min;
}

inline int get_segundos_now()
{ 
	time_t now = time(0);
	tm *ltm = localtime(&now);
	
	return ltm->tm_sec;
}

#endif
