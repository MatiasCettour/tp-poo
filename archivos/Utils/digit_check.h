#ifndef DIGIT_CHECK_H
#define DIGIT_CHECK_H

inline bool es_digito(std::string _string)
{
	//char aux;
	for (size_t i = 0; i < _string.length(); ++i)
	{
		//aux = _string[i].c_str;
		
		if (std::isdigit(static_cast<unsigned char>(_string[i])) == 0) //Non-zero value if the character is a numeric character, zero otherwise. 
		{
			return false;
		}
	}
return true;
}
#endif
