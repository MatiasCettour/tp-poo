#include "Hija_GUI_dialog_unPaciente_turnos.h"
#include "Utils/string_conv.h"

Hija_GUI_dialog_unPaciente_turnos::Hija_GUI_dialog_unPaciente_turnos(wxWindow* _parent, const std::vector<Turno> _turnos, Server* _server) 
	: Base_GUI_dialog_unPaciente_turnos(_parent)
{
		m_grilla_turnos_paciente->AppendRows( _turnos.size());
		m_grilla_turnos_paciente->SetSelectionMode(wxGrid::wxGridSelectRows);
		
		for (size_t i = 0; i < _turnos.size(); ++i)
			
			if (_turnos[i].confirmado == true)
			{
				m_grilla_turnos_paciente->SetCellValue(i, 0, "[CONFIRMADO] "+ std_to_wx(_server->request_paciente(_turnos[i].dni_paciente).get_nombre()) +" "+ std_to_wx(_server->request_paciente(_turnos[i].dni_paciente).get_apellido()) +". DNI: "+ std::to_string(_turnos[i].dni_paciente)
											  +". Hora: " + std::to_string(_turnos[i].hora) +". Dia: "+ std::to_string(_turnos[i].dia) + ". Mes: " + std::to_string(_turnos[i].mes) + ". Year: "+ std::to_string(_turnos[i].year));
			}
			
			else if (_turnos[i].cancelado == false and _turnos[i].expirado == false)
			{
				m_grilla_turnos_paciente->SetCellValue(i, 0, std_to_wx(_server->request_paciente(_turnos[i].dni_paciente).get_nombre()) +" "+ std_to_wx(_server->request_paciente(_turnos[i].dni_paciente).get_apellido()) +". DNI: "+ std::to_string(_turnos[i].dni_paciente)
											  +". Hora: " + std::to_string(_turnos[i].hora) +". Dia: "+ std::to_string(_turnos[i].dia) + ". Mes: " + std::to_string(_turnos[i].mes) + ". Year: "+ std::to_string(_turnos[i].year));
			}

			else
			{
				m_grilla_turnos_paciente->SetCellValue(i, 0, "[EXPIRADO] "+ std_to_wx(_server->request_paciente(_turnos[i].dni_paciente).get_nombre()) +" "+ std_to_wx(_server->request_paciente(_turnos[i].dni_paciente).get_apellido()) +". DNI: "+ std::to_string(_turnos[i].dni_paciente)
											  +". Hora: " + std::to_string(_turnos[i].hora) +". Dia: "+ std::to_string(_turnos[i].dia) + ". Mes: " + std::to_string(_turnos[i].mes) + ". Year: "+ std::to_string(_turnos[i].year));
			}
}

