#ifndef APPLICATION_H
#define APPLICATION_H

#include <wx/app.h>
#include "server.h"

class Application : public wxApp 
{
public:
	virtual bool OnInit();
private:
	Server m_server;
};

#endif
