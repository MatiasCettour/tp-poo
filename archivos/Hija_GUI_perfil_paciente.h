#ifndef HIJA_PERFIL_PACIENTE_H
#define HIJA_PERFIL_PACIENTE_H
#include "Bases_GUI/GUI_paciente.h"
#include "server.h"

class Server;
class Paciente;

class Hija_GUI_perfil_paciente : public Base_GUI_perfil_paciente 
{
public:
	Hija_GUI_perfil_paciente (wxWindow *parent, Server *_server, Paciente *_paciente);
	
	void OnClick_aceptar ( wxCommandEvent& event );
	void OnClick_cancelar( wxCommandEvent& event );
	void OnChoice_mes   ( wxCommandEvent& event );
private:
	Server   *m_server   = nullptr;
	Paciente *m_paciente = nullptr;
};

#endif

