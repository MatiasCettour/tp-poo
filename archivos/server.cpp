#include "server.h"

Server::Server()
{
    leer_datos();
	
        Id_Pass aux;
        
        std::ifstream file_passwords_in(m_file_id_pass, std::ios::binary | std::ios::ate);

        int tam_bytes = file_passwords_in.tellg();
        int n_elementos = tam_bytes / sizeof(aux);
        
        if (n_elementos > 0 and file_passwords_in.is_open())
        {
            file_passwords_in.seekg(0);
            for (int i = 0; i < n_elementos; ++i)
            {
                file_passwords_in.read(reinterpret_cast<char*>(&aux), sizeof(aux));
                m_id_pass[aux.id] = std::string(aux.password);

                    auto it = m_pacientes.find(aux.id);
                    if (it == m_pacientes.end())
                    {
                        auto it2 = m_profesionales.find(aux.id);
                        if (it2 != m_profesionales.end())
                        {
                            std::string str = aux.password;
                            it2->second.set_password(str);
                        }
                    }
                    else
                    {
                        std::string str = aux.password;
                        it->second.set_password(str);
                    }
            }
        }
}

int Server::leer_datos()
{
    std::ifstream file_datos_in(m_file_datos);
    std::string linea;

    while (getline(file_datos_in, linea))
    {
        if (linea.find("#", 0) != std::string::npos) continue;

        if (linea.find("[Paciente]") != std::string::npos)
        {
            Paciente un_paciente;
            bool fin = false;

            while (getline(file_datos_in, linea) and fin == false)
            {
                if (linea.find("#", 0) != std::string::npos)
                    continue;

                if (linea.find("END") != std::string::npos)
                    fin = true;

                else if (linea.find("dni") != std::string::npos)
                {
                    size_t cursor = linea.find("=");

                    if (cursor == std::string::npos)
                    {
                        fin = true;
                        continue;
                    }
                    else
                    {
                        cursor = set_cursor(cursor, linea);
                        un_paciente.set_dni(std::stol(linea.substr(cursor)));
                    }
                }
                else if (linea.find("nombre") != std::string::npos)
                {
                    size_t cursor = linea.find("=");

                    if (cursor == std::string::npos)
                    {
                        fin = true;
                        continue;
                    }
                    else
                    {
                        cursor = set_cursor(cursor, linea);
                        un_paciente.set_nombre(linea.substr(cursor));
                    }
                }
                else if (linea.find("apellido") != std::string::npos)
                {
                    size_t cursor = linea.find("=");

                    if (cursor == std::string::npos)
                    {
                        fin = true;
                        continue;
                    }
                    else
                    {
                        cursor = set_cursor(cursor, linea);
                        un_paciente.set_apellido(linea.substr(cursor));
                    }
                }
                else if (linea.find("birth") != std::string::npos)
                {
                    size_t cursor = linea.find("=");
                    if (linea.find("/") == std::string::npos or cursor == std::string::npos)
                    {
                        fin = true;
                        continue;
                    }
                    else
                    {
                        cursor = set_cursor(cursor, linea);
                        un_paciente.set_dia_nacimiento (std::stoi(linea.substr(cursor, linea.find("/") - cursor)));
                        cursor = linea.find("/") + 1;
                        un_paciente.set_mes_nacimiento (std::stoi(linea.substr(cursor, linea.find("/") - cursor)));
                        cursor = linea.find("/", cursor) + 1;
                        un_paciente.set_year_nacimiento(std::stoi(linea.substr(cursor)));
                    }
                }
                else if (linea.find("sexo") != std::string::npos)
                {
                    size_t cursor = linea.find("=");
                    if (cursor == std::string::npos)
                        continue;
                    else
                    {
                        cursor = set_cursor(cursor, linea);
                        un_paciente.set_sexo(linea.substr(cursor));
                    }
                }
                else if (linea.find("obra social") != std::string::npos)
                {
                    size_t cursor = linea.find("=");
                    if (cursor == std::string::npos)
                        continue;
                    else
                    {
                        cursor = set_cursor(cursor, linea);
                        un_paciente.set_obra_social(linea.substr(cursor));
                    }
                }
                else if (linea.find("credencial") != std::string::npos)
                {
                    size_t cursor = linea.find("=");
                    if (cursor == std::string::npos)
                        continue;
                    else
                    {
                        cursor = set_cursor(cursor, linea);
                        un_paciente.set_credencial(linea.substr(cursor));
                    }
                }
                else if (linea.find("diagnosticos") != std::string::npos)
                {
					bool end = false;
					
					while (end == false and getline(file_datos_in, linea))
					{
						if (linea.find("}") != std::string::npos)
							end = true;
						
						else un_paciente.add_diagnostico (linea.substr(0));
					}
                }
            }
            m_pacientes[un_paciente.get_dni()] = un_paciente;
        }
		
        else if(linea.find("[Profesional]") != std::string::npos)
        {
            Profesional un_profesional;
            bool fin           = false;
            bool usario_valido = true;

            while (getline(file_datos_in, linea) and fin == false)
            {
                if (linea.find("END") != std::string::npos)
                    fin = true;

                else if (linea.find("dni") != std::string::npos)
                {
                    size_t cursor = linea.find("=");

                    if (cursor == std::string::npos)
                        fin = true;
                    else
                    {
                        cursor = set_cursor(cursor, linea);
                        un_profesional.set_dni(std::stol(linea.substr(cursor)));
                    }
                }
                else if (linea.find("nombre") != std::string::npos)
                {
                    size_t cursor = linea.find("=");

                    if (cursor == std::string::npos)
                        fin = true;
                    else
                    {
                        cursor = set_cursor(cursor, linea);
                        un_profesional.set_nombre(linea.substr(cursor));
                    }
                }
                else if (linea.find("apellido") != std::string::npos)
                {
                    size_t cursor = linea.find("=");

                    if (cursor == std::string::npos)
                        fin = true;
                    else
                    {
                        cursor = set_cursor(cursor, linea);
                        un_profesional.set_apellido(linea.substr(cursor));
                    }
                }
                else if (linea.find("birth") != std::string::npos)
                {
                    size_t cursor = linea.find("=");
                    if (linea.find("/") == std::string::npos or cursor == std::string::npos)
                    {
                        fin = true;
                        continue;
                    }
                    else
                    {
                        cursor = set_cursor(cursor, linea);
                        un_profesional.set_dia_nacimiento (std::stoi(linea.substr(cursor, linea.find("/") - cursor)));
                        cursor = linea.find("/") + 1;
                        un_profesional.set_mes_nacimiento (std::stoi(linea.substr(cursor, linea.find("/") - cursor)));
                        cursor = linea.find("/", cursor) + 1;
                        un_profesional.set_year_nacimiento(std::stoi(linea.substr(cursor)));
                    }
                }
                else if (linea.find("sexo") != std::string::npos)
                {
                    size_t cursor = linea.find("=");
                    if (cursor == std::string::npos)
                        continue;
                    else
                    {
                        cursor = set_cursor(cursor, linea);
                        un_profesional.set_sexo(linea.substr(cursor));
                    }
                }
                else if (linea.find("obras sociales {") != std::string::npos)
                {
					bool end = false;
					
					while (end == false and getline(file_datos_in, linea))
					{
						if (linea.find("}") != std::string::npos)
							end = true;
						
						else un_profesional.add_obra_social(linea.substr(0));
					}
                }
                else if (linea.find("profesion") != std::string::npos)
                {
                    size_t cursor = linea.find("=");
                    if (cursor == std::string::npos)
                        continue;
                    else
                    {
                        cursor = set_cursor(cursor, linea);
                        un_profesional.set_profesion(linea.substr(cursor));
                    }
                }
                else if (linea.find("matricula") != std::string::npos)
                {
                    size_t cursor = linea.find("=");
                    if (cursor == std::string::npos)
                        continue;
                    else
                    {
                        cursor = set_cursor(cursor, linea);
                        un_profesional.set_matricula(linea.substr(cursor));
                    }
                }
                else if (linea.find("universidad") != std::string::npos)
                {
                    size_t cursor = linea.find("=");
                    if (cursor == std::string::npos)
                        continue;
                    else
                    {
                        cursor = set_cursor(cursor, linea);
                        un_profesional.set_universidad(linea.substr(cursor));
                    }
                }
                else if (linea.find("especialidades {") != std::string::npos)
                {
					bool end = false;
					
					while (end == false and getline(file_datos_in, linea))
					{
						if (linea.find("}") != std::string::npos)
							end = true;
						
						else un_profesional.add_especialidad(linea.substr(0));
					}
                }
                else if (linea.find("horarios = {") != std::string::npos)
                {
                    std::vector<int> horarios;
                    bool end = false;
                    int index_dia = 0;

                    while (getline(file_datos_in, linea) and end == false)
                    {
                        size_t cursor = linea.find(":") + 2;

                            if (linea.find("_") != std::string::npos or linea.find(".") != std::string::npos)
                            {
                                if (linea.find("_") == std::string::npos)
                                {
                                    horarios.push_back(std::stol(linea.substr(cursor, linea.find(".") - cursor)));
                                }
                                else
                                {
                                    bool fin_horarios = false;
                                    do
                                    {
                                        if (linea.find("_", cursor) == std::string::npos)
                                        {
                                            horarios.push_back(std::stol(linea.substr(cursor, linea.find(".") - cursor)));
                                            fin_horarios = true;
                                        }
                                        else
                                        {
                                            horarios.push_back(std::stol(linea.substr(cursor, linea.find("_") - cursor)));
                                            cursor = linea.find("_", cursor) + 1;
                                        }
                                    } while (fin_horarios == false);
                                }
                            }
                                        un_profesional.add_horario(index_dia, horarios);
                                            horarios.erase(horarios.begin(), horarios.end());

                                            if (linea.find("6:") != std::string::npos)
                                                end = true;
                                            else
                                                ++index_dia;
                    }
                }
                else if (linea.find("dias no laborales") != std::string::npos)
                {
                    bool end = false;
                    
                    while (end == false and getline(file_datos_in, linea))
                    {
                        if (linea.find("}") != std::string::npos)
                            end = true;

                        else if (linea.find(".") != std::string::npos) un_profesional.add_dia_no_laboral(std::stol(linea.substr(0, linea.find("/"))),
                                                               std::stol(linea.substr(linea.find("/") + 1, linea.find("."))) );
                    }
                }
            }
            m_profesionales[un_profesional.get_dni()] = un_profesional;
        }
		else if(linea.find("turnos {") != std::string::npos) // 41268246,21268246,17,7,2,2020,0,0
		{
			bool fin = false;

			while (getline(file_datos_in, linea) and fin == false) //dniPaciente,dniProfesional,hora,dia,mes,a�o,visto,cancelado,expirado
			{
				if (linea.find("}") != std::string::npos) fin = true;

				else
				{
					long dni_paciente, dni_profesional;
					int hora, dia, mes, year;
					bool confirmado, cancelado, expirado;
					
					dni_paciente = std::stol( linea.substr(0, linea.find(",")) );
						size_t cursor =  linea.find(",") + 1;
						dni_profesional = std::stol( linea.substr(cursor, linea.find(",") - cursor) );
							cursor =  linea.find(",", cursor) + 1;
							hora = std::stoi( linea.substr(cursor, linea.find(",") - cursor) );
								cursor =  linea.find(",", cursor) + 1;
								dia = std::stoi( linea.substr(cursor, linea.find(",") - cursor) );
									cursor =  linea.find(",", cursor) + 1;
									mes = std::stoi( linea.substr(cursor, linea.find(",") - cursor) );
										cursor =  linea.find(",", cursor) + 1;
										year = std::stoi( linea.substr(cursor, linea.find(",") - cursor) );
											cursor =  linea.find(",", cursor) + 1;
											std::istringstream( linea.substr(cursor, linea.find(",") - cursor) ) >> confirmado;
												cursor =  linea.find(",", cursor) + 1;
												std::istringstream( linea.substr(cursor, linea.find(",") - cursor) ) >> cancelado;
													cursor =  linea.find(",", cursor) + 1;
													std::istringstream( linea.substr(cursor, linea.find(",") - cursor) ) >> expirado;
													
					Turno un_turno(dni_paciente, dni_profesional, hora, dia, mes, year);
					un_turno.confirmado = confirmado;
					un_turno.cancelado  = cancelado;
					
					if (expirado == false and confirmado == false)
					{
						int year_actual = get_year_now();
						int mes_actual  = get_mes_now();
						int dia_actual  = get_dia_now();
						int hora_actual = get_hora_now();
							
							if      (un_turno.year < year_actual) un_turno.expirado = true;
							else if (un_turno.year == year_actual)
							{
								if (un_turno.mes < mes_actual) un_turno.expirado = true;
								else if (un_turno.mes == mes_actual)
								{
									if(un_turno.dia < dia_actual) un_turno.expirado = true;
									else if (un_turno.dia == dia_actual and un_turno.hora < hora_actual) un_turno.expirado = true;
									else un_turno.expirado = false;
								}
								else un_turno.expirado = false;
							}
							else un_turno.expirado = false;
					}
					else if (confirmado == false) un_turno.expirado = true;
						m_turnos.push_back(un_turno);
				}
			}
		}
	}
return 0;
}

void Server::subir_datos(const Paciente& _un_paciente)
{
    auto it = m_id_pass.find(_un_paciente.get_dni());
    if (it == m_id_pass.end())
    {
        m_id_pass  [_un_paciente.get_dni()]   = _un_paciente.get_password();
        m_pacientes[_un_paciente.get_dni()] = _un_paciente;
    }
    else
        update(_un_paciente);
}
void Server::subir_datos(const Profesional& _un_profesional)
{
    auto it = m_id_pass.find(_un_profesional.get_dni());
    if (it == m_id_pass.end())
    {
        m_id_pass      [_un_profesional.get_dni()] = _un_profesional.get_password();
        m_profesionales[_un_profesional.get_dni()] = _un_profesional;
    }
    else
        update(_un_profesional);
}

int Server::cambiar_password(long _dni, int _index_rol, std::string _pass)
{
	m_id_pass[_dni] = _pass;
return 0;
}

void Server::update(Paciente _un_paciente)
{
    Id_Pass aux;

    auto it = m_id_pass.find(_un_paciente.get_dni());

    if (_un_paciente.get_password() != it->second) //si las passwords son distintas
    {
        std::fstream file(m_file_id_pass, std::ios::binary | std::ios::in | std::ios::out | std::ios::ate);
        int tam_bytes   = file.tellg();
        int n_elementos = tam_bytes / sizeof(aux);
        file.seekg(0);

        aux.id = _un_paciente.get_dni();
        for (int i = 0; i < n_elementos; ++i)
        {
            file.read(reinterpret_cast<char *>(&aux), sizeof(aux));
            if (aux.id == _un_paciente.get_dni())
            {
                file.seekp((i + 1) * sizeof(aux));
                strcpy(aux.password, _un_paciente.get_password().c_str());
                file.write(reinterpret_cast<char *>(&aux), sizeof(aux));

                i = n_elementos;
            }
        }
        m_id_pass[_un_paciente.get_dni()]   = _un_paciente.get_password();
    }
                                m_pacientes[_un_paciente.get_dni()] = _un_paciente;
                                //m_id_pass[_un_paciente.get_dni()]   = _un_paciente.get_password();
}
void Server::update(Profesional _un_profesional)
{
    Id_Pass aux;

    auto it = m_id_pass.find(_un_profesional.get_dni());

    if (_un_profesional.get_password() != it->second) //si las passwords son distintas
    {
        std::fstream file(m_file_id_pass, std::ios::binary | std::ios::in | std::ios::out | std::ios::ate);
        int tam_bytes = file.tellg();
        int n_elementos = tam_bytes / sizeof(aux);
        file.seekg(0);

        aux.id = _un_profesional.get_dni();
        for (int i = 0; i < n_elementos; ++i)
        {
            file.read(reinterpret_cast<char*>(&aux), sizeof(aux));
            if (aux.id == _un_profesional.get_dni())
            {
                file.seekp((i + 1) * sizeof(aux));
                strcpy(aux.password, _un_profesional.get_password().c_str());
                file.write(reinterpret_cast<char *>(&aux), sizeof(aux));

                i = n_elementos;
            }
        }
    }
                                m_profesionales[_un_profesional.get_dni()] = _un_profesional;
}

void Server::add_obra_social(std::string _obra_social)
{

    if (find(m_obras_sociales.begin(), m_obras_sociales.end(), _obra_social) == m_obras_sociales.end())
        m_obras_sociales.push_back(_obra_social);

}
std::vector<std::string> Server::request_obras_sociales()
{
    return m_obras_sociales;
}

void Server::add_especialidad (std::string _especialidad)
{
    if (find (m_especialidades.begin(), m_especialidades.end(), _especialidad) == m_especialidades.end())
        m_especialidades.push_back(_especialidad);
}

std::vector<std::string> Server::request_especialidades()
{
    return m_especialidades;
}

void Server::crear_file()
{

}

bool Server::buscar_paciente(long _dni)
{
	auto it = m_pacientes.find (_dni);
	
		if (it == m_pacientes.end())
			return false;
		else
			return true;
}
bool Server::buscar_profesional(long _dni)
{
	auto it = m_profesionales.find (_dni);
	
		if (it == m_profesionales.end())
			return false;
		else
			return true;
}

std::map<long, Profesional> Server::request_profesionales()
{
    return m_profesionales;
}

std::map<long, Paciente> Server::request_pacientes()
{
    return m_pacientes;
}

std::map<long, std::string> Server::request_passwords()
{
    return m_id_pass;
}

std::map<std::string, std::vector<long>> Server::request_especialidades_con_dni()
{
	std::map<std::string, std::vector<long>> especialidades_con_dni;

		for (auto &un_profesional : m_profesionales)
		{
			std::vector<std::string> sus_especialdiades = un_profesional.second.get_especialidades();
			
			for (std::string una_especialidad : sus_especialdiades)
			{
				especialidades_con_dni[una_especialidad].push_back(un_profesional.second.get_dni());
			}
		}
    return especialidades_con_dni;
}

Profesional Server::request_profesional (long _dni)
{
	auto it = m_profesionales.find (_dni);
	
		if (it != m_profesionales.end())
			return m_profesionales [_dni];
		else 
			return Profesional("null", "null");
}
Paciente Server::request_paciente (long _dni)
{
	auto it = m_pacientes.find (_dni);
	
		if (it != m_pacientes.end())
			return m_pacientes [_dni];
		else
			return Paciente("null", "null");
}

std::vector<std::vector<int>> Server::get_horarios_profesional(long _dni)	
{
	if (this->buscar_profesional(_dni) == true)
	{
		return m_profesionales[_dni].get_horarios();
	}
std::vector<std::vector<int>> a;
return a;
}
std::vector<Dia_libre> Server::get_dias_no_laborales_profesional(long _dni)
{
	if (this->buscar_profesional(_dni) == true)
	{
		return m_profesionales[_dni].get_dias_no_laborales();
	}
std::vector<Dia_libre> a;
return a;
}

int Server::add_turno (Turno _new_turno)
{
	m_turnos.push_back(_new_turno);
	//sort(m_turnos.begin(), m_turnos.end());
	
return 0;
}
int Server::add_turno (long _dni_paciente, long _dni_profesional, int _hora, int _dia, int _mes, int _year)
{
	Turno _new_turno(_dni_paciente, _dni_profesional, _hora,  _dia, _mes, _year);
	m_turnos.push_back(_new_turno);
	
	return 0;
}
std::vector<Turno> Server::request_turnos (long _dni)
{
		std::vector<Turno> turnos;
		for (auto& un_turno : m_turnos)
		{
			if (_dni == un_turno.dni_paciente or _dni == un_turno.dni_profesional)
			{
				turnos.push_back(un_turno);
			}
		}
return turnos;
}
std::vector<Turno> Server::request_turnos (long _dni_profesional, int _mes, int _year)
{
	std::vector<Turno> turnos;
	
	if (_mes > 0 and _mes < 13)
	{
		for (auto& un_turno : m_turnos)
		{
			if (_dni_profesional == un_turno.dni_profesional and _mes == un_turno.mes and _year == un_turno.year)
			{
				turnos.push_back(un_turno);
			}
		}
	}
return turnos;
}

std::vector<Turno> Server::request_turnos (long _dni_profesional, int _dia, int _mes, int _year)
{
	std::vector<Turno> turnos;
	
	if (_mes > 0 and _mes < 13)
	{
		for (auto& un_turno : m_turnos)
		{
			if (_dni_profesional == un_turno.dni_profesional and _dia == un_turno.dia and _mes == un_turno.mes and _year == un_turno.year)
			{
				turnos.push_back(un_turno);
			}
		}
	}
return turnos;
}

std::vector<Turno> Server::request_turnos (long _dni_profesional, std::string _obra_social)
{
	std::vector<Turno> turnos;
	
		for (auto& un_turno : m_turnos)
		{
			if (_dni_profesional == un_turno.dni_profesional and _obra_social == m_pacientes[un_turno.dni_paciente].get_obra_social())
			{
				turnos.push_back(un_turno);
			}
		}
return turnos;
}

std::vector<Turno> Server::request_turnos (long _dni_profesional, long _dni_paciente)
{
	std::vector<Turno> turnos;
	
	for (auto& un_turno : m_turnos)
	{
		if (_dni_profesional == un_turno.dni_profesional and _dni_paciente == un_turno.dni_paciente)
		{
			turnos.push_back(un_turno);
		}
	}
return turnos;
}

bool Server::check_turno (Turno _new_turno)
{
	std::vector<Turno> turnos_paciente    = this->request_turnos(_new_turno.dni_paciente);
	std::vector<Turno> turnos_profesional = this->request_turnos(_new_turno.dni_profesional);
	
	for (auto &un_turno : turnos_paciente)
	{
		if (un_turno.hora == _new_turno.hora and un_turno.dia == _new_turno.dia and un_turno.mes == _new_turno.mes and un_turno.year == _new_turno.year)
			return false;
	}
	
	for (auto &un_turno : turnos_profesional)
	{
		if (un_turno.hora == _new_turno.hora and un_turno.dia == _new_turno.dia and un_turno.mes == _new_turno.mes and un_turno.year == _new_turno.year)
			return false;
	}
	
		std::vector<std::vector<int>> horarios_profesional   = this->get_horarios_profesional(_new_turno.dni_profesional);
		
		int t[] = {0, 3, 2, 5, 0, 3, 5, 1, 4, 6, 2, 4};
		int aux = _new_turno.year;
		aux -= _new_turno.mes < 3;
		int indice_dia = (aux + aux / 4 - aux / 100 + aux / 400 + t[_new_turno.mes - 1] + _new_turno.dia) % 7;
		
		auto it_hora = find(horarios_profesional[indice_dia].begin(), horarios_profesional[indice_dia].end(), _new_turno.hora); //buscar en lunes o martes ...
		
		if (it_hora == horarios_profesional[indice_dia].end())
			return false;
		
			std::vector<Dia_libre> dias_no_laborales_profesional = this->get_dias_no_laborales_profesional(_new_turno.dni_profesional);
			
			for (auto &una_fecha : dias_no_laborales_profesional)
			{
				if (una_fecha.dia == _new_turno.dia)
					return false;
			}

return true;
}
bool Server::check_turno (long _dni_paciente, long _dni_profesional, int _hora, int _dia, int _mes, int _year)
{
Turno _new_turno(_dni_paciente, _dni_profesional, _hora,  _dia, _mes, _year);
	
	auto it = find(m_turnos.begin(), m_turnos.end(), _new_turno);
	
	if (it != m_turnos.end()) return false;
	
		std::vector<std::vector<int>> horarios_profesional = this->get_horarios_profesional(_dni_profesional);
		
		int t[] = {0, 3, 2, 5, 0, 3, 5, 1, 4, 6, 2, 4};
		int aux = _new_turno.year;
		aux -= _new_turno.mes < 3;
		int indice_dia = (aux + aux / 4 - aux / 100 + aux / 400 + t[_new_turno.mes - 1] + _new_turno.dia) % 7;
		
		auto it_hora = find(horarios_profesional[indice_dia].begin(), horarios_profesional[indice_dia].end(), _hora); //buscar en lunes o martes ...

		if (it_hora == horarios_profesional[indice_dia].end()) return false;
	
			std::vector<Dia_libre> dias_no_laborales_profesional = this->get_dias_no_laborales_profesional(_new_turno.dni_profesional);
			
			for (auto &una_fecha : dias_no_laborales_profesional)
			{
				if (una_fecha.dia == _new_turno.dia)
					return false;
			}
	
	return true;
}
void Server::confirmar_turno (Turno _un_turno)
{
	auto it = find(m_turnos.begin(), m_turnos.end(), _un_turno);
	
		it->confirmado = true;
}
void Server::cancelar_turno (Turno _un_turno)
{
	auto it = find(m_turnos.begin(), m_turnos.end(), _un_turno);
	
		m_turnos.erase(it);
}

void Server::cancelar_turno (long _dni_paciente, long _dni_profesional, int _hora, int _dia, int _mes, int _year)
{
	Turno _new_turno (_dni_paciente, _dni_profesional, _hora, _dia, _mes, _year);
	
	auto it = find(m_turnos.begin(), m_turnos.end(), _new_turno);
	
		m_turnos.erase(it);
}

std::vector<int> Server::request_dias_disponibles (long _dni_profesional, int _mes, int _year)
{
	std::vector<int>              dias_disponibles;
	std::vector<Turno>            turnos_profesional = this->request_turnos(_dni_profesional);
	std::vector<std::vector<int>> horarios           = m_profesionales[_dni_profesional].get_horarios();
	std::vector<Dia_libre> 		  dias_no_laborales  = m_profesionales[_dni_profesional].get_dias_no_laborales();
	
	int cantidad_dias_mes = 0;
	
		switch (_mes)
		{
		case 1:
			cantidad_dias_mes = 31; break;
			
		case 2:
			
			if (_year % 400 == 0 || (_year % 4 == 0 && _year % 100 != 0) )
				cantidad_dias_mes = 29;
			else
				cantidad_dias_mes = 28;
			break;
		case 3:
			cantidad_dias_mes = 31; break;
			
		case 4:
			cantidad_dias_mes = 30; break;
			
		case 5:
			cantidad_dias_mes = 31; break;
			
		case 6:
			cantidad_dias_mes = 30; break;
			
		case 7:
			cantidad_dias_mes = 31; break;
			
		case 8:
			cantidad_dias_mes = 30; break;
			
		case 9:
			cantidad_dias_mes = 30; break;
			
		case 10:
			cantidad_dias_mes = 31; break;
			
		case 11:
			cantidad_dias_mes = 30; break;
			
		case 12:
			cantidad_dias_mes = 31; break;
		}
		
		int t[] = { 0, 3, 2, 5, 0, 3, 5, 1, 4, 6, 2, 4 }; 
		_year -= _mes < 3; 
		int index_dia;
		
			for (int i = 1; i <= cantidad_dias_mes; ++i)
			{
				index_dia = ( _year + _year/4 - _year/100 + _year/400 + t[_mes-1] + i) % 7; // 0: domingo, 1: lunes ...
				
				if (horarios[index_dia].empty()) continue;
				
				Dia_libre un_dia = { i, _mes };
				
				if (find( dias_no_laborales.begin(), dias_no_laborales.end(), un_dia ) == dias_no_laborales.end())
				{
					dias_disponibles.push_back(i);
				}
			}
				for (auto& un_turno : turnos_profesional)
				{
					if (un_turno.mes == _mes)
					{
						auto it_remove = remove(dias_disponibles.begin(), dias_disponibles.end(), un_turno.dia);
						dias_disponibles.erase(it_remove, dias_disponibles.end());
					}
				}
	return dias_disponibles;
}

size_t Server::set_cursor(size_t _cursor, std::string _linea)
{
	if (_cursor == _linea.find("="))
		++_cursor;
	
	while (_linea.substr(_cursor, 1) == " " or _linea.substr(_cursor, 1) == "_")
		++_cursor;
	
	return _cursor;
}


Server::~Server()
{
		Id_Pass aux;
		std::ofstream file_out(m_file_id_pass, std::ios::binary | std::ios::trunc);
		for (auto &un_paciente : m_id_pass)
		{
			aux.id = un_paciente.first;
			strcpy(aux.password, un_paciente.second.c_str());
			file_out.write(reinterpret_cast<char *>(&aux), sizeof(aux));
		}

    std::ofstream file_datos_out(m_file_datos, std::ios::trunc);
    std::vector<std::string> v;
    for (auto &un_paciente : m_pacientes)
    {
        file_datos_out << std::endl;
        file_datos_out << "[Paciente]" << std::endl;
        file_datos_out << "dni = " << un_paciente.second.get_dni() << std::endl;
        file_datos_out << "nombre = " << un_paciente.second.get_nombre() << std::endl;
        file_datos_out << "apellido = " << un_paciente.second.get_apellido() << std::endl;
        file_datos_out << "birth = " << un_paciente.second.get_dia_nacimiento() <<"/"<<un_paciente.second.get_mes_nacimiento()<<"/"<<un_paciente.second.get_year_nacimiento() << std::endl;
        file_datos_out << "sexo = " << un_paciente.second.get_sexo() << std::endl;
        file_datos_out << "obra social = " << un_paciente.second.get_obra_social() << std::endl;
        file_datos_out << "credencial = " << un_paciente.second.get_credencial() << std::endl;
        file_datos_out << "diagnosticos { " << std::endl;

        v = un_paciente.second.get_diagnosticos();
        for (size_t i = 0; i < v.size(); ++i)
        {
            if (v.size() - i == 1)
            {
                file_datos_out << v[i] << std::endl;
            }
            else
            {
                file_datos_out << v[i] << std::endl;
            }
        }

        file_datos_out << "}";
                        

file_datos_out << std::endl
			   << "END"
			   << std::endl;
    }

    for (auto &un_profesional : m_profesionales)
    {
        file_datos_out << std::endl;
        file_datos_out << "[Profesional]" << std::endl;
        file_datos_out << "dni = " << un_profesional.second.get_dni() << std::endl;
        file_datos_out << "nombre = " << un_profesional.second.get_nombre() << std::endl;
        file_datos_out << "apellido = " << un_profesional.second.get_apellido() << std::endl;
        file_datos_out << "birth = " << un_profesional.second.get_dia_nacimiento() <<"/"<<un_profesional.second.get_mes_nacimiento()<<"/"<<un_profesional.second.get_year_nacimiento() << std::endl;
        file_datos_out << "sexo = " << un_profesional.second.get_sexo() << std::endl;
        file_datos_out << "profesion = " << un_profesional.second.get_profesion() << std::endl;
        file_datos_out << "matricula = " << un_profesional.second.get_matricula() << std::endl;
        file_datos_out << "universidad = " << un_profesional.second.get_universiad() << std::endl;
        file_datos_out << "especialidades { " << std::endl;

        v = un_profesional.second.get_especialidades();
        for (size_t i = 0; i < v.size(); ++i)
        {
			if (v.size() - i == 1)
			{
				file_datos_out << v[i];
			}
			else
			{
				file_datos_out << v[i] << std::endl;
			}
        }
        file_datos_out << std::endl <<"}"<<std::endl;

        file_datos_out << "obras sociales { " << std::endl;

        v = un_profesional.second.get_obras_sociales();
        for (size_t i = 0; i < v.size(); ++i)
        {
            if (v.size() - i == 1)
            {
                file_datos_out << v[i];
            }
            else
            {
                file_datos_out << v[i] << std::endl;
            }
        }
        file_datos_out << std::endl << "}" << std::endl;

        file_datos_out << "horarios = {" << std::endl;
        
        std::vector<std::vector<int>> horarios = un_profesional.second.get_horarios();

        for (size_t i = 0; i < horarios.size(); ++i)
        {
			file_datos_out << i << ": ";
			for (size_t j = 0; j < horarios[i].size(); ++j)
			{
				if (horarios[i].size() - j == 1)
				{
					file_datos_out << horarios[i][j] << ".";
				}
				else
				{
					file_datos_out << horarios[i][j] << "_";
				}
			}
			file_datos_out << std::endl;
        }
                file_datos_out << "}" << std::endl;

        file_datos_out << "dias no laborales {" << std::endl;
        /* = {
            25/12.} */
        std::vector<Dia_libre> dias_libres = un_profesional.second.get_dias_no_laborales();
        for (size_t i = 0; i < dias_libres.size(); ++i)
        {
            if (dias_libres.size() - i == 1)
            {
                file_datos_out << dias_libres[i].dia << "/"
                                << dias_libres[i].mes << ".";
            }
            else
            {
                file_datos_out << dias_libres[i].dia << "/"
                                << dias_libres[i].mes << "." << std::endl;
            }
        }
            file_datos_out << std::endl << "}";

file_datos_out << std::endl
			   << "END"
			   << std::endl << std::endl;

    }
	
			file_datos_out << "turnos {" << std::endl;
	
			for (auto& un_turno : m_turnos)
			{
				file_datos_out << un_turno.dni_paciente<<","<<un_turno.dni_profesional <<","<< un_turno.hora<<","<<un_turno.dia<<","<<un_turno.mes<<","<< un_turno.year<<","<< un_turno.confirmado<<","<< un_turno.cancelado<<","<< un_turno.expirado; 
				file_datos_out << std::endl;
			}
			file_datos_out << "}";
			
			
}
