#include "Utils/string_conv.h"
#include <wx/msgdlg.h>
#include "Hija_GUI_dialog_obra_social.h"

Hija_GUI_dialog_obra_social::Hija_GUI_dialog_obra_social(wxWindow *parent, Server* _server, Profesional* _profesional)
	: Base_GUI_dialog_obra_social(parent), m_server(_server), m_profesional(_profesional)
{
	std::vector<std::string> vstr = m_server->request_obras_sociales();
	
	for (auto& x : vstr)
	{
		m_choice_all_obras_sociales->Append(x);
	}
}

void Hija_GUI_dialog_obra_social::OnClick_ingresar_obra_social( wxCommandEvent& event )
{
	int feed_back = m_profesional->add_obra_social(wx_to_std( m_choice_all_obras_sociales->GetString( m_choice_all_obras_sociales->GetSelection() )));
	if (feed_back == 0) 
	{
		wxMessageBox("Obra social agregada");
		EndModal(0);
	}
	else wxMessageBox("Error al agregar obra social");
}
