#include "Utils/string_conv.h"
#include "Utils/fecha.h"
#include <wx/msgdlg.h>
#include "Hija_GUI_dialog_dias_libres.h"

Hija_GUI_dialog_dias_libres::Hija_GUI_dialog_dias_libres(wxWindow *parent, Server* server, Profesional* profesional)
	: Base_GUI_dialog_dias_libres(parent), m_server(server), m_profesional(profesional)
{
	
}

void Hija_GUI_dialog_dias_libres::OnClick_add_free_dia( wxCommandEvent& event )
{
	if (m_choice_mes->GetSelection() != 0 and m_choice_dia->GetSelection() != -1)
	{
		m_profesional->add_dia_no_laboral( m_choice_dia->GetSelection()+1,
										  m_choice_mes->GetSelection() );
		
			wxMessageBox("Dia agregado");
			EndModal(0);
	}
}

void Hija_GUI_dialog_dias_libres::OnChoice_mes( wxCommandEvent& event )
{
	int year = get_year_now(); 
	int mes = m_choice_mes->GetSelection();

m_choice_dia->Clear();

	int cantidad_dias_mes = 0;
	switch (mes)
	{
	case 1:
		cantidad_dias_mes = 31; break;
		
	case 2:
		
		if (year % 400 == 0 || (year % 4 == 0 && year % 100 != 0) )
			cantidad_dias_mes = 29;
		else
			cantidad_dias_mes = 28;
		break;
	case 3:
		cantidad_dias_mes = 31; break;
		
	case 4:
		cantidad_dias_mes = 30; break;
		
	case 5:
		cantidad_dias_mes = 31; break;
		
	case 6:
		cantidad_dias_mes = 30; break;
		
	case 7:
		cantidad_dias_mes = 31; break;
		
	case 8:
		cantidad_dias_mes = 30; break;
		
	case 9:
		cantidad_dias_mes = 30; break;
		
	case 10:
		cantidad_dias_mes = 31; break;
		
	case 11:
		cantidad_dias_mes = 30; break;
		
	case 12:
		cantidad_dias_mes = 31; break;
	}
	
			for (int i = 1; i <= cantidad_dias_mes; ++i)
					m_choice_dia->Append( std::to_string(i) );
}
