#include "wx/msgdlg.h"
#include "Utils/string_conv.h"
#include "paciente.h"
#include "profesional.h"
#include "server.h"
#include "Hija_main_GUI_profesional.h"
#include "Hija_GUI_horarios_profesional.h"
#include "Hija_GUI_perfil_profesional.h"
#include "Hija_GUI_dialog_unPaciente.h"
#include "Hija_GUI_dialog_busqueda_avanzada.h"


Hija_main_GUI_profesional::Hija_main_GUI_profesional(Server* server, Profesional* profesional)
	: Base_main_GUI_profesional(nullptr), m_server(server), m_profesional(profesional)
{
	refresh();
}

void Hija_main_GUI_profesional::OnClick_cancelar_turno  ( wxCommandEvent& event )
{
	int seleccion = m_grilla_turnos->GetGridCursorRow();
	
	if (seleccion > -1)
	{
		long dni_paciente = m_turnos[seleccion].dni_paciente;
		int hora = m_turnos[seleccion].hora;
		int dia  = m_turnos[seleccion].dia;
		int mes  = m_turnos[seleccion].mes;
		int year = m_turnos[seleccion].year;
		
			m_server->cancelar_turno (dni_paciente, m_profesional->get_dni(), hora, dia, mes, year);
		
				wxMessageBox("Turno cancelado");
		
					refresh();
	}
}
void Hija_main_GUI_profesional::OnClick_confirmar_turno ( wxCommandEvent& event )
{
	int seleccion = m_grilla_turnos->GetGridCursorRow();
	
	Turno turno = m_turnos[seleccion];
	
	if (seleccion > -1)
	{
		if (turno.expirado == false and turno.confirmado == false)
		{
			m_server->confirmar_turno (turno);
			
			wxMessageBox("Turno confirmado");
			
			refresh();
		}
		else if (turno.expirado == true)   wxMessageBox(_T("El turno seleccionado expir\u00F3"));
		else if (turno.confirmado == true) wxMessageBox("Turno ya confirmado");
	}
}

void Hija_main_GUI_profesional::OnClick_cerrar_sesion   ( wxCommandEvent& event )
{
	Close();
}
void Hija_main_GUI_profesional::OnClick_config_perfil   ( wxCommandEvent& event )
{
	Hija_GUI_perfil_profesional win_perfil(this, m_server, m_profesional);
	win_perfil.ShowModal();
}
void Hija_main_GUI_profesional::OnClick_config_horarios ( wxCommandEvent& event )
{
	Hija_GUI_horarios_profesional win_horarios(this, m_server, m_profesional);
	win_horarios.ShowModal();
}

void Hija_main_GUI_profesional::OnClick_buscar_paciente( wxCommandEvent& event )
{
	long dni_ingresado; m_textCtrl_dni_paciente->GetValue().ToLong(&dni_ingresado);
	
	if (m_server->buscar_paciente(dni_ingresado))
	{

		Paciente un_paciente =  m_server->request_paciente(dni_ingresado);
		Hija_GUI_dialog_unPaciente win_paciente(this, m_server, m_profesional, &un_paciente);
		win_paciente.ShowModal();
		
	}
	else wxMessageBox ("Paciente no registrado");
}

void Hija_main_GUI_profesional::OnClick_busqueda_avanzada ( wxCommandEvent& event )
{
	Hija_GUI_dialog_busqueda_avanzada win_busqueda(this, m_server, m_profesional);
	win_busqueda.ShowModal();
}

void Hija_main_GUI_profesional::refresh()
{
	int filas = m_grilla_turnos->GetNumberRows();
	if (filas > 0) m_grilla_turnos->DeleteRows(0,filas,true);
		
		m_turnos = m_server->request_turnos(m_profesional->get_dni());
		m_grilla_turnos->AppendRows(m_turnos.size());
		m_grilla_turnos->SetSelectionMode(wxGrid::wxGridSelectRows);
		
		for (size_t i = 0; i < m_turnos.size(); ++i)
		{
			if (m_turnos[i].confirmado == true)
			{
				m_grilla_turnos->SetCellValue(i, 0, "[CONFIRMADO] "+ std_to_wx(m_server->request_paciente(m_turnos[i].dni_paciente).get_nombre()) +" "+ std_to_wx(m_server->request_paciente(m_turnos[i].dni_paciente).get_apellido()) +". DNI: "+ std::to_string(m_turnos[i].dni_paciente)
											  +". Hora: " + std::to_string(m_turnos[i].hora) +". Dia: "+ std::to_string(m_turnos[i].dia) + ". Mes: " + std::to_string(m_turnos[i].mes) + ". Year: "+ std::to_string(m_turnos[i].year));
			}
			else if (m_turnos[i].cancelado == false and m_turnos[i].expirado == false)
			{
				m_grilla_turnos->SetCellValue(i, 0, std_to_wx(m_server->request_paciente(m_turnos[i].dni_paciente).get_nombre()) +" "+ std_to_wx(m_server->request_paciente(m_turnos[i].dni_paciente).get_apellido()) +". DNI: "+ std::to_string(m_turnos[i].dni_paciente)
											  +". Hora: " + std::to_string(m_turnos[i].hora) +". Dia: "+ std::to_string(m_turnos[i].dia) + ". Mes: " + std::to_string(m_turnos[i].mes) + ". Year: "+ std::to_string(m_turnos[i].year));
			}
			/*else if (m_turnos[i].cancelado)
			{
			m_grilla_turnos->SetCellValue(i, 0, "[CANCELADO] "+ m_server->request_paciente(m_turnos[i].dni_paciente).get_nombre() +" "+ m_server->request_paciente(m_turnos[i].dni_paciente).dni_paciente).get_apellido()
			+". Hora: " + std::to_string(m_turnos[i].hora) +". Dia: "+ std::to_string(m_turnos[i].dia) + ". Mes: " + std::to_string(m_turnos[i].mes) + ". Year: "+ std::to_string(m_turnos[i].year));
			}*/
			else
			{
				m_grilla_turnos->SetCellValue(i, 0, "[EXPIRADO] "+ std_to_wx(m_server->request_paciente(m_turnos[i].dni_paciente).get_nombre()) +" "+ std_to_wx(m_server->request_paciente(m_turnos[i].dni_paciente).get_apellido()) +". DNI: "+ std::to_string(m_turnos[i].dni_paciente)
											  +". Hora: " + std::to_string(m_turnos[i].hora) +". Dia: "+ std::to_string(m_turnos[i].dia) + ". Mes: " + std::to_string(m_turnos[i].mes) + ". Year: "+ std::to_string(m_turnos[i].year));
			}
		}
}


Hija_main_GUI_profesional::~Hija_main_GUI_profesional()
{
	m_server->subir_datos(*m_profesional);
}
