#ifndef HIJA_GUI_DIALOG_UNPACIENTE_H
#define HIJA_GUI_DIALOG_UNPACIENTE_H
#include "wx/msgdlg.h"
#include "Utils/string_conv.h"
#include "paciente.h"
#include "profesional.h"
#include "server.h"
#include "Bases_GUI/GUI_profesional.h"

class Hija_GUI_dialog_unPaciente : public Base_GUI_dialog_unPaciente 
{
public:
	Hija_GUI_dialog_unPaciente (wxWindow* parent, Server* _server, Profesional* _profesional, Paciente* _un_paciente);
	
	void OnClick_borrar_diagnostico ( wxCommandEvent& event );
	void OnClick_agregar_diagnostico( wxCommandEvent& event );
	void OnClick_ver_turnos 		( wxCommandEvent& event );
		
private:
	Server*      m_server      = nullptr;
	Profesional* m_profesional = nullptr;
	Paciente*    m_paciente    = nullptr;
	
	void refresh();
};

#endif

