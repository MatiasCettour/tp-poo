#include <iostream>
#include <fstream>
#include <sstream>
#include <cstring>
#include <map>

using namespace std;

struct Id_Pass
{
	int id = 0;
	char password [32] = "";
};

int main(int argc, char *argv[]) 
{
	Id_Pass aux;
	
	std::ifstream file_passwords_in("../Datos/dni_pass.bin", std::ios::binary | std::ios::ate);
	
	int tam_bytes = file_passwords_in.tellg();
	int n_elementos = tam_bytes / sizeof(aux);
	
	cout << n_elementos << endl;
	
	if (n_elementos > 0 and file_passwords_in.is_open())
	{
		file_passwords_in.seekg(0);
		for (int i = 0; i < n_elementos; ++i)
		{
			file_passwords_in.read(reinterpret_cast<char*>(&aux), sizeof(aux));
			cout << aux.id << " " << aux.password << endl;
		}
	}
	return 0;
}

