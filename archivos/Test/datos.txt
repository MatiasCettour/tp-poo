
[Paciente]
dni = 41268246
nombre = Matias
apellido = Cettour
birth = 7/10/1998
sexo = Masculino
obra social = Medicus
credencial = 44-555-97
diagnosticos { 
Dengue
COVID19
Malaria
}
turnos = {
DNI: 21268246. hora: 17. dia: 9. mes: 10. year: 2020.
}
END

[Paciente]
dni = 42268245
nombre = Alejandro
apellido = Cettour
birth = 7/10/1999
sexo = -
obra social = -
credencial = 44-555-97
diagnosticos { 
Complejo de superioridad
}
turnos = {
}
END

[Profesional]
dni = 21268246
nombre = Rodolfo
apellido = Zacarias
birth = 10/7/1968
sexo = Masculino
profesion = Medico
matricula = 9-4555
universidad = UNL
especialidades { 

Dermatologia
Psicologia
Infectologia
}
obras sociales { 

Swiss Medical
Medicus
}
horarios = {
0: 
1: 
2: 
3: 9_10_11.
4: 
5: 17.
6: 
}
dias no laborales {
25/12.
}
turnos = {
DNI: 41268246. hora: 17. dia: 9. mes: 10. year: 2020.
}
END
