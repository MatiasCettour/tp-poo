#include "archivos/paciente.h"
#include "archivos/profesional.h"
#include "archivos/server.h"
#include <iostream>

using namespace std;


ostream &operator<< (ostream &o, const Paciente&    un_paciente);
ostream &operator<< (ostream &o, const Profesional& un_profesional);

int main()
{
cout << "opening server" << endl;
Server server;

std::map<long, Paciente>    pacientes     = server.request_pacientes();
std::map<long, Profesional> profesionales = server.request_profesionales();
std::map<long, string>      passwords     = server.request_passwords();

//profesionales[21268246].set_health_insurance("Swiss Medical");
//profesionales[21268246].add_especialidad("Infectologia");

    /*if (profesionales[21268246].check_turno(41268246, 17, 3, 4, 2020) == true and pacientes[41268246].check_turno(21268246, 17, 3, 4, 2020) == true)
    {
        profesionales[21268246].add_turno(41268246, 17, 3, 4, 2020);
        pacientes[41268246].add_turno(41268246, 17, 3, 4, 2020);
        cout << "turno agregado" << endl; 
    }*/

//profesionales[21268246].add_dia_no_laboral(25,12);

//server.subir_datos(profesionales[21268246]);
//server.subir_datos(pacientes[41268246]);

//server.add_health_insurance("Swiss Medical");

//vector<string> vstr = server.request_health_insurances();

/*for (auto &x : vstr)
{
    cout << x << endl;
}*/

for (auto &p : pacientes)
{
    cout << p.second << endl;
}
for (auto &p : profesionales)
{
    cout << p.second << endl;
}
for (auto &p : passwords)
    cout << p.first << " " << p.second << endl;



//------------------------------------------------------------------------------------------
/* Paciente alejandro("Alejandro", "Cettour");
    alejandro.set_password("asd123");
    alejandro.set_fecha_nacimiento("7/10/1999");
    alejandro.set_dni(42268245);
    alejandro.set_estado_civil("Soltero");

    alejandro.set_health_insurance("Swiss Medical");
    alejandro.set_credencial("44-555-97");
    alejandro.add_diagnostico("Esquizofrenia");
    alejandro.add_diagnostico("Sindrome de DOWN");

    cout << alejandro;

    ////////////////////////////////////////////////////////////////////////////////////////
    Profesional rodolfo("Rodolfo", "Zacarias");
    rodolfo.set_password("asd456");
    rodolfo.set_fecha_nacimiento("10/7/1958");
    rodolfo.set_dni(21268246);
    rodolfo.set_estado_civil("single");

    rodolfo.set_profesion("Medico");
    rodolfo.set_matricula("9-4555");
    rodolfo.set_universidad("UNL");
    rodolfo.add_especialidad("Ginecologia");

    cout << rodolfo << endl;

    server.subir_datos(alejandro);
    server.subir_datos(rodolfo);*/

//server.crear_file();

std::vector<std::vector<int>> horarios_profesional = server->get_horarios_profesional(21268246);

for (int i = 0; i<horarios_profesional.size(); ++i)
{
    for (int j = 0; j<horarios[i].size(); ++j)
        cout << horarios[i][j] << " ";
    cout << endl;
}

cout << "server closed" << endl;
}
//____________________________________________________________________________________________________________________________________________________________________
ostream& operator<< (ostream& o, const Paciente& un_paciente)
{
    o << un_paciente.get_nombre() << " " << un_paciente.get_apellido()
      << endl
      << un_paciente.get_password() << endl
      << un_paciente.get_edad() << endl
      << un_paciente.get_dni() << endl
      << un_paciente.get_dia_nacimiento() <<"/"<<un_paciente.get_mes_nacimiento()<<"/"<<un_paciente.get_year_nacimiento()<< endl
      << un_paciente.get_sexo() << endl
      << un_paciente.get_health_insurance() << endl
      << un_paciente.get_credencial() << endl;

    vector<string> diagnosticos = un_paciente.get_diagnosticos();
    o << "Diagnosticos: ";
    for (auto &x : diagnosticos)
        o << x << " ";

    o << "\n\n";

return o;
}

ostream &operator<<(ostream &o, const Profesional& un_profesional)
{
    o << un_profesional.get_nombre() << " " << un_profesional.get_apellido()
      << endl
      << un_profesional.get_password() << endl
      << un_profesional.get_edad() << endl
      << un_profesional.get_dni() << endl
      << un_profesional.get_dia_nacimiento() <<"/"<<un_profesional.get_mes_nacimiento()<<"/"<<un_profesional.get_year_nacimiento()<< endl
      << un_profesional.get_sexo() << endl
      << un_profesional.get_profesion() << endl
      << un_profesional.get_matricula() << endl
      << un_profesional.get_universiad() << endl;

    vector<string> diagnosticos = un_profesional.get_especialidades();
    o << "Especialidades: ";
    for (auto &x : diagnosticos)
        o << x << " ";
        
    o << "\n";

    /*std::vector<std::vector<int>> horarios = un_profesional.get_horarios();
    for (auto &v : horarios)
        for (auto &x : v)
            o << x << " ";*/

    vector<Dia_libre> dias_libres = un_profesional.get_dias_no_laborales();
    if (dias_libres.empty()) o << "no dias libres";
    else
    {
        for (auto &un_dia : dias_libres)
            o << un_dia.dia << "/" << un_dia.mes << " ";
    }
    

        o << "\n";

    return o;
}

/*
g++ -std=c++17 -c main.cpp && g++ -std=c++17 -o main user.o paciente.o profesional.o server.o  main.o && ./main

g++ -std=c++17 -c server.cpp && g++ -std=c++17 -o main user.o paciente.o profesional.o server.o  main.o && ./main

g++ -std=c++17 -c user.cpp && g++ -std=c++17 -c paciente.cpp && g++ -std=c++17 -c profesional.cpp && g++ -std=c++17 -c server.cpp && g++ -std=c++17 -c main.cpp

*/
