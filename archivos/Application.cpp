#include <wx/image.h>
#include <fstream>
#include <sstream>
#include <cstring>
#include <map>
#include "Application.h"
#include "Hija_GUI_login.h"
#include "Hija_main_GUI_paciente.h"
#include "server.h"

IMPLEMENT_APP(Application)

bool Application::OnInit()
{
	wxInitAllImageHandlers();
	
	Hija_GUI_login *win_login = new Hija_GUI_login(&m_server);
	win_login->Show();
	
	return true;
}

//`wx-config --cppflags` -Wno-unused-local-typedefs
