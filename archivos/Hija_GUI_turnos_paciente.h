#ifndef HIJA_GUI_TURNOS_PACIENTE_H
#define HIJA_GUI_TURNOS_PACIENTE_H
#include "Bases_GUI/GUI_paciente.h"
#include "server.h"

class Server;
class Paciente;
class User;

class Hija_GUI_turnos_paciente : public Base_GUI_turnos_paciente
{
public:
	Hija_GUI_turnos_paciente(wxWindow *parent, Server *_server, Paciente *_paciente, std::vector<Turno> _turnos);
	
	void OnClick_cerrar( wxCommandEvent& event );
	void OnClick_cancelar_turno( wxCommandEvent& event );
	
private:
	Server   *m_server   = nullptr;
	Paciente *m_paciente = nullptr;
	std::vector<Turno> m_turnos;

	void refresh();
};

#endif

