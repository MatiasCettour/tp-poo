#ifndef HIJA_PERFIL_PROFESIONAL_H
#define HIJA_PERFIL_PROFESIONAL_H
#include "Bases_GUI/GUI_profesional.h"
#include "server.h"

class Server;
class Profesional;

class Hija_GUI_perfil_profesional : public Base_GUI_perfil_profesional 
{
public:
	Hija_GUI_perfil_profesional (wxWindow* parent, Server* server, Profesional* profesional);
	
	void Onbutton_eliminar_obra_social  ( wxCommandEvent& event );
	void Onbutton_agregar_obra_social   ( wxCommandEvent& event );
	void OnChoice_eliminar_especialidad ( wxCommandEvent& event );
	void OnChoice_agregar_especialidad  ( wxCommandEvent& event );
	void OnClick_aceptar_perfil         ( wxCommandEvent& event );
	
private:
	Server* m_server           = nullptr;
	Profesional* m_profesional = nullptr;
	void refresh();
};

#endif

