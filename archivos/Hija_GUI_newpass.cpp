#include "Hija_GUI_newpass.h"
#include "Utils/string_conv.h"
#include <wx/msgdlg.h>
#include "server.h"

Hija_GUI_newpass::Hija_GUI_newpass(wxWindow* _parent, Server* _server, long _dni, int _index_rol)
	:	Base_GUI_newpass(_parent), m_server(_server), m_dni(_dni), m_index_rol(_index_rol)
{}

void Hija_GUI_newpass::OnClick_aceptar_newpass ( wxCommandEvent& event )
{
	std::string new_pass1   = wx_to_std(m_textCtrl_new_pass1->GetValue());
	std::string new_pass2   = wx_to_std(m_textCtrl_new_pass2->GetValue());
	
		if (new_pass1.length() > 5 and new_pass2.length() > 5)
		{
			if (new_pass1 == new_pass2)
			{
				m_server->cambiar_password(m_dni, m_index_rol, new_pass1);
					
					m_textCtrl_new_pass1->Clear();
					m_textCtrl_new_pass2->Clear();
					
						wxMessageBox(_T("Contrase\u00F1a cambiada"));
				
			EndModal(0);
			}
			else wxMessageBox(_T("Las contrase\u00F1as no coinciden"));
		}
		else wxMessageBox(_T("La contrase\u00F1a debe tener al menos 5 caracteres"));
}
void Hija_GUI_newpass::OnClick_cancelar_newpass( wxCommandEvent& event )
{
	EndModal(0);
}
