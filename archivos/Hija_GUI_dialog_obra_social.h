#ifndef HIJA_GUI_DIALOG_OBRA_SOCIAL_H
#define HIJA_GUI_DIALOG_OBRA_SOCIAL_H
#include "Bases_GUI/GUI_profesional.h"
#include "server.h"

class Hija_GUI_dialog_obra_social : public Base_GUI_dialog_obra_social 
{
public:
	Hija_GUI_dialog_obra_social(wxWindow* parent, Server* _server, Profesional* _profesional);
	
	void OnClick_ingresar_obra_social( wxCommandEvent& event );
	
private:
	Server* m_server           = nullptr;
	Profesional* m_profesional = nullptr;
};

#endif

