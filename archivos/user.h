#ifndef USER_H
#define USER_H
#include <string>

struct Id_Pass
{
    int id = 0;
    char password [32] = "";
};

struct Fecha
{
	int hora = -1, dia = -1, mes = -1, year = -1;
	
	bool operator== (const Fecha& una_fecha){ return hora == una_fecha.hora && 
		dia  == una_fecha.dia  && 
		mes  == una_fecha.mes  && 
		year == una_fecha.year; }
};

struct Turno : public Fecha
{
	Turno(long _dni_paciente, long _dni_profesional, int _hora, int _dia, int _mes, int _year)
		: dni_paciente(_dni_paciente), dni_profesional(_dni_profesional)
	{
		this->hora = _hora; this->dia = _dia; this->mes = _mes; this->year = _year;
	}
	
	long dni_paciente = -1, dni_profesional = -1;
	
	bool confirmado = false, cancelado = false, expirado = false;
	
	bool operator== (const Turno& un_turno)
	{ 
		Turno el_turno(this->dni_paciente, this->dni_profesional, this->hora, this->dia, this->mes, this->year);
		
		return  dni_paciente == un_turno.dni_paciente    && 
			dni_profesional == un_turno.dni_profesional && 
			this->hora == un_turno.hora  && 
			this->dia == un_turno.dia &&
			this->mes == un_turno.mes &&
			this->year== un_turno.year;
	}
};

struct Dia_libre
{
    int dia, mes;
    bool operator== (const Dia_libre& un_dia){ return dia == un_dia.dia && mes == un_dia.mes; }
};

class User
{
public:
    User(std::string _nombre, std::string _apellido);

    void set_nombre(std::string _nombre);
    void set_apellido(std::string _apellido);
	int set_password(std::string _password);
	int set_dni(long _dni);
	void set_dia_nacimiento (int _dia);
	void set_mes_nacimiento (int _mes);
    void set_year_nacimiento(int _year);
    void set_sexo(std::string _sexo);

    std::string get_nombre() const;
    std::string get_apellido() const;
    long get_dni() const;
    std::string get_password() const;
    int get_dia_nacimiento() const;
    int get_mes_nacimiento() const;
    int get_year_nacimiento() const;
    int get_edad() const;
    std::string get_sexo() const;
    std::string get_health_insurance() const;
    virtual std::string get_rol() =0;

protected:
    std::string m_password;
    std::string m_nombre, m_apellido;
    long m_dni = -1;
    int m_dia_nacimiento = -1, m_mes_nacimiento = -1, m_year_nacimiento = -1;
    std::string m_sexo =     "-";
};

#endif
