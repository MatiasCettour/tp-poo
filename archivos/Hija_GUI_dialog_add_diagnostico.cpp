#include "Utils/string_conv.h"
#include <wx/msgdlg.h>
#include "Hija_GUI_dialog_add_diagnostico.h"

Hija_GUI_dialog_add_diagnostico::Hija_GUI_dialog_add_diagnostico(wxWindow* _parent, Paciente* _paciente, Server* _server)
	:	Base_GUI_dialog_add_diagnostico(_parent), m_paciente(_paciente), m_server(_server)
{}

void Hija_GUI_dialog_add_diagnostico::OnClick_aceptar_diagnostico( wxCommandEvent& event )
{
	if (m_textCtrl_add_diagnostico->IsEmpty())
	{
		wxMessageBox ("Escriba un diagnostico");
	}
	else
	{
		m_paciente->add_diagnostico(wx_to_std( m_textCtrl_add_diagnostico->GetValue() ));
		wxMessageBox ("Diagnostico agregado");
		m_server->subir_datos(*m_paciente);
	}
EndModal(0);
}
