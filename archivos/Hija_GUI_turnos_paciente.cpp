#include <wx/msgdlg.h>
#include "Utils/string_conv.h"
#include "Hija_GUI_turnos_paciente.h"

Hija_GUI_turnos_paciente::Hija_GUI_turnos_paciente(wxWindow *parent, Server *_server, Paciente *_paciente, std::vector<Turno> _turnos) 
	: Base_GUI_turnos_paciente(parent), m_server(_server), m_paciente(_paciente), m_turnos(_turnos)
{
	refresh();
}

void Hija_GUI_turnos_paciente::OnClick_cerrar( wxCommandEvent& event )
{
	EndModal(0);
}
void Hija_GUI_turnos_paciente::OnClick_cancelar_turno( wxCommandEvent& event )
{
	int seleccion = m_grilla->GetGridCursorRow();

	if (seleccion > -1)
	{
		long dni_profesional = m_turnos[seleccion].dni_profesional;
		int hora = m_turnos[seleccion].hora;
		int dia  = m_turnos[seleccion].dia;
		int mes  = m_turnos[seleccion].mes;
		int year = m_turnos[seleccion].year;

			m_server->cancelar_turno (m_paciente->get_dni(), dni_profesional, hora, dia, mes, year);
		
				wxMessageBox("Turno cancelado");

					refresh();
	}
}

void Hija_GUI_turnos_paciente::refresh()
{
	m_grilla->ClearGrid();
	
	m_turnos = m_server->request_turnos(m_paciente->get_dni());              
	m_grilla->AppendRows(m_turnos.size());
	m_grilla->SetSelectionMode(wxGrid::wxGridSelectRows);
	
		for (size_t i = 0; i < m_turnos.size(); ++i)//DNI: 21268246. hora: 9. dia: 7. mes: 10. year: 2020.
		{
			if (m_turnos[i].confirmado == true)
			{
				m_grilla->SetCellValue(i, 0, "[CONFIRMADO] "+ std_to_wx(m_server->request_paciente(m_turnos[i].dni_paciente).get_nombre()) +" "+ std_to_wx(m_server->request_paciente(m_turnos[i].dni_paciente).get_apellido()) +". DNI: "+ std::to_string(m_turnos[i].dni_paciente)
											  +". Hora: " + std::to_string(m_turnos[i].hora) +". Dia: "+ std::to_string(m_turnos[i].dia) + ". Mes: " + std::to_string(m_turnos[i].mes) + ". Year: "+ std::to_string(m_turnos[i].year));
			}
			else if (m_turnos[i].cancelado == false and m_turnos[i].expirado == false)
			{
				m_grilla->SetCellValue(i, 0, std_to_wx(m_server->request_profesional(m_turnos[i].dni_profesional).get_nombre()) +" "+ std_to_wx(m_server->request_profesional(m_turnos[i].dni_profesional).get_apellido()) +". DNI: "+ std::to_string(m_turnos[i].dni_profesional)
									   +". Hora: " + std::to_string(m_turnos[i].hora) +". Dia: "+ std::to_string(m_turnos[i].dia) + ". Mes: " + std::to_string(m_turnos[i].mes) + ". Year: "+ std::to_string(m_turnos[i].year));
			}
			/*else if (m_turnos[i].cancelado)
			{
			m_grilla->SetCellValue(i, 0, "[CANCELADO] "+ m_server->request_profesional(m_turnos[i].dni_profesional).get_nombre() +" "+ m_server->request_profesional(m_turnos[i].dni_profesional).get_apellido()
								   +". Hora: " + std::to_string(m_turnos[i].hora) +". Dia: "+ std::to_string(m_turnos[i].dia) + ". Mes: " + std::to_string(m_turnos[i].mes) + ". Year: "+ std::to_string(m_turnos[i].year));
			}*/
			else
			{
				m_grilla->SetCellValue(i, 0, "[EXPIRADO] "+ std_to_wx(m_server->request_profesional(m_turnos[i].dni_profesional).get_nombre()) +" "+ std_to_wx(m_server->request_profesional(m_turnos[i].dni_profesional).get_apellido()) +". DNI: "+ std::to_string(m_turnos[i].dni_profesional)
									   +". Hora: " + std::to_string(m_turnos[i].hora) +". Dia: "+ std::to_string(m_turnos[i].dia) + ". Mes: " + std::to_string(m_turnos[i].mes) + ". Year: "+ std::to_string(m_turnos[i].year));
			}
		}
}
