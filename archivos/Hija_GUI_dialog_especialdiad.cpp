#include "Utils/string_conv.h"
#include <wx/msgdlg.h>
#include "Hija_GUI_dialog_especialdiad.h"

Hija_GUI_dialog_especialdiad::Hija_GUI_dialog_especialdiad(wxWindow* _parent, Server* _server, Profesional* _profesional) 
	: Base_GUI_dialog_especialdiad(_parent), m_server(_server), m_profesional(_profesional)
{
	std::vector<std::string> vstr = m_server->request_especialidades();
	
	for (auto& una_especialidad : vstr)
	{
		m_choice_all_especialdiades->Append(una_especialidad);
	}
}

void Hija_GUI_dialog_especialdiad::OnClick_aceptar_especialidad    ( wxCommandEvent& event )
{
	if ( m_profesional->add_especialidad(wx_to_std( m_choice_all_especialdiades->GetString( m_choice_all_especialdiades->GetSelection() ))) == -1)
	{
		wxMessageBox("Especialidad ya agregada");
	}
	else
	{
		wxMessageBox("Especialidad agregada");
		EndModal(0);
	}
	
}
