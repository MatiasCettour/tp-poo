#ifndef HIJA_LOGIN_H
#define HIJA_LOGIN_H
#include "Bases_GUI/GUI_login.h"
#include "paciente.h"
#include "profesional.h"
#include "server.h"

class Server;

class Hija_GUI_login : public Base_GUI_login 
{
public:
	Hija_GUI_login(Server* server);
	
protected: 
	void OnClick_ingresar ( wxCommandEvent& event );
	void OnClick_crear    ( wxCommandEvent& event );
	void OnClick_cambiar_pass( wxCommandEvent& event );
	
private:
	Server* m_server = nullptr;
};

#endif

