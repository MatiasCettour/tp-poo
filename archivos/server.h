#ifndef SERVER_H
#define SERVER_H
#include "paciente.h"
#include "profesional.h"
#include <fstream>
#include <sstream>
#include <cstring>
#include <map>
#include "Utils/fecha.h"

class Paciente;
class Profesional;

class Server
{
public:
    Server();

    void subir_datos(const Paciente&    _un_paciente);
    void subir_datos(const Profesional& _un_profesional);

	int cambiar_password(long _dni, int _index_rol, std::string _pass);
	
    void update(Paciente    _un_paciente);
    void update(Profesional _un_profesional);

    void add_obra_social(std::string _obra_social);
    std::vector<std::string> request_obras_sociales();
    void add_especialidad (std::string _especialidad);

    void crear_file();

	bool buscar_paciente   (long _dni);
	bool buscar_profesional(long _dni);
	
    std::map<long, Paciente>     request_pacientes();
    std::map<long, Profesional>  request_profesionales();
    std::map<long, std::string>  request_passwords();
	
	Paciente    request_paciente    (long _dni);
	Profesional request_profesional (long _dni);
	std::vector<std::vector<int>> get_horarios_profesional(long _dni_profesional);
	std::vector<Dia_libre> get_dias_no_laborales_profesional(long _dni_profesional);
	
    std::vector<std::string> request_especialidades();
    std::map<std::string, std::vector<long>> request_especialidades_con_dni();
	
	int add_turno        (Turno _new_turno);
	int add_turno        (long _dni_paciente, long _dni_profesional, int _hora, int _dia, int _mes, int _year);
	std::vector<Turno> request_turnos     (long _dni);
	std::vector<Turno> request_turnos (long _dni_profesional, int _mes, int _year);
	std::vector<Turno> request_turnos (long _dni_profesional, int _dia, int _mes, int _year);
	std::vector<Turno> request_turnos (long _dni_profesional, std::string _obra_social);
	std::vector<Turno> request_turnos (long _dni_profesional, long _dni_paciente);
	bool check_turno     (Turno _new_turno);
	bool check_turno     (long _dni_paciente, long _dni_profesional, int _hora, int _dia, int _mes, int _year);
	void confirmar_turno (Turno _un_turno);
	void cancelar_turno  (Turno _un_turno);
	void cancelar_turno  (long _dni_paciente, long _dni_profesional, int _hora, int _dia, int _mes, int _year);
	std::vector<int> request_dias_disponibles (long _dni_profesional, int _mes, int _year);

    ~Server();

private:
    size_t set_cursor(size_t _cursor, std::string _linea);
    std::string m_file_id_pass = "Datos/dni_pass.bin", m_file_datos = "Datos/datos.txt";
    std::map <long, std::string> m_id_pass;
    std::map <long, Paciente>    m_pacientes;
    std::map <long, Profesional> m_profesionales;
	std::vector <Turno>          m_turnos;
    std::vector <std::string>    m_especialidades = {"Adolescencia",
		"Alergia e inmunologia",
		"Anatomia patologica",
		"Andrologia",
		"Anestologia",
		"Audiologia",
		"Branquioterapia",
		"Cardiologia",
		"Cirugia",
		"Dermatologia",
		"Diabetologia",
		"Dietetis",
		"Ecocardiografia",
		"Electrocardiografia",
		"Embarazo de Alto Riesgo",
		"Endocrinologia",
		"Endodoncia",
		"Endoscopia Digestiva",
		"Ergometria",
		"Esterilidad",
		"Estomatologia",
		"Fibrobroncoscopia",
		"Fonoaudiologia y Foniatria",
		"Gastroenterologia",
		"Ginecologia",
		"Hematologia",
		"Hemodialisis",
		"Infectologia",
		"Kinesiologia",
		"Mamografia",
		"Nefrologia",
		"Neumonologia",
		"Neurocirugia",
		"Nutricion",
		"Odontologia General",
		"Oftalmologia",
		"Oncologia",
		"Otorrinolaringologia",
		"Pediatria",
		"Protesis Odontologica",
		"Psicologia",
		"Psiquiatria",
		"Radiologia",
		"Sexologia",
		"Toxicologia",
		"Urodinamia",
		"Virologia"             };
    std::vector <std::string>    m_obras_sociales = {"Iosper","Medicus", "Pami" ,"Swiss Medical"};

    int leer_datos();
};

#endif

//chore|feat|docs|fix|refactor|style|test|sonar|hack|release)
