#ifndef HIJA_HORARIOS_PROFESIONAL_H
#define HIJA_HORARIOS_PROFESIONAL_H
#include "Bases_GUI/GUI_profesional.h"
#include "server.h"

class Server;
class Profesional;

class Hija_GUI_horarios_profesional : public Base_GUI_horarios_profesional
{
public:
	Hija_GUI_horarios_profesional (wxWindow *parent, Server* server, Profesional* profesional);
	
	void OnButton_agregar_dia_no_laboral ( wxCommandEvent& event );
	void OnClick_guardar 				 ( wxCommandEvent& event );
	void OnChoice_horas1 				 ( wxCommandEvent& event );
	void OnClick_eliminar_dia 			 ( wxCommandEvent& event );
	void OnClick_cerrar					 ( wxCommandEvent& event );
		
private:
		Server* m_server            = nullptr;
		Profesional* m_profesional  = nullptr;
		void refresh();
};

#endif

