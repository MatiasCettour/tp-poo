#include "Hija_GUI_dialog_busqueda_avanzada.h"
#include "Utils/string_conv.h"
#include "Utils/fecha.h"

Hija_GUI_dialog_busqueda_avanzada::Hija_GUI_dialog_busqueda_avanzada(wxWindow* _parent, Server* _server, Profesional* _profesional)
	: Base_GUI_dialog_busqueda_avanzada(_parent), m_server(_server), m_profesional(_profesional)
{
	std::string year = std::to_string(get_year_now());
	std::string next_year = std::to_string(get_year_now()+1);
	
	m_choice_year_adv1->Append(year);
	m_choice_year_adv1->Append(next_year);
	m_choice_year_adv1->SetSelection(0);
	
	m_choice_year_adv2->Append(year);
	m_choice_year_adv2->Append(next_year);
	m_choice_year_adv2->SetSelection(0);
	
		std::vector<std::string> obras_sociales = m_server->request_obras_sociales();
		
			for (auto& una_obra_social : obras_sociales)
			{
				m_choice_obra_social_adv->Append(una_obra_social);
			}
			if (obras_sociales.size() > 0) m_choice_obra_social_adv->SetSelection(0);
}
void Hija_GUI_dialog_busqueda_avanzada::OnClick_buscar_mes ( wxCommandEvent& event )
{
	m_grilla_turnos_adv->ClearGrid();
	
		int year = std::stoi(wx_to_std( m_choice_year_adv1->GetString(m_choice_year_adv1->GetSelection())));
		int mes = m_choice_mes_adv1->GetSelection();
		
		if (mes < 1)
		{
			wxMessageBox("Elija un mes");
		}
		else
		{
			std::vector<Turno> turnos = m_server->request_turnos(m_profesional->get_dni(), mes, year);
			
			if (turnos.empty())
			{
				wxMessageBox("No hay turnos en la fecha seleccionada");
			}
			else refresh(turnos);
		}
}

void Hija_GUI_dialog_busqueda_avanzada::OnChoice_mes_adv2 ( wxCommandEvent& event )
{
m_choice_dia_adv->Clear();
	
	int cantidad_dias_mes = 0;
	int mes = m_choice_mes_adv2->GetSelection();
	int year = std::stoi(wx_to_std( m_choice_year_adv2->GetString(m_choice_year_adv2->GetSelection())));
	
	if (mes > 0)
	{
		switch (mes)
		{
		case 1:
			cantidad_dias_mes = 31; break;
			
		case 2:
			
			if (year % 400 == 0 || (year % 4 == 0 && year % 100 != 0) )
				cantidad_dias_mes = 29;
			else
				cantidad_dias_mes = 28;
			break;
		case 3:
			cantidad_dias_mes = 31; break;
			
		case 4:
			cantidad_dias_mes = 30; break;
			
		case 5:
			cantidad_dias_mes = 31; break;
			
		case 6:
			cantidad_dias_mes = 30; break;
			
		case 7:
			cantidad_dias_mes = 31; break;
			
		case 8:
			cantidad_dias_mes = 30; break;
			
		case 9:
			cantidad_dias_mes = 30; break;
			
		case 10:
			cantidad_dias_mes = 31; break;
			
		case 11:
			cantidad_dias_mes = 30; break;
			
		case 12:
			cantidad_dias_mes = 31; break;
		}
		
			for (int dia = 1; dia <= cantidad_dias_mes; ++dia)
			{
				m_choice_dia_adv->Append(std::to_string(dia));	
			}
	}
}
void Hija_GUI_dialog_busqueda_avanzada::OnClick_buscar_dia ( wxCommandEvent& event )
{
	m_grilla_turnos_adv->ClearGrid();
	
		int mes = m_choice_mes_adv2->GetSelection();
		int year = std::stoi(wx_to_std( m_choice_year_adv2->GetString(m_choice_year_adv2->GetSelection())));
		
		if (m_choice_dia_adv->GetSelection() != -1)
		{
			int dia = std::stoi(wx_to_std(m_choice_dia_adv->GetString(m_choice_dia_adv->GetSelection())));
			std::vector<Turno> turnos = m_server->request_turnos(m_profesional->get_dni(), dia, mes, year);
			
			if (turnos.empty())
			{
				wxMessageBox("No hay turnos en la fecha seleccionada");
			}
			else refresh(turnos);
		}
		else wxMessageBox("Elija una fecha");
}
void Hija_GUI_dialog_busqueda_avanzada::OnClick_buscar_obra_social ( wxCommandEvent& event )
{
m_grilla_turnos_adv->ClearGrid();

	std::string obra_social = wx_to_std(m_choice_obra_social_adv->GetString(m_choice_obra_social_adv->GetSelection()));
	std::vector<Turno> turnos = m_server->request_turnos(m_profesional->get_dni(), obra_social);
			
		if (turnos.size() < 1)
		{
			std::string mensaje = "No hay turnos para: " + obra_social;
			wxMessageBox(mensaje);
		}
		else refresh(turnos);
}

void Hija_GUI_dialog_busqueda_avanzada::refresh(const std::vector<Turno> &turnos)
{
	int filas = m_grilla_turnos_adv->GetNumberRows();
	if (filas > 0) m_grilla_turnos_adv->DeleteRows(0,filas,true);
	m_grilla_turnos_adv->AppendRows(turnos.size());
	m_grilla_turnos_adv->SetSelectionMode(wxGrid::wxGridSelectRows);
	
	for (size_t i = 0; i < turnos.size(); ++i)
	{
		if (turnos[i].confirmado == true)
		{
			m_grilla_turnos_adv->SetCellValue(i, 0, "[CONFIRMADO] "+ std_to_wx(m_server->request_paciente(turnos[i].dni_paciente).get_nombre()) +" "+ std_to_wx(m_server->request_paciente(turnos[i].dni_paciente).get_apellido()) +". DNI: "+ std::to_string(turnos[i].dni_paciente)
										  +". Hora: " + std::to_string(turnos[i].hora) +". Dia: "+ std::to_string(turnos[i].dia) + ". Mes: " + std::to_string(turnos[i].mes) + ". Year: "+ std::to_string(turnos[i].year));
		}
		else if (turnos[i].cancelado == false and turnos[i].expirado == false)
		{
			m_grilla_turnos_adv->SetCellValue(i, 0, std_to_wx(m_server->request_paciente(turnos[i].dni_paciente).get_nombre()) +" "+ std_to_wx(m_server->request_paciente(turnos[i].dni_paciente).get_apellido()) +". DNI: "+ std::to_string(turnos[i].dni_paciente)
										  +". Hora: " + std::to_string(turnos[i].hora) +". Dia: "+ std::to_string(turnos[i].dia) + ". Mes: " + std::to_string(turnos[i].mes) + ". Year: "+ std::to_string(turnos[i].year));
		}
		else
		{
			m_grilla_turnos_adv->SetCellValue(i, 0, "[EXPIRADO] "+ std_to_wx(m_server->request_paciente(turnos[i].dni_paciente).get_nombre()) +" "+ std_to_wx(m_server->request_paciente(turnos[i].dni_paciente).get_apellido()) +". DNI: "+ std::to_string(turnos[i].dni_paciente)
										  +". Hora: " + std::to_string(turnos[i].hora) +". Dia: "+ std::to_string(turnos[i].dia) + ". Mes: " + std::to_string(turnos[i].mes) + ". Year: "+ std::to_string(turnos[i].year));
		}
	}
}
