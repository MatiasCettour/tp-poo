///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Oct 26 2018)
// http://www.wxformbuilder.org/
//
// PLEASE DO *NOT* EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#include "GUI_paciente.h"

///////////////////////////////////////////////////////////////////////////

Base_main_GUI_paciente::Base_main_GUI_paciente( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxFrame( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );

	wxBoxSizer* bSizer1;
	bSizer1 = new wxBoxSizer( wxVERTICAL );

	m_label_esp = new wxStaticText( this, wxID_ANY, wxT("Especialidades"), wxDefaultPosition, wxDefaultSize, 0 );
	m_label_esp->Wrap( -1 );
	bSizer1->Add( m_label_esp, 0, wxALL|wxEXPAND, 5 );

	wxArrayString m_choice_especialidadesChoices;
	m_choice_especialidades = new wxChoice( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, m_choice_especialidadesChoices, 0 );
	m_choice_especialidades->SetSelection( 0 );
	bSizer1->Add( m_choice_especialidades, 0, wxALL|wxEXPAND, 5 );

	m_staticline1 = new wxStaticLine( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL );
	bSizer1->Add( m_staticline1, 0, wxEXPAND | wxALL, 5 );

	m_label_prof = new wxStaticText( this, wxID_ANY, wxT("Profesionales"), wxDefaultPosition, wxDefaultSize, 0 );
	m_label_prof->Wrap( -1 );
	bSizer1->Add( m_label_prof, 0, wxALL|wxEXPAND, 5 );

	wxBoxSizer* bSizer12;
	bSizer12 = new wxBoxSizer( wxHORIZONTAL );

	wxArrayString m_choice_profesionalesChoices;
	m_choice_profesionales = new wxChoice( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, m_choice_profesionalesChoices, 0 );
	m_choice_profesionales->SetSelection( 0 );
	bSizer12->Add( m_choice_profesionales, 1, wxALL|wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );

	m_staticText26 = new wxStaticText( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText26->Wrap( -1 );
	bSizer12->Add( m_staticText26, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_button11 = new wxButton( this, wxID_ANY, wxT("     Ver perfil      "), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer12->Add( m_button11, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );


	bSizer1->Add( bSizer12, 1, wxEXPAND, 5 );

	m_staticline31 = new wxStaticLine( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL );
	bSizer1->Add( m_staticline31, 0, wxEXPAND | wxALL, 5 );

	wxBoxSizer* bSizer2;
	bSizer2 = new wxBoxSizer( wxHORIZONTAL );

	m_staticText5 = new wxStaticText( this, wxID_ANY, wxT("Fecha"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText5->Wrap( -1 );
	bSizer2->Add( m_staticText5, 0, wxALL|wxEXPAND, 5 );

	m_staticText11 = new wxStaticText( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText11->Wrap( -1 );
	bSizer2->Add( m_staticText11, 0, wxALL|wxEXPAND, 5 );

	wxArrayString m_choice_yearChoices;
	m_choice_year = new wxChoice( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, m_choice_yearChoices, 0 );
	m_choice_year->SetSelection( 0 );
	bSizer2->Add( m_choice_year, 1, wxALL, 5 );

	m_staticText23 = new wxStaticText( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText23->Wrap( -1 );
	bSizer2->Add( m_staticText23, 0, wxALL, 5 );

	wxArrayString m_choice_mesChoices;
	m_choice_mes = new wxChoice( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, m_choice_mesChoices, 0 );
	m_choice_mes->SetSelection( 0 );
	bSizer2->Add( m_choice_mes, 1, wxALL, 5 );

	m_staticText24 = new wxStaticText( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText24->Wrap( -1 );
	bSizer2->Add( m_staticText24, 0, wxALL, 5 );

	wxArrayString m_choice_diaChoices;
	m_choice_dia = new wxChoice( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, m_choice_diaChoices, 0 );
	m_choice_dia->SetSelection( 0 );
	bSizer2->Add( m_choice_dia, 1, wxALL, 5 );


	bSizer1->Add( bSizer2, 0, wxEXPAND, 5 );

	m_staticline3 = new wxStaticLine( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL );
	bSizer1->Add( m_staticline3, 0, wxEXPAND | wxALL, 5 );

	wxBoxSizer* bSizer3;
	bSizer3 = new wxBoxSizer( wxHORIZONTAL );

	m_staticText10 = new wxStaticText( this, wxID_ANY, wxT("Hora"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText10->Wrap( -1 );
	bSizer3->Add( m_staticText10, 0, wxALL|wxEXPAND, 5 );

	wxArrayString m_choice_horaChoices;
	m_choice_hora = new wxChoice( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, m_choice_horaChoices, 0 );
	m_choice_hora->SetSelection( 0 );
	bSizer3->Add( m_choice_hora, 0, wxALL|wxEXPAND, 5 );


	bSizer1->Add( bSizer3, 0, wxEXPAND, 5 );

	m_boton_reservar_turno = new wxButton( this, wxID_ANY, wxT("Reservar turno"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer1->Add( m_boton_reservar_turno, 0, wxALL|wxEXPAND, 5 );

	m_staticline6 = new wxStaticLine( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL );
	bSizer1->Add( m_staticline6, 0, wxEXPAND | wxALL, 5 );

	wxBoxSizer* bSizer7;
	bSizer7 = new wxBoxSizer( wxHORIZONTAL );

	m_boton_ver_turnos = new wxButton( this, wxID_ANY, wxT("Ver turnos"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer7->Add( m_boton_ver_turnos, 1, wxALL|wxALIGN_BOTTOM, 5 );

	m_staticText9 = new wxStaticText( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText9->Wrap( -1 );
	bSizer7->Add( m_staticText9, 0, wxALL|wxEXPAND, 5 );

	m_boton_config = new wxButton( this, wxID_ANY, wxT("Configurar perfil"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer7->Add( m_boton_config, 1, wxALL|wxALIGN_BOTTOM, 5 );

	m_staticText101 = new wxStaticText( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText101->Wrap( -1 );
	bSizer7->Add( m_staticText101, 0, wxALL|wxEXPAND, 5 );

	m_staticText1011 = new wxStaticText( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText1011->Wrap( -1 );
	bSizer7->Add( m_staticText1011, 0, wxALL, 5 );

	m_cerrar_sesion = new wxButton( this, wxID_ANY, wxT("Cerrar sesion"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer7->Add( m_cerrar_sesion, 1, wxALL|wxALIGN_BOTTOM, 5 );


	bSizer1->Add( bSizer7, 0, wxEXPAND, 5 );


	this->SetSizer( bSizer1 );
	this->Layout();

	this->Centre( wxBOTH );

	// Connect Events
	m_choice_especialidades->Connect( wxEVT_COMMAND_CHOICE_SELECTED, wxCommandEventHandler( Base_main_GUI_paciente::OnChoice_especialidades ), NULL, this );
	m_choice_profesionales->Connect( wxEVT_COMMAND_CHOICE_SELECTED, wxCommandEventHandler( Base_main_GUI_paciente::OnChoice_profesionales ), NULL, this );
	m_choice_profesionales->Connect( wxEVT_LEAVE_WINDOW, wxMouseEventHandler( Base_main_GUI_paciente::OnChoice_profesionales ), NULL, this );
	m_button11->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Base_main_GUI_paciente::OnClick_ver_perfil_profesional ), NULL, this );
	m_choice_year->Connect( wxEVT_COMMAND_CHOICE_SELECTED, wxCommandEventHandler( Base_main_GUI_paciente::OnChoice_year ), NULL, this );
	m_choice_mes->Connect( wxEVT_COMMAND_CHOICE_SELECTED, wxCommandEventHandler( Base_main_GUI_paciente::OnChoice_mes ), NULL, this );
	m_choice_mes->Connect( wxEVT_LEAVE_WINDOW, wxMouseEventHandler( Base_main_GUI_paciente::OnChoice_mes ), NULL, this );
	m_choice_dia->Connect( wxEVT_COMMAND_CHOICE_SELECTED, wxCommandEventHandler( Base_main_GUI_paciente::OnChoice_dia ), NULL, this );
	m_boton_reservar_turno->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Base_main_GUI_paciente::OnClick_reservar ), NULL, this );
	m_boton_ver_turnos->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Base_main_GUI_paciente::OnClick_ver_turnos ), NULL, this );
	m_boton_config->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Base_main_GUI_paciente::OnClick_configurar_perfil ), NULL, this );
	m_cerrar_sesion->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Base_main_GUI_paciente::OnClick_cerrar_sesion ), NULL, this );
}

Base_main_GUI_paciente::~Base_main_GUI_paciente()
{
	// Disconnect Events
	m_choice_especialidades->Disconnect( wxEVT_COMMAND_CHOICE_SELECTED, wxCommandEventHandler( Base_main_GUI_paciente::OnChoice_especialidades ), NULL, this );
	m_choice_profesionales->Disconnect( wxEVT_COMMAND_CHOICE_SELECTED, wxCommandEventHandler( Base_main_GUI_paciente::OnChoice_profesionales ), NULL, this );
	m_choice_profesionales->Disconnect( wxEVT_LEAVE_WINDOW, wxMouseEventHandler( Base_main_GUI_paciente::OnChoice_profesionales ), NULL, this );
	m_button11->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Base_main_GUI_paciente::OnClick_ver_perfil_profesional ), NULL, this );
	m_choice_year->Disconnect( wxEVT_COMMAND_CHOICE_SELECTED, wxCommandEventHandler( Base_main_GUI_paciente::OnChoice_year ), NULL, this );
	m_choice_mes->Disconnect( wxEVT_COMMAND_CHOICE_SELECTED, wxCommandEventHandler( Base_main_GUI_paciente::OnChoice_mes ), NULL, this );
	m_choice_mes->Disconnect( wxEVT_LEAVE_WINDOW, wxMouseEventHandler( Base_main_GUI_paciente::OnChoice_mes ), NULL, this );
	m_choice_dia->Disconnect( wxEVT_COMMAND_CHOICE_SELECTED, wxCommandEventHandler( Base_main_GUI_paciente::OnChoice_dia ), NULL, this );
	m_boton_reservar_turno->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Base_main_GUI_paciente::OnClick_reservar ), NULL, this );
	m_boton_ver_turnos->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Base_main_GUI_paciente::OnClick_ver_turnos ), NULL, this );
	m_boton_config->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Base_main_GUI_paciente::OnClick_configurar_perfil ), NULL, this );
	m_cerrar_sesion->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Base_main_GUI_paciente::OnClick_cerrar_sesion ), NULL, this );

}

Base_GUI_turnos_paciente::Base_GUI_turnos_paciente( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );

	wxBoxSizer* bSizer4;
	bSizer4 = new wxBoxSizer( wxVERTICAL );

	m_staticText3 = new wxStaticText( this, wxID_ANY, wxT("Turnos"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText3->Wrap( -1 );
	bSizer4->Add( m_staticText3, 0, wxALL|wxALIGN_CENTER_HORIZONTAL, 5 );

	m_staticline1 = new wxStaticLine( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL );
	bSizer4->Add( m_staticline1, 0, wxEXPAND | wxALL, 5 );

	m_grilla = new wxGrid( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, 0 );

	// Grid
	m_grilla->CreateGrid( 0, 1 );
	m_grilla->EnableEditing( false );
	m_grilla->EnableGridLines( true );
	m_grilla->EnableDragGridSize( true );
	m_grilla->SetMargins( 0, 0 );

	// Columns
	m_grilla->SetColSize( 0, 817 );
	m_grilla->EnableDragColMove( false );
	m_grilla->EnableDragColSize( true );
	m_grilla->SetColLabelSize( 30 );
	m_grilla->SetColLabelAlignment( wxALIGN_CENTER, wxALIGN_CENTER );

	// Rows
	m_grilla->EnableDragRowSize( true );
	m_grilla->SetRowLabelSize( 80 );
	m_grilla->SetRowLabelAlignment( wxALIGN_CENTER, wxALIGN_CENTER );

	// Label Appearance

	// Cell Defaults
	m_grilla->SetDefaultCellAlignment( wxALIGN_LEFT, wxALIGN_TOP );
	bSizer4->Add( m_grilla, 1, wxALL|wxEXPAND, 5 );

	wxBoxSizer* bSizer6;
	bSizer6 = new wxBoxSizer( wxHORIZONTAL );

	m_button4 = new wxButton( this, wxID_ANY, wxT("Cerrar"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer6->Add( m_button4, 0, wxALL|wxALIGN_BOTTOM, 5 );

	m_staticText7 = new wxStaticText( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText7->Wrap( -1 );
	bSizer6->Add( m_staticText7, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_button5 = new wxButton( this, wxID_ANY, wxT("Cancelar turno seleccionado"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer6->Add( m_button5, 0, wxALL, 5 );


	bSizer4->Add( bSizer6, 0, 0, 5 );


	this->SetSizer( bSizer4 );
	this->Layout();

	this->Centre( wxBOTH );

	// Connect Events
	m_button4->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Base_GUI_turnos_paciente::OnClick_cerrar ), NULL, this );
	m_button5->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Base_GUI_turnos_paciente::OnClick_cancelar_turno ), NULL, this );
}

Base_GUI_turnos_paciente::~Base_GUI_turnos_paciente()
{
	// Disconnect Events
	m_button4->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Base_GUI_turnos_paciente::OnClick_cerrar ), NULL, this );
	m_button5->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Base_GUI_turnos_paciente::OnClick_cancelar_turno ), NULL, this );

}

Base_GUI_perfil_paciente::Base_GUI_perfil_paciente( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );

	wxBoxSizer* OnClick_reservarOnClick_reservar;
	OnClick_reservarOnClick_reservar = new wxBoxSizer( wxVERTICAL );

	m_static_Text = new wxStaticText( this, wxID_ANY, wxT("Nombre"), wxDefaultPosition, wxDefaultSize, 0 );
	m_static_Text->Wrap( -1 );
	OnClick_reservarOnClick_reservar->Add( m_static_Text, 0, wxALL, 5 );

	m_textCtrl_nombre = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	OnClick_reservarOnClick_reservar->Add( m_textCtrl_nombre, 0, wxALL|wxEXPAND, 5 );

	m_staticline6 = new wxStaticLine( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL );
	OnClick_reservarOnClick_reservar->Add( m_staticline6, 0, wxEXPAND | wxALL, 5 );

	m_staticText11 = new wxStaticText( this, wxID_ANY, wxT("Apellido"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText11->Wrap( -1 );
	OnClick_reservarOnClick_reservar->Add( m_staticText11, 0, wxALL, 5 );

	m_textCtrl_apellido = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	OnClick_reservarOnClick_reservar->Add( m_textCtrl_apellido, 0, wxALL|wxEXPAND, 5 );

	m_staticline7 = new wxStaticLine( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL );
	OnClick_reservarOnClick_reservar->Add( m_staticline7, 0, wxEXPAND | wxALL, 5 );

	wxBoxSizer* bSizer8;
	bSizer8 = new wxBoxSizer( wxHORIZONTAL );

	m_staticText12 = new wxStaticText( this, wxID_ANY, wxT("Sexo"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText12->Wrap( -1 );
	bSizer8->Add( m_staticText12, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	wxString m_choice_sexoChoices[] = { wxT("Femenino"), wxT("Masculino"), wxT("Otro") };
	int m_choice_sexoNChoices = sizeof( m_choice_sexoChoices ) / sizeof( wxString );
	m_choice_sexo = new wxChoice( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, m_choice_sexoNChoices, m_choice_sexoChoices, 0 );
	m_choice_sexo->SetSelection( 0 );
	bSizer8->Add( m_choice_sexo, 0, wxALL, 5 );


	OnClick_reservarOnClick_reservar->Add( bSizer8, 0, wxEXPAND, 5 );

	m_staticline9 = new wxStaticLine( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL );
	OnClick_reservarOnClick_reservar->Add( m_staticline9, 0, wxEXPAND | wxALL, 5 );

	m_staticText14 = new wxStaticText( this, wxID_ANY, wxT("Fecha de nacimiento"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText14->Wrap( -1 );
	OnClick_reservarOnClick_reservar->Add( m_staticText14, 0, wxALL, 5 );

	wxBoxSizer* bSizer9;
	bSizer9 = new wxBoxSizer( wxHORIZONTAL );

	m_staticText15 = new wxStaticText( this, wxID_ANY, wxT("Día"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText15->Wrap( -1 );
	bSizer9->Add( m_staticText15, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	wxArrayString m_choice_diaChoices;
	m_choice_dia = new wxChoice( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, m_choice_diaChoices, 0 );
	m_choice_dia->SetSelection( 0 );
	bSizer9->Add( m_choice_dia, 0, wxALL, 5 );

	m_staticText17 = new wxStaticText( this, wxID_ANY, wxT("Mes"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText17->Wrap( -1 );
	bSizer9->Add( m_staticText17, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	wxString m_choice_mesChoices[] = { wxT("Enero"), wxT("Febrero"), wxT("Marzo"), wxT("Abril"), wxT("Mayo"), wxT("Junio"), wxT("Julio"), wxT("Agosto"), wxT("Septiembre"), wxT("Octubre"), wxT("Noviembre"), wxT("Diciembre") };
	int m_choice_mesNChoices = sizeof( m_choice_mesChoices ) / sizeof( wxString );
	m_choice_mes = new wxChoice( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, m_choice_mesNChoices, m_choice_mesChoices, 0 );
	m_choice_mes->SetSelection( 99 );
	bSizer9->Add( m_choice_mes, 0, wxALL|wxEXPAND, 5 );

	m_staticText18 = new wxStaticText( this, wxID_ANY, wxT("Año"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText18->Wrap( -1 );
	bSizer9->Add( m_staticText18, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	wxString m_choice_yearChoices[] = { wxT("2020") };
	int m_choice_yearNChoices = sizeof( m_choice_yearChoices ) / sizeof( wxString );
	m_choice_year = new wxChoice( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, m_choice_yearNChoices, m_choice_yearChoices, 0 );
	m_choice_year->SetSelection( 1 );
	bSizer9->Add( m_choice_year, 0, wxALL, 5 );


	OnClick_reservarOnClick_reservar->Add( bSizer9, 0, wxEXPAND, 5 );

	m_staticline10 = new wxStaticLine( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL );
	OnClick_reservarOnClick_reservar->Add( m_staticline10, 0, wxEXPAND | wxALL, 5 );

	wxBoxSizer* bSizer12;
	bSizer12 = new wxBoxSizer( wxHORIZONTAL );

	m_staticText27 = new wxStaticText( this, wxID_ANY, wxT("Obra social"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText27->Wrap( -1 );
	bSizer12->Add( m_staticText27, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	wxArrayString m_choice_obra_socialChoices;
	m_choice_obra_social = new wxChoice( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, m_choice_obra_socialChoices, 0 );
	m_choice_obra_social->SetSelection( 0 );
	bSizer12->Add( m_choice_obra_social, 1, wxALL|wxALIGN_CENTER_VERTICAL, 5 );


	OnClick_reservarOnClick_reservar->Add( bSizer12, 0, wxEXPAND, 5 );

	m_staticline12 = new wxStaticLine( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL );
	OnClick_reservarOnClick_reservar->Add( m_staticline12, 0, wxEXPAND | wxALL, 5 );

	wxBoxSizer* bSizer11;
	bSizer11 = new wxBoxSizer( wxHORIZONTAL );

	m_staticText26 = new wxStaticText( this, wxID_ANY, wxT("Credencial"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText26->Wrap( -1 );
	bSizer11->Add( m_staticText26, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_textCtrl_credencial = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	bSizer11->Add( m_textCtrl_credencial, 1, wxALL|wxALIGN_CENTER_VERTICAL, 5 );


	OnClick_reservarOnClick_reservar->Add( bSizer11, 1, wxEXPAND, 5 );

	m_staticText23 = new wxStaticText( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText23->Wrap( -1 );
	OnClick_reservarOnClick_reservar->Add( m_staticText23, 0, wxALL, 5 );

	m_staticText24 = new wxStaticText( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText24->Wrap( -1 );
	OnClick_reservarOnClick_reservar->Add( m_staticText24, 0, wxALL, 5 );

	m_button_aceptar = new wxButton( this, wxID_ANY, wxT("Aceptar"), wxDefaultPosition, wxDefaultSize, 0 );
	OnClick_reservarOnClick_reservar->Add( m_button_aceptar, 0, wxALL|wxALIGN_CENTER_HORIZONTAL, 5 );

	m_staticText25 = new wxStaticText( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText25->Wrap( -1 );
	OnClick_reservarOnClick_reservar->Add( m_staticText25, 0, wxALL|wxALIGN_CENTER_HORIZONTAL, 5 );

	m_boton_cancelar = new wxButton( this, wxID_ANY, wxT("Cancelar"), wxDefaultPosition, wxDefaultSize, 0 );
	OnClick_reservarOnClick_reservar->Add( m_boton_cancelar, 0, wxALL|wxALIGN_CENTER_HORIZONTAL, 5 );


	this->SetSizer( OnClick_reservarOnClick_reservar );
	this->Layout();

	this->Centre( wxBOTH );

	// Connect Events
	m_choice_mes->Connect( wxEVT_COMMAND_CHOICE_SELECTED, wxCommandEventHandler( Base_GUI_perfil_paciente::OnChoice_mes ), NULL, this );
	m_choice_mes->Connect( wxEVT_LEAVE_WINDOW, wxMouseEventHandler( Base_GUI_perfil_paciente::OnChoice_mes ), NULL, this );
	m_choice_year->Connect( wxEVT_LEAVE_WINDOW, wxMouseEventHandler( Base_GUI_perfil_paciente::OnChoice_mes ), NULL, this );
	m_button_aceptar->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Base_GUI_perfil_paciente::OnClick_aceptar ), NULL, this );
	m_boton_cancelar->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Base_GUI_perfil_paciente::OnClick_cancelar ), NULL, this );
}

Base_GUI_perfil_paciente::~Base_GUI_perfil_paciente()
{
	// Disconnect Events
	m_choice_mes->Disconnect( wxEVT_COMMAND_CHOICE_SELECTED, wxCommandEventHandler( Base_GUI_perfil_paciente::OnChoice_mes ), NULL, this );
	m_choice_mes->Disconnect( wxEVT_LEAVE_WINDOW, wxMouseEventHandler( Base_GUI_perfil_paciente::OnChoice_mes ), NULL, this );
	m_choice_year->Disconnect( wxEVT_LEAVE_WINDOW, wxMouseEventHandler( Base_GUI_perfil_paciente::OnChoice_mes ), NULL, this );
	m_button_aceptar->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Base_GUI_perfil_paciente::OnClick_aceptar ), NULL, this );
	m_boton_cancelar->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Base_GUI_perfil_paciente::OnClick_cancelar ), NULL, this );

}

Base_GUI_perfil_unProfesional::Base_GUI_perfil_unProfesional( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );

	wxBoxSizer* bSizer3;
	bSizer3 = new wxBoxSizer( wxVERTICAL );

	m_staticText2 = new wxStaticText( this, wxID_ANY, wxT("Configuración de perfil"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText2->Wrap( -1 );
	bSizer3->Add( m_staticText2, 0, wxALL|wxALIGN_CENTER_HORIZONTAL, 5 );

	m_staticText13 = new wxStaticText( this, wxID_ANY, wxT("Nombre"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText13->Wrap( -1 );
	bSizer3->Add( m_staticText13, 0, wxALL, 5 );

	m_textCtrl_nombre = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_READONLY );
	bSizer3->Add( m_textCtrl_nombre, 0, wxALL|wxEXPAND, 5 );

	m_staticline2 = new wxStaticLine( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL );
	bSizer3->Add( m_staticline2, 0, wxEXPAND | wxALL, 5 );

	m_staticText3 = new wxStaticText( this, wxID_ANY, wxT("Apellido"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText3->Wrap( -1 );
	bSizer3->Add( m_staticText3, 0, wxALL, 5 );

	m_textCtrl_apellido = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_READONLY );
	bSizer3->Add( m_textCtrl_apellido, 0, wxALL|wxEXPAND, 5 );

	m_staticline13 = new wxStaticLine( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL );
	bSizer3->Add( m_staticline13, 0, wxEXPAND | wxALL, 5 );

	wxBoxSizer* bSizer6;
	bSizer6 = new wxBoxSizer( wxHORIZONTAL );

	m_staticText15 = new wxStaticText( this, wxID_ANY, wxT("Sexo"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText15->Wrap( -1 );
	bSizer6->Add( m_staticText15, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_textCtrl_sexo = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	bSizer6->Add( m_textCtrl_sexo, 0, wxALL, 5 );


	bSizer3->Add( bSizer6, 0, 0, 5 );

	m_staticline3 = new wxStaticLine( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL );
	bSizer3->Add( m_staticline3, 0, wxEXPAND | wxALL, 5 );

	wxBoxSizer* bSizer5;
	bSizer5 = new wxBoxSizer( wxHORIZONTAL );


	bSizer3->Add( bSizer5, 0, 0, 5 );

	m_staticText4 = new wxStaticText( this, wxID_ANY, wxT("Fecha de nacimiento"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText4->Wrap( -1 );
	bSizer3->Add( m_staticText4, 0, wxALL, 5 );

	wxBoxSizer* bSizer4;
	bSizer4 = new wxBoxSizer( wxHORIZONTAL );

	m_staticText6 = new wxStaticText( this, wxID_ANY, wxT("Año"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText6->Wrap( -1 );
	bSizer4->Add( m_staticText6, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_textCtrl_year_nacimiento = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_READONLY );
	bSizer4->Add( m_textCtrl_year_nacimiento, 0, wxALL, 5 );

	m_staticText7 = new wxStaticText( this, wxID_ANY, wxT("Mes"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText7->Wrap( -1 );
	bSizer4->Add( m_staticText7, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_textCtrl_mes_nacimiento = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_READONLY );
	bSizer4->Add( m_textCtrl_mes_nacimiento, 0, wxALL, 5 );

	m_staticText8 = new wxStaticText( this, wxID_ANY, wxT("Día"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText8->Wrap( -1 );
	bSizer4->Add( m_staticText8, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_textCtrl_dia_nacimiento = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_READONLY );
	bSizer4->Add( m_textCtrl_dia_nacimiento, 0, wxALL, 5 );


	bSizer3->Add( bSizer4, 0, 0, 5 );

	m_staticline5 = new wxStaticLine( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL );
	bSizer3->Add( m_staticline5, 0, wxEXPAND | wxALL, 5 );

	m_staticText12 = new wxStaticText( this, wxID_ANY, wxT("Obras sociales"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText12->Wrap( -1 );
	bSizer3->Add( m_staticText12, 0, wxALL, 5 );

	m_staticline51 = new wxStaticLine( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL );
	bSizer3->Add( m_staticline51, 0, wxEXPAND | wxALL, 5 );

	wxBoxSizer* bSizer61;
	bSizer61 = new wxBoxSizer( wxHORIZONTAL );

	wxArrayString m_choice_obras_socialesChoices;
	m_choice_obras_sociales = new wxChoice( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, m_choice_obras_socialesChoices, 0 );
	m_choice_obras_sociales->SetSelection( 0 );
	bSizer61->Add( m_choice_obras_sociales, 0, wxALL, 5 );


	bSizer3->Add( bSizer61, 0, wxEXPAND, 5 );

	m_staticline8 = new wxStaticLine( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL );
	bSizer3->Add( m_staticline8, 0, wxEXPAND | wxALL, 5 );

	m_staticText24 = new wxStaticText( this, wxID_ANY, wxT("Universidad"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText24->Wrap( -1 );
	bSizer3->Add( m_staticText24, 0, wxALL, 5 );

	m_textCtrl_universidad = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_READONLY );
	bSizer3->Add( m_textCtrl_universidad, 0, wxALL|wxEXPAND, 5 );

	m_staticline14 = new wxStaticLine( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL );
	bSizer3->Add( m_staticline14, 0, wxEXPAND | wxALL, 5 );

	wxBoxSizer* bSizer11;
	bSizer11 = new wxBoxSizer( wxHORIZONTAL );

	m_staticText26 = new wxStaticText( this, wxID_ANY, wxT("Matricula:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText26->Wrap( -1 );
	bSizer11->Add( m_staticText26, 0, wxALL, 5 );

	m_textCtrl_matricula = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_READONLY );
	bSizer11->Add( m_textCtrl_matricula, 1, wxALL, 5 );


	bSizer3->Add( bSizer11, 0, wxEXPAND, 5 );

	m_staticline17 = new wxStaticLine( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL );
	bSizer3->Add( m_staticline17, 0, wxEXPAND | wxALL, 5 );

	wxBoxSizer* bSizer12;
	bSizer12 = new wxBoxSizer( wxHORIZONTAL );

	m_staticText29 = new wxStaticText( this, wxID_ANY, wxT("Especialidades:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText29->Wrap( -1 );
	bSizer12->Add( m_staticText29, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	wxArrayString m_choice_especialdiadesChoices;
	m_choice_especialdiades = new wxChoice( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, m_choice_especialdiadesChoices, 0 );
	m_choice_especialdiades->SetSelection( 0 );
	bSizer12->Add( m_choice_especialdiades, 0, wxALL, 5 );


	bSizer3->Add( bSizer12, 0, wxEXPAND, 5 );

	wxBoxSizer* bSizer36;
	bSizer36 = new wxBoxSizer( wxVERTICAL );

	m_staticText56 = new wxStaticText( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText56->Wrap( -1 );
	bSizer36->Add( m_staticText56, 0, wxALL, 5 );


	bSizer3->Add( bSizer36, 1, wxEXPAND, 5 );


	this->SetSizer( bSizer3 );
	this->Layout();

	this->Centre( wxBOTH );
}

Base_GUI_perfil_unProfesional::~Base_GUI_perfil_unProfesional()
{
}
