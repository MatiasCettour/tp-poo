///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Oct 26 2018)
// http://www.wxformbuilder.org/
//
// PLEASE DO *NOT* EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#pragma once

#include <wx/artprov.h>
#include <wx/xrc/xmlres.h>
#include <wx/string.h>
#include <wx/stattext.h>
#include <wx/gdicmn.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/choice.h>
#include <wx/statline.h>
#include <wx/bitmap.h>
#include <wx/image.h>
#include <wx/icon.h>
#include <wx/button.h>
#include <wx/sizer.h>
#include <wx/frame.h>
#include <wx/grid.h>
#include <wx/dialog.h>
#include <wx/textctrl.h>

///////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////
/// Class Base_main_GUI_paciente
///////////////////////////////////////////////////////////////////////////////
class Base_main_GUI_paciente : public wxFrame
{
	private:

	protected:
		wxStaticText* m_label_esp;
		wxChoice* m_choice_especialidades;
		wxStaticLine* m_staticline1;
		wxStaticText* m_label_prof;
		wxChoice* m_choice_profesionales;
		wxStaticText* m_staticText26;
		wxButton* m_button11;
		wxStaticLine* m_staticline31;
		wxStaticText* m_staticText5;
		wxStaticText* m_staticText11;
		wxChoice* m_choice_year;
		wxStaticText* m_staticText23;
		wxChoice* m_choice_mes;
		wxStaticText* m_staticText24;
		wxChoice* m_choice_dia;
		wxStaticLine* m_staticline3;
		wxStaticText* m_staticText10;
		wxChoice* m_choice_hora;
		wxButton* m_boton_reservar_turno;
		wxStaticLine* m_staticline6;
		wxButton* m_boton_ver_turnos;
		wxStaticText* m_staticText9;
		wxButton* m_boton_config;
		wxStaticText* m_staticText101;
		wxStaticText* m_staticText1011;
		wxButton* m_cerrar_sesion;

		// Virtual event handlers, overide them in your derived class
		virtual void OnChoice_especialidades( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnChoice_profesionales( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnChoice_profesionales( wxMouseEvent& event ) { event.Skip(); }
		virtual void OnClick_ver_perfil_profesional( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnChoice_year( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnChoice_mes( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnChoice_mes( wxMouseEvent& event ) { event.Skip(); }
		virtual void OnChoice_dia( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnClick_reservar( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnClick_ver_turnos( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnClick_configurar_perfil( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnClick_cerrar_sesion( wxCommandEvent& event ) { event.Skip(); }


	public:

		Base_main_GUI_paciente( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxT("Elegir un turno"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 659,402 ), long style = wxDEFAULT_FRAME_STYLE|wxTAB_TRAVERSAL );

		~Base_main_GUI_paciente();

};

///////////////////////////////////////////////////////////////////////////////
/// Class Base_GUI_turnos_paciente
///////////////////////////////////////////////////////////////////////////////
class Base_GUI_turnos_paciente : public wxDialog
{
	private:

	protected:
		wxStaticText* m_staticText3;
		wxStaticLine* m_staticline1;
		wxGrid* m_grilla;
		wxButton* m_button4;
		wxStaticText* m_staticText7;
		wxButton* m_button5;

		// Virtual event handlers, overide them in your derived class
		virtual void OnClick_cerrar( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnClick_cancelar_turno( wxCommandEvent& event ) { event.Skip(); }


	public:

		Base_GUI_turnos_paciente( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxEmptyString, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 923,448 ), long style = wxDEFAULT_DIALOG_STYLE );
		~Base_GUI_turnos_paciente();

};

///////////////////////////////////////////////////////////////////////////////
/// Class Base_GUI_perfil_paciente
///////////////////////////////////////////////////////////////////////////////
class Base_GUI_perfil_paciente : public wxDialog
{
	private:

	protected:
		wxStaticText* m_static_Text;
		wxTextCtrl* m_textCtrl_nombre;
		wxStaticLine* m_staticline6;
		wxStaticText* m_staticText11;
		wxTextCtrl* m_textCtrl_apellido;
		wxStaticLine* m_staticline7;
		wxStaticText* m_staticText12;
		wxChoice* m_choice_sexo;
		wxStaticLine* m_staticline9;
		wxStaticText* m_staticText14;
		wxStaticText* m_staticText15;
		wxChoice* m_choice_dia;
		wxStaticText* m_staticText17;
		wxChoice* m_choice_mes;
		wxStaticText* m_staticText18;
		wxChoice* m_choice_year;
		wxStaticLine* m_staticline10;
		wxStaticText* m_staticText27;
		wxChoice* m_choice_obra_social;
		wxStaticLine* m_staticline12;
		wxStaticText* m_staticText26;
		wxTextCtrl* m_textCtrl_credencial;
		wxStaticText* m_staticText23;
		wxStaticText* m_staticText24;
		wxButton* m_button_aceptar;
		wxStaticText* m_staticText25;
		wxButton* m_boton_cancelar;

		// Virtual event handlers, overide them in your derived class
		virtual void OnChoice_mes( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnChoice_mes( wxMouseEvent& event ) { event.Skip(); }
		virtual void OnClick_aceptar( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnClick_cancelar( wxCommandEvent& event ) { event.Skip(); }


	public:

		Base_GUI_perfil_paciente( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxEmptyString, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 687,613 ), long style = wxDEFAULT_DIALOG_STYLE );
		~Base_GUI_perfil_paciente();

};

///////////////////////////////////////////////////////////////////////////////
/// Class Base_GUI_perfil_unProfesional
///////////////////////////////////////////////////////////////////////////////
class Base_GUI_perfil_unProfesional : public wxDialog
{
	private:

	protected:
		wxStaticText* m_staticText2;
		wxStaticText* m_staticText13;
		wxTextCtrl* m_textCtrl_nombre;
		wxStaticLine* m_staticline2;
		wxStaticText* m_staticText3;
		wxTextCtrl* m_textCtrl_apellido;
		wxStaticLine* m_staticline13;
		wxStaticText* m_staticText15;
		wxTextCtrl* m_textCtrl_sexo;
		wxStaticLine* m_staticline3;
		wxStaticText* m_staticText4;
		wxStaticText* m_staticText6;
		wxTextCtrl* m_textCtrl_year_nacimiento;
		wxStaticText* m_staticText7;
		wxTextCtrl* m_textCtrl_mes_nacimiento;
		wxStaticText* m_staticText8;
		wxTextCtrl* m_textCtrl_dia_nacimiento;
		wxStaticLine* m_staticline5;
		wxStaticText* m_staticText12;
		wxStaticLine* m_staticline51;
		wxChoice* m_choice_obras_sociales;
		wxStaticLine* m_staticline8;
		wxStaticText* m_staticText24;
		wxTextCtrl* m_textCtrl_universidad;
		wxStaticLine* m_staticline14;
		wxStaticText* m_staticText26;
		wxTextCtrl* m_textCtrl_matricula;
		wxStaticLine* m_staticline17;
		wxStaticText* m_staticText29;
		wxChoice* m_choice_especialdiades;
		wxStaticText* m_staticText56;

	public:

		Base_GUI_perfil_unProfesional( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxEmptyString, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 593,653 ), long style = wxDEFAULT_DIALOG_STYLE );
		~Base_GUI_perfil_unProfesional();

};

