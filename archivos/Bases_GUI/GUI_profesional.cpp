///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Oct 26 2018)
// http://www.wxformbuilder.org/
//
// PLEASE DO *NOT* EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#include "GUI_profesional.h"

///////////////////////////////////////////////////////////////////////////

Base_main_GUI_profesional::Base_main_GUI_profesional( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxFrame( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );

	wxBoxSizer* bSizer5;
	bSizer5 = new wxBoxSizer( wxVERTICAL );

	m_staticText3 = new wxStaticText( this, wxID_ANY, wxT("Turnos"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText3->Wrap( -1 );
	bSizer5->Add( m_staticText3, 0, wxALL|wxALIGN_CENTER_HORIZONTAL, 5 );

	m_grilla_turnos = new wxGrid( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, 0 );

	// Grid
	m_grilla_turnos->CreateGrid( 0, 1 );
	m_grilla_turnos->EnableEditing( false );
	m_grilla_turnos->EnableGridLines( true );
	m_grilla_turnos->EnableDragGridSize( false );
	m_grilla_turnos->SetMargins( 0, 0 );

	// Columns
	m_grilla_turnos->SetColSize( 0, 718 );
	m_grilla_turnos->EnableDragColMove( false );
	m_grilla_turnos->EnableDragColSize( true );
	m_grilla_turnos->SetColLabelSize( 30 );
	m_grilla_turnos->SetColLabelAlignment( wxALIGN_CENTER, wxALIGN_CENTER );

	// Rows
	m_grilla_turnos->EnableDragRowSize( true );
	m_grilla_turnos->SetRowLabelSize( 80 );
	m_grilla_turnos->SetRowLabelAlignment( wxALIGN_CENTER, wxALIGN_CENTER );

	// Label Appearance

	// Cell Defaults
	m_grilla_turnos->SetDefaultCellAlignment( wxALIGN_LEFT, wxALIGN_TOP );
	bSizer5->Add( m_grilla_turnos, 1, wxALL|wxEXPAND, 5 );

	m_staticline10 = new wxStaticLine( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL );
	bSizer5->Add( m_staticline10, 0, wxEXPAND | wxALL, 5 );

	m_staticText43 = new wxStaticText( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText43->Wrap( -1 );
	bSizer5->Add( m_staticText43, 0, wxALL, 5 );

	wxBoxSizer* bSizer38;
	bSizer38 = new wxBoxSizer( wxHORIZONTAL );

	m_boton_cancelar_turno = new wxButton( this, wxID_ANY, wxT("Cancelar turno seleccionado"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer38->Add( m_boton_cancelar_turno, 0, wxALL, 5 );

	m_staticText57 = new wxStaticText( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText57->Wrap( -1 );
	bSizer38->Add( m_staticText57, 0, wxALL, 5 );

	m_button27 = new wxButton( this, wxID_ANY, wxT("Confirmar turno seleccionado"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer38->Add( m_button27, 0, wxALL, 5 );


	bSizer5->Add( bSizer38, 0, wxEXPAND, 5 );

	m_staticline23 = new wxStaticLine( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL );
	bSizer5->Add( m_staticline23, 0, wxEXPAND | wxALL, 5 );

	wxBoxSizer* bSizer28;
	bSizer28 = new wxBoxSizer( wxHORIZONTAL );

	m_button15 = new wxButton( this, wxID_ANY, wxT("Buscar paciente"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer28->Add( m_button15, 0, wxALL, 5 );

	m_textCtrl_dni_paciente = new wxTextCtrl( this, wxID_ANY, wxT("Ingrese DNI"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer28->Add( m_textCtrl_dni_paciente, 1, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_button19 = new wxButton( this, wxID_ANY, wxT("Búsqueda avanzada"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer28->Add( m_button19, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );


	bSizer5->Add( bSizer28, 0, wxEXPAND, 5 );

	wxBoxSizer* bSizer2;
	bSizer2 = new wxBoxSizer( wxHORIZONTAL );

	m_boton_cerrar_sesion = new wxButton( this, wxID_ANY, wxT("Cerrar sesión"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer2->Add( m_boton_cerrar_sesion, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_staticText62 = new wxStaticText( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText62->Wrap( -1 );
	bSizer2->Add( m_staticText62, 0, wxALL, 5 );

	m_boton_config_perfil = new wxButton( this, wxID_ANY, wxT("Configurar perfil"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer2->Add( m_boton_config_perfil, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_staticText63 = new wxStaticText( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText63->Wrap( -1 );
	bSizer2->Add( m_staticText63, 0, wxALL, 5 );

	m_boton_horarios = new wxButton( this, wxID_ANY, wxT("Cambiar horarios"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer2->Add( m_boton_horarios, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );


	bSizer5->Add( bSizer2, 1, wxEXPAND, 5 );


	this->SetSizer( bSizer5 );
	this->Layout();

	this->Centre( wxBOTH );

	// Connect Events
	m_boton_cancelar_turno->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Base_main_GUI_profesional::OnClick_cancelar_turno ), NULL, this );
	m_button27->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Base_main_GUI_profesional::OnClick_confirmar_turno ), NULL, this );
	m_button15->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Base_main_GUI_profesional::OnClick_buscar_paciente ), NULL, this );
	m_button19->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Base_main_GUI_profesional::OnClick_busqueda_avanzada ), NULL, this );
	m_boton_cerrar_sesion->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Base_main_GUI_profesional::OnClick_cerrar_sesion ), NULL, this );
	m_boton_config_perfil->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Base_main_GUI_profesional::OnClick_config_perfil ), NULL, this );
	m_boton_horarios->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Base_main_GUI_profesional::OnClick_config_horarios ), NULL, this );
}

Base_main_GUI_profesional::~Base_main_GUI_profesional()
{
	// Disconnect Events
	m_boton_cancelar_turno->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Base_main_GUI_profesional::OnClick_cancelar_turno ), NULL, this );
	m_button27->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Base_main_GUI_profesional::OnClick_confirmar_turno ), NULL, this );
	m_button15->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Base_main_GUI_profesional::OnClick_buscar_paciente ), NULL, this );
	m_button19->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Base_main_GUI_profesional::OnClick_busqueda_avanzada ), NULL, this );
	m_boton_cerrar_sesion->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Base_main_GUI_profesional::OnClick_cerrar_sesion ), NULL, this );
	m_boton_config_perfil->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Base_main_GUI_profesional::OnClick_config_perfil ), NULL, this );
	m_boton_horarios->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Base_main_GUI_profesional::OnClick_config_horarios ), NULL, this );

}

Base_GUI_horarios_profesional::Base_GUI_horarios_profesional( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );

	wxBoxSizer* bSizer3;
	bSizer3 = new wxBoxSizer( wxVERTICAL );

	m_staticText2 = new wxStaticText( this, wxID_ANY, wxT("Agregar horarios semanales "), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText2->Wrap( -1 );
	bSizer3->Add( m_staticText2, 0, wxALL, 5 );

	wxBoxSizer* bSizer4;
	bSizer4 = new wxBoxSizer( wxHORIZONTAL );

	m_staticText4 = new wxStaticText( this, wxID_ANY, wxT("Seleccione día semana"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText4->Wrap( -1 );
	bSizer4->Add( m_staticText4, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	wxString m_choice_dia_semanaChoices[] = { wxT("Domingo"), wxT("Lunes"), wxT("Martes"), wxT("Miercoles"), wxT("Jueves"), wxT("Viernes"), wxT("Sabado") };
	int m_choice_dia_semanaNChoices = sizeof( m_choice_dia_semanaChoices ) / sizeof( wxString );
	m_choice_dia_semana = new wxChoice( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, m_choice_dia_semanaNChoices, m_choice_dia_semanaChoices, 0 );
	m_choice_dia_semana->SetSelection( 0 );
	bSizer4->Add( m_choice_dia_semana, 0, wxALL, 5 );


	bSizer3->Add( bSizer4, 0, wxEXPAND, 5 );

	m_staticline22 = new wxStaticLine( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL );
	bSizer3->Add( m_staticline22, 0, wxEXPAND | wxALL, 5 );

	m_staticText6 = new wxStaticText( this, wxID_ANY, wxT("Seleccione hora (intervalo)"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText6->Wrap( -1 );
	bSizer3->Add( m_staticText6, 0, wxALL, 5 );

	wxBoxSizer* bSizer7;
	bSizer7 = new wxBoxSizer( wxHORIZONTAL );

	m_staticText7 = new wxStaticText( this, wxID_ANY, wxT("Desde"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText7->Wrap( -1 );
	bSizer7->Add( m_staticText7, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	wxArrayString m_choice_horas1Choices;
	m_choice_horas1 = new wxChoice( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, m_choice_horas1Choices, 0 );
	m_choice_horas1->SetSelection( 0 );
	bSizer7->Add( m_choice_horas1, 0, wxALL, 5 );

	m_staticText8 = new wxStaticText( this, wxID_ANY, wxT("Hasta"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText8->Wrap( -1 );
	bSizer7->Add( m_staticText8, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	wxArrayString m_choice_horas2Choices;
	m_choice_horas2 = new wxChoice( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, m_choice_horas2Choices, 0 );
	m_choice_horas2->SetSelection( 0 );
	bSizer7->Add( m_choice_horas2, 0, wxALL, 5 );

	m_staticText28 = new wxStaticText( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText28->Wrap( -1 );
	bSizer7->Add( m_staticText28, 0, wxALL, 5 );

	m_button15 = new wxButton( this, wxID_ANY, wxT("Guardar"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer7->Add( m_button15, 0, wxALL, 5 );


	bSizer3->Add( bSizer7, 0, wxEXPAND, 5 );

	m_staticline7 = new wxStaticLine( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL );
	bSizer3->Add( m_staticline7, 0, wxEXPAND|wxALL, 5 );

	m_staticText11 = new wxStaticText( this, wxID_ANY, wxT("Días no laborales"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText11->Wrap( -1 );
	bSizer3->Add( m_staticText11, 0, wxALL, 5 );

	wxBoxSizer* bSizer16;
	bSizer16 = new wxBoxSizer( wxHORIZONTAL );

	wxArrayString m_choice_dias_libresChoices;
	m_choice_dias_libres = new wxChoice( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, m_choice_dias_libresChoices, 0 );
	m_choice_dias_libres->SetSelection( 0 );
	bSizer16->Add( m_choice_dias_libres, 0, wxALL, 5 );

	m_staticText23 = new wxStaticText( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText23->Wrap( -1 );
	bSizer16->Add( m_staticText23, 0, wxALL, 5 );

	m_button_agregar_dia_no_laboral = new wxButton( this, wxID_ANY, wxT("Agregar otro"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer16->Add( m_button_agregar_dia_no_laboral, 0, wxALL, 5 );

	m_staticText58 = new wxStaticText( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText58->Wrap( -1 );
	bSizer16->Add( m_staticText58, 0, wxALL, 5 );

	m_button26 = new wxButton( this, wxID_ANY, wxT("Eliminar seleccionado"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer16->Add( m_button26, 0, wxALL, 5 );


	bSizer3->Add( bSizer16, 0, wxEXPAND, 5 );

	m_staticline13 = new wxStaticLine( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL );
	bSizer3->Add( m_staticline13, 0, wxEXPAND|wxALL, 5 );

	wxBoxSizer* bSizer41;
	bSizer41 = new wxBoxSizer( wxHORIZONTAL );

	m_grilla_horarios = new wxGrid( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, 0 );

	// Grid
	m_grilla_horarios->CreateGrid( 1, 7 );
	m_grilla_horarios->EnableEditing( false );
	m_grilla_horarios->EnableGridLines( true );
	m_grilla_horarios->EnableDragGridSize( false );
	m_grilla_horarios->SetMargins( 0, 0 );

	// Columns
	m_grilla_horarios->SetColSize( 0, 100 );
	m_grilla_horarios->SetColSize( 1, 100 );
	m_grilla_horarios->SetColSize( 2, 100 );
	m_grilla_horarios->SetColSize( 3, 100 );
	m_grilla_horarios->SetColSize( 4, 100 );
	m_grilla_horarios->SetColSize( 5, 100 );
	m_grilla_horarios->SetColSize( 6, 100 );
	m_grilla_horarios->EnableDragColMove( false );
	m_grilla_horarios->EnableDragColSize( true );
	m_grilla_horarios->SetColLabelSize( 30 );
	m_grilla_horarios->SetColLabelAlignment( wxALIGN_CENTER, wxALIGN_CENTER );

	// Rows
	m_grilla_horarios->EnableDragRowSize( true );
	m_grilla_horarios->SetRowLabelSize( 80 );
	m_grilla_horarios->SetRowLabelAlignment( wxALIGN_CENTER, wxALIGN_CENTER );

	// Label Appearance

	// Cell Defaults
	m_grilla_horarios->SetDefaultCellAlignment( wxALIGN_LEFT, wxALIGN_TOP );
	bSizer41->Add( m_grilla_horarios, 1, wxALL, 5 );


	bSizer3->Add( bSizer41, 1, wxEXPAND, 5 );

	m_button7 = new wxButton( this, wxID_ANY, wxT("Cerrar"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer3->Add( m_button7, 0, wxALL|wxEXPAND, 5 );


	this->SetSizer( bSizer3 );
	this->Layout();

	this->Centre( wxBOTH );

	// Connect Events
	m_choice_dia_semana->Connect( wxEVT_COMMAND_CHOICE_SELECTED, wxCommandEventHandler( Base_GUI_horarios_profesional::OnChoice_dia_semana ), NULL, this );
	m_choice_horas1->Connect( wxEVT_COMMAND_CHOICE_SELECTED, wxCommandEventHandler( Base_GUI_horarios_profesional::OnChoice_horas1 ), NULL, this );
	m_button15->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Base_GUI_horarios_profesional::OnClick_guardar ), NULL, this );
	m_button_agregar_dia_no_laboral->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Base_GUI_horarios_profesional::OnButton_agregar_dia_no_laboral ), NULL, this );
	m_button26->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Base_GUI_horarios_profesional::OnClick_eliminar_dia ), NULL, this );
	m_button7->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Base_GUI_horarios_profesional::OnClick_cerrar ), NULL, this );
}

Base_GUI_horarios_profesional::~Base_GUI_horarios_profesional()
{
	// Disconnect Events
	m_choice_dia_semana->Disconnect( wxEVT_COMMAND_CHOICE_SELECTED, wxCommandEventHandler( Base_GUI_horarios_profesional::OnChoice_dia_semana ), NULL, this );
	m_choice_horas1->Disconnect( wxEVT_COMMAND_CHOICE_SELECTED, wxCommandEventHandler( Base_GUI_horarios_profesional::OnChoice_horas1 ), NULL, this );
	m_button15->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Base_GUI_horarios_profesional::OnClick_guardar ), NULL, this );
	m_button_agregar_dia_no_laboral->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Base_GUI_horarios_profesional::OnButton_agregar_dia_no_laboral ), NULL, this );
	m_button26->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Base_GUI_horarios_profesional::OnClick_eliminar_dia ), NULL, this );
	m_button7->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Base_GUI_horarios_profesional::OnClick_cerrar ), NULL, this );

}

Base_GUI_perfil_profesional::Base_GUI_perfil_profesional( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );

	wxBoxSizer* bSizer3;
	bSizer3 = new wxBoxSizer( wxVERTICAL );

	m_staticText2 = new wxStaticText( this, wxID_ANY, wxT("Configuración de perfil"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText2->Wrap( -1 );
	bSizer3->Add( m_staticText2, 0, wxALL|wxALIGN_CENTER_HORIZONTAL, 5 );

	m_staticText13 = new wxStaticText( this, wxID_ANY, wxT("Nombre"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText13->Wrap( -1 );
	bSizer3->Add( m_staticText13, 0, wxALL, 5 );

	m_textCtrl_nombre = new wxTextCtrl( this, wxID_ANY, wxT("-"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer3->Add( m_textCtrl_nombre, 0, wxALL|wxEXPAND, 5 );

	m_staticline2 = new wxStaticLine( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL );
	bSizer3->Add( m_staticline2, 0, wxEXPAND | wxALL, 5 );

	m_staticText3 = new wxStaticText( this, wxID_ANY, wxT("Apellido"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText3->Wrap( -1 );
	bSizer3->Add( m_staticText3, 0, wxALL, 5 );

	m_textCtrl_apellido = new wxTextCtrl( this, wxID_ANY, wxT("-"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer3->Add( m_textCtrl_apellido, 0, wxALL|wxEXPAND, 5 );

	m_staticline13 = new wxStaticLine( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL );
	bSizer3->Add( m_staticline13, 0, wxEXPAND | wxALL, 5 );

	wxBoxSizer* bSizer6;
	bSizer6 = new wxBoxSizer( wxHORIZONTAL );

	m_staticText15 = new wxStaticText( this, wxID_ANY, wxT("Sexo"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText15->Wrap( -1 );
	bSizer6->Add( m_staticText15, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	wxString m_choice_sexoChoices[] = { wxT("Femenino"), wxT("Masculino"), wxT("Otro") };
	int m_choice_sexoNChoices = sizeof( m_choice_sexoChoices ) / sizeof( wxString );
	m_choice_sexo = new wxChoice( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, m_choice_sexoNChoices, m_choice_sexoChoices, 0 );
	m_choice_sexo->SetSelection( 0 );
	bSizer6->Add( m_choice_sexo, 0, wxALL, 5 );


	bSizer3->Add( bSizer6, 0, 0, 5 );

	m_staticline3 = new wxStaticLine( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL );
	bSizer3->Add( m_staticline3, 0, wxEXPAND | wxALL, 5 );

	wxBoxSizer* bSizer5;
	bSizer5 = new wxBoxSizer( wxHORIZONTAL );


	bSizer3->Add( bSizer5, 0, 0, 5 );

	m_staticText4 = new wxStaticText( this, wxID_ANY, wxT("Fecha de nacimiento"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText4->Wrap( -1 );
	bSizer3->Add( m_staticText4, 0, wxALL, 5 );

	wxBoxSizer* bSizer4;
	bSizer4 = new wxBoxSizer( wxHORIZONTAL );

	m_staticText6 = new wxStaticText( this, wxID_ANY, wxT("Año"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText6->Wrap( -1 );
	bSizer4->Add( m_staticText6, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	wxArrayString m_choice_yearChoices;
	m_choice_year = new wxChoice( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, m_choice_yearChoices, 0 );
	m_choice_year->SetSelection( 0 );
	bSizer4->Add( m_choice_year, 0, wxALL, 5 );

	m_staticText7 = new wxStaticText( this, wxID_ANY, wxT("Mes"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText7->Wrap( -1 );
	bSizer4->Add( m_staticText7, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	wxString m_choice_mesChoices[] = { wxT("1"), wxT("2"), wxT("3"), wxT("4"), wxT("5"), wxT("6"), wxT("7"), wxT("8"), wxT("9"), wxT("10"), wxT("11"), wxT("12") };
	int m_choice_mesNChoices = sizeof( m_choice_mesChoices ) / sizeof( wxString );
	m_choice_mes = new wxChoice( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, m_choice_mesNChoices, m_choice_mesChoices, 0 );
	m_choice_mes->SetSelection( 0 );
	bSizer4->Add( m_choice_mes, 0, wxALL, 5 );

	m_staticText8 = new wxStaticText( this, wxID_ANY, wxT("Día"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText8->Wrap( -1 );
	bSizer4->Add( m_staticText8, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	wxArrayString m_choice_diaChoices;
	m_choice_dia = new wxChoice( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, m_choice_diaChoices, 0 );
	m_choice_dia->SetSelection( 0 );
	bSizer4->Add( m_choice_dia, 0, wxALL, 5 );


	bSizer3->Add( bSizer4, 0, 0, 5 );

	m_staticline5 = new wxStaticLine( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL );
	bSizer3->Add( m_staticline5, 0, wxEXPAND | wxALL, 5 );

	m_staticText12 = new wxStaticText( this, wxID_ANY, wxT("Obras sociales"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText12->Wrap( -1 );
	bSizer3->Add( m_staticText12, 0, wxALL, 5 );

	m_staticline51 = new wxStaticLine( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL );
	bSizer3->Add( m_staticline51, 0, wxEXPAND | wxALL, 5 );

	wxBoxSizer* bSizer61;
	bSizer61 = new wxBoxSizer( wxHORIZONTAL );

	wxArrayString m_choice_obras_socialesChoices;
	m_choice_obras_sociales = new wxChoice( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, m_choice_obras_socialesChoices, 0 );
	m_choice_obras_sociales->SetSelection( 0 );
	bSizer61->Add( m_choice_obras_sociales, 0, wxALL, 5 );

	m_boton_eliminar_pbra_social = new wxButton( this, wxID_ANY, wxT("Eliminar seleccionada"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer61->Add( m_boton_eliminar_pbra_social, 0, wxALL, 5 );

	m_boton_agregar_obra_social = new wxButton( this, wxID_ANY, wxT("Agregar otra"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer61->Add( m_boton_agregar_obra_social, 1, wxALL, 5 );


	bSizer3->Add( bSizer61, 0, wxEXPAND, 5 );

	m_staticline8 = new wxStaticLine( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL );
	bSizer3->Add( m_staticline8, 0, wxEXPAND | wxALL, 5 );

	wxBoxSizer* bSizer361;
	bSizer361 = new wxBoxSizer( wxHORIZONTAL );

	m_staticText57 = new wxStaticText( this, wxID_ANY, wxT("Universidad:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText57->Wrap( -1 );
	bSizer361->Add( m_staticText57, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_textCtrl_universidad = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	bSizer361->Add( m_textCtrl_universidad, 1, wxALL, 5 );


	bSizer3->Add( bSizer361, 0, wxEXPAND, 5 );

	m_staticline14 = new wxStaticLine( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL );
	bSizer3->Add( m_staticline14, 0, wxEXPAND | wxALL, 5 );

	wxBoxSizer* bSizer11;
	bSizer11 = new wxBoxSizer( wxHORIZONTAL );

	m_staticText26 = new wxStaticText( this, wxID_ANY, wxT("Matricula:      "), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText26->Wrap( -1 );
	bSizer11->Add( m_staticText26, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_textCtrl_matricula = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	bSizer11->Add( m_textCtrl_matricula, 1, wxALL, 5 );


	bSizer3->Add( bSizer11, 0, wxEXPAND, 5 );

	m_staticline17 = new wxStaticLine( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL );
	bSizer3->Add( m_staticline17, 0, wxEXPAND | wxALL, 5 );

	wxBoxSizer* bSizer12;
	bSizer12 = new wxBoxSizer( wxHORIZONTAL );

	m_staticText29 = new wxStaticText( this, wxID_ANY, wxT("Especialidades:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText29->Wrap( -1 );
	bSizer12->Add( m_staticText29, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	wxArrayString m_choice_especialdiadesChoices;
	m_choice_especialdiades = new wxChoice( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, m_choice_especialdiadesChoices, 0 );
	m_choice_especialdiades->SetSelection( 0 );
	bSizer12->Add( m_choice_especialdiades, 1, wxALL, 5 );

	m_boton_eliminar_especialidad = new wxButton( this, wxID_ANY, wxT("Eliminar seleccionada"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer12->Add( m_boton_eliminar_especialidad, 0, wxALL, 5 );

	m_boton_agregar_especialidad = new wxButton( this, wxID_ANY, wxT("Agregar otra"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer12->Add( m_boton_agregar_especialidad, 1, wxALL, 5 );


	bSizer3->Add( bSizer12, 0, wxEXPAND, 5 );

	m_staticline19 = new wxStaticLine( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL );
	bSizer3->Add( m_staticline19, 0, wxEXPAND | wxALL, 5 );

	m_button24 = new wxButton( this, wxID_ANY, wxT("Aceptar"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer3->Add( m_button24, 0, wxALL, 5 );


	this->SetSizer( bSizer3 );
	this->Layout();

	this->Centre( wxBOTH );

	// Connect Events
	m_boton_eliminar_pbra_social->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Base_GUI_perfil_profesional::Onbutton_eliminar_obra_social ), NULL, this );
	m_boton_agregar_obra_social->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Base_GUI_perfil_profesional::Onbutton_agregar_obra_social ), NULL, this );
	m_boton_eliminar_especialidad->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Base_GUI_perfil_profesional::OnChoice_eliminar_especialidad ), NULL, this );
	m_boton_agregar_especialidad->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Base_GUI_perfil_profesional::OnChoice_agregar_especialidad ), NULL, this );
	m_button24->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Base_GUI_perfil_profesional::OnClick_aceptar_perfil ), NULL, this );
}

Base_GUI_perfil_profesional::~Base_GUI_perfil_profesional()
{
	// Disconnect Events
	m_boton_eliminar_pbra_social->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Base_GUI_perfil_profesional::Onbutton_eliminar_obra_social ), NULL, this );
	m_boton_agregar_obra_social->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Base_GUI_perfil_profesional::Onbutton_agregar_obra_social ), NULL, this );
	m_boton_eliminar_especialidad->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Base_GUI_perfil_profesional::OnChoice_eliminar_especialidad ), NULL, this );
	m_boton_agregar_especialidad->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Base_GUI_perfil_profesional::OnChoice_agregar_especialidad ), NULL, this );
	m_button24->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Base_GUI_perfil_profesional::OnClick_aceptar_perfil ), NULL, this );

}

Base_GUI_dialog_especialdiad::Base_GUI_dialog_especialdiad( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );

	wxBoxSizer* bSizer20;
	bSizer20 = new wxBoxSizer( wxVERTICAL );

	wxBoxSizer* bSizer14;
	bSizer14 = new wxBoxSizer( wxHORIZONTAL );

	m_staticText22 = new wxStaticText( this, wxID_ANY, wxT("Nombre especialidad    "), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText22->Wrap( -1 );
	bSizer14->Add( m_staticText22, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	wxArrayString m_choice_all_especialdiadesChoices;
	m_choice_all_especialdiades = new wxChoice( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, m_choice_all_especialdiadesChoices, 0 );
	m_choice_all_especialdiades->SetSelection( 0 );
	bSizer14->Add( m_choice_all_especialdiades, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_staticText23 = new wxStaticText( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText23->Wrap( -1 );
	bSizer14->Add( m_staticText23, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	wxBoxSizer* bSizer16;
	bSizer16 = new wxBoxSizer( wxHORIZONTAL );

	m_boton_ingresar_especialdiad = new wxButton( this, wxID_ANY, wxT("Aceptar"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer16->Add( m_boton_ingresar_especialdiad, 0, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5 );


	bSizer14->Add( bSizer16, 0, wxEXPAND, 5 );


	bSizer20->Add( bSizer14, 1, wxEXPAND, 5 );


	this->SetSizer( bSizer20 );
	this->Layout();

	this->Centre( wxBOTH );

	// Connect Events
	m_boton_ingresar_especialdiad->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Base_GUI_dialog_especialdiad::OnClick_aceptar_especialidad ), NULL, this );
}

Base_GUI_dialog_especialdiad::~Base_GUI_dialog_especialdiad()
{
	// Disconnect Events
	m_boton_ingresar_especialdiad->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Base_GUI_dialog_especialdiad::OnClick_aceptar_especialidad ), NULL, this );

}

Base_GUI_dialog_obra_social::Base_GUI_dialog_obra_social( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );

	wxBoxSizer* bSizer20;
	bSizer20 = new wxBoxSizer( wxVERTICAL );

	wxBoxSizer* bSizer14;
	bSizer14 = new wxBoxSizer( wxHORIZONTAL );

	m_staticText22 = new wxStaticText( this, wxID_ANY, wxT("Obra social"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText22->Wrap( -1 );
	bSizer14->Add( m_staticText22, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_staticText23 = new wxStaticText( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText23->Wrap( -1 );
	bSizer14->Add( m_staticText23, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	wxArrayString m_choice_all_obras_socialesChoices;
	m_choice_all_obras_sociales = new wxChoice( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, m_choice_all_obras_socialesChoices, 0 );
	m_choice_all_obras_sociales->SetSelection( 0 );
	bSizer14->Add( m_choice_all_obras_sociales, 1, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	wxBoxSizer* bSizer16;
	bSizer16 = new wxBoxSizer( wxHORIZONTAL );

	m_boton_ingresar_obra_social = new wxButton( this, wxID_ANY, wxT("Aceptar"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer16->Add( m_boton_ingresar_obra_social, 0, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5 );


	bSizer14->Add( bSizer16, 0, wxEXPAND, 5 );


	bSizer20->Add( bSizer14, 1, wxEXPAND, 5 );


	this->SetSizer( bSizer20 );
	this->Layout();

	this->Centre( wxBOTH );

	// Connect Events
	m_boton_ingresar_obra_social->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Base_GUI_dialog_obra_social::OnClick_ingresar_obra_social ), NULL, this );
}

Base_GUI_dialog_obra_social::~Base_GUI_dialog_obra_social()
{
	// Disconnect Events
	m_boton_ingresar_obra_social->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Base_GUI_dialog_obra_social::OnClick_ingresar_obra_social ), NULL, this );

}

Base_GUI_dialog_dias_libres::Base_GUI_dialog_dias_libres( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );

	wxBoxSizer* bSizer17;
	bSizer17 = new wxBoxSizer( wxHORIZONTAL );

	m_staticText23 = new wxStaticText( this, wxID_ANY, wxT("Mes"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText23->Wrap( -1 );
	bSizer17->Add( m_staticText23, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	wxString m_choice_mesChoices[] = { wxT("-"), wxT("1"), wxT("2"), wxT("3"), wxT("4"), wxT("5"), wxT("6"), wxT("7"), wxT("8"), wxT("9"), wxT("10"), wxT("11"), wxT("12") };
	int m_choice_mesNChoices = sizeof( m_choice_mesChoices ) / sizeof( wxString );
	m_choice_mes = new wxChoice( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, m_choice_mesNChoices, m_choice_mesChoices, 0 );
	m_choice_mes->SetSelection( 0 );
	bSizer17->Add( m_choice_mes, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_staticText28 = new wxStaticText( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText28->Wrap( -1 );
	bSizer17->Add( m_staticText28, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_staticText27 = new wxStaticText( this, wxID_ANY, wxT("Día"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText27->Wrap( -1 );
	bSizer17->Add( m_staticText27, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	wxString m_choice_diaChoices[] = { wxT("-              ") };
	int m_choice_diaNChoices = sizeof( m_choice_diaChoices ) / sizeof( wxString );
	m_choice_dia = new wxChoice( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, m_choice_diaNChoices, m_choice_diaChoices, 0 );
	m_choice_dia->SetSelection( 0 );
	bSizer17->Add( m_choice_dia, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_staticText29 = new wxStaticText( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText29->Wrap( -1 );
	bSizer17->Add( m_staticText29, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_button17 = new wxButton( this, wxID_ANY, wxT("Aceptar"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer17->Add( m_button17, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );


	this->SetSizer( bSizer17 );
	this->Layout();

	this->Centre( wxBOTH );

	// Connect Events
	m_choice_mes->Connect( wxEVT_COMMAND_CHOICE_SELECTED, wxCommandEventHandler( Base_GUI_dialog_dias_libres::OnChoice_mes ), NULL, this );
	m_button17->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Base_GUI_dialog_dias_libres::OnClick_add_free_dia ), NULL, this );
}

Base_GUI_dialog_dias_libres::~Base_GUI_dialog_dias_libres()
{
	// Disconnect Events
	m_choice_mes->Disconnect( wxEVT_COMMAND_CHOICE_SELECTED, wxCommandEventHandler( Base_GUI_dialog_dias_libres::OnChoice_mes ), NULL, this );
	m_button17->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Base_GUI_dialog_dias_libres::OnClick_add_free_dia ), NULL, this );

}

Base_GUI_dialog_unPaciente::Base_GUI_dialog_unPaciente( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );

	wxBoxSizer* OnClick_reservarOnClick_reservar;
	OnClick_reservarOnClick_reservar = new wxBoxSizer( wxVERTICAL );

	m_static_Text = new wxStaticText( this, wxID_ANY, wxT("Nombre"), wxDefaultPosition, wxDefaultSize, 0 );
	m_static_Text->Wrap( -1 );
	OnClick_reservarOnClick_reservar->Add( m_static_Text, 0, wxALL, 5 );

	m_textCtrl_nombre_paciente = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_READONLY );
	OnClick_reservarOnClick_reservar->Add( m_textCtrl_nombre_paciente, 0, wxALL|wxEXPAND, 5 );

	m_staticline6 = new wxStaticLine( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL );
	OnClick_reservarOnClick_reservar->Add( m_staticline6, 0, wxEXPAND | wxALL, 5 );

	m_staticText11 = new wxStaticText( this, wxID_ANY, wxT("Apellido"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText11->Wrap( -1 );
	OnClick_reservarOnClick_reservar->Add( m_staticText11, 0, wxALL, 5 );

	m_textCtrl_apellido_paciente = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_READONLY );
	OnClick_reservarOnClick_reservar->Add( m_textCtrl_apellido_paciente, 0, wxALL|wxEXPAND, 5 );

	m_staticline7 = new wxStaticLine( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL );
	OnClick_reservarOnClick_reservar->Add( m_staticline7, 0, wxEXPAND | wxALL, 5 );

	wxBoxSizer* bSizer8;
	bSizer8 = new wxBoxSizer( wxHORIZONTAL );

	m_staticText12 = new wxStaticText( this, wxID_ANY, wxT("Sexo"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText12->Wrap( -1 );
	bSizer8->Add( m_staticText12, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_textCtrl_sexo_paciente = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_READONLY );
	bSizer8->Add( m_textCtrl_sexo_paciente, 0, wxALL, 5 );


	OnClick_reservarOnClick_reservar->Add( bSizer8, 0, wxEXPAND, 5 );

	m_staticline9 = new wxStaticLine( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL );
	OnClick_reservarOnClick_reservar->Add( m_staticline9, 0, wxEXPAND | wxALL, 5 );

	m_staticText14 = new wxStaticText( this, wxID_ANY, wxT("Fecha de nacimiento"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText14->Wrap( -1 );
	OnClick_reservarOnClick_reservar->Add( m_staticText14, 0, wxALL, 5 );

	wxBoxSizer* bSizer9;
	bSizer9 = new wxBoxSizer( wxHORIZONTAL );

	m_staticText15 = new wxStaticText( this, wxID_ANY, wxT("Día"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText15->Wrap( -1 );
	bSizer9->Add( m_staticText15, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_textCtrl_dia_pacietne = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	bSizer9->Add( m_textCtrl_dia_pacietne, 0, wxALL, 5 );

	m_staticText17 = new wxStaticText( this, wxID_ANY, wxT("Mes"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText17->Wrap( -1 );
	bSizer9->Add( m_staticText17, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_textCtrl_mes_paciente = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_READONLY );
	bSizer9->Add( m_textCtrl_mes_paciente, 0, wxALL, 5 );

	m_staticText18 = new wxStaticText( this, wxID_ANY, wxT("Año"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText18->Wrap( -1 );
	bSizer9->Add( m_staticText18, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_textCtrl_year_paciente = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_READONLY );
	bSizer9->Add( m_textCtrl_year_paciente, 0, wxALL, 5 );


	OnClick_reservarOnClick_reservar->Add( bSizer9, 0, wxEXPAND, 5 );

	m_staticline10 = new wxStaticLine( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL );
	OnClick_reservarOnClick_reservar->Add( m_staticline10, 0, wxEXPAND | wxALL, 5 );

	wxBoxSizer* bSizer12;
	bSizer12 = new wxBoxSizer( wxHORIZONTAL );

	m_staticText27 = new wxStaticText( this, wxID_ANY, wxT("Obra social"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText27->Wrap( -1 );
	bSizer12->Add( m_staticText27, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_textCtrl_obra_social_paciente = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_READONLY );
	bSizer12->Add( m_textCtrl_obra_social_paciente, 1, wxALL, 5 );


	OnClick_reservarOnClick_reservar->Add( bSizer12, 0, wxEXPAND, 5 );

	m_staticline12 = new wxStaticLine( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL );
	OnClick_reservarOnClick_reservar->Add( m_staticline12, 0, wxEXPAND | wxALL, 5 );

	wxBoxSizer* bSizer11;
	bSizer11 = new wxBoxSizer( wxHORIZONTAL );

	m_staticText26 = new wxStaticText( this, wxID_ANY, wxT("Credencial"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText26->Wrap( -1 );
	bSizer11->Add( m_staticText26, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_textCtrl_credencial_paciente = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_READONLY );
	bSizer11->Add( m_textCtrl_credencial_paciente, 1, wxALL, 5 );


	OnClick_reservarOnClick_reservar->Add( bSizer11, 0, wxEXPAND, 5 );

	m_staticText23 = new wxStaticText( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText23->Wrap( -1 );
	OnClick_reservarOnClick_reservar->Add( m_staticText23, 0, wxALL, 5 );

	m_grilla_diagnosticos = new wxGrid( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, 0 );

	// Grid
	m_grilla_diagnosticos->CreateGrid( 0, 1 );
	m_grilla_diagnosticos->EnableEditing( false );
	m_grilla_diagnosticos->EnableGridLines( true );
	m_grilla_diagnosticos->EnableDragGridSize( false );
	m_grilla_diagnosticos->SetMargins( 0, 0 );

	// Columns
	m_grilla_diagnosticos->SetColSize( 0, 379 );
	m_grilla_diagnosticos->EnableDragColMove( false );
	m_grilla_diagnosticos->EnableDragColSize( true );
	m_grilla_diagnosticos->SetColLabelSize( 30 );
	m_grilla_diagnosticos->SetColLabelAlignment( wxALIGN_CENTER, wxALIGN_CENTER );

	// Rows
	m_grilla_diagnosticos->EnableDragRowSize( true );
	m_grilla_diagnosticos->SetRowLabelSize( 80 );
	m_grilla_diagnosticos->SetRowLabelAlignment( wxALIGN_CENTER, wxALIGN_CENTER );

	// Label Appearance

	// Cell Defaults
	m_grilla_diagnosticos->SetDefaultCellAlignment( wxALIGN_LEFT, wxALIGN_TOP );
	OnClick_reservarOnClick_reservar->Add( m_grilla_diagnosticos, 1, wxALL|wxEXPAND, 5 );

	wxBoxSizer* bSizer27;
	bSizer27 = new wxBoxSizer( wxHORIZONTAL );

	m_button18 = new wxButton( this, wxID_ANY, wxT("Borrar diagnostico seleccionado"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer27->Add( m_button18, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_staticText40 = new wxStaticText( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText40->Wrap( -1 );
	bSizer27->Add( m_staticText40, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_button19 = new wxButton( this, wxID_ANY, wxT("Agregar diagnostico"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer27->Add( m_button19, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_staticText57 = new wxStaticText( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText57->Wrap( -1 );
	bSizer27->Add( m_staticText57, 0, wxALL, 5 );

	m_button25 = new wxButton( this, wxID_ANY, wxT("Ver turnos"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer27->Add( m_button25, 0, wxALL, 5 );


	OnClick_reservarOnClick_reservar->Add( bSizer27, 0, 0, 5 );


	this->SetSizer( OnClick_reservarOnClick_reservar );
	this->Layout();

	this->Centre( wxBOTH );

	// Connect Events
	m_button18->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Base_GUI_dialog_unPaciente::OnClick_borrar_diagnostico ), NULL, this );
	m_button19->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Base_GUI_dialog_unPaciente::OnClick_agregar_diagnostico ), NULL, this );
	m_button25->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Base_GUI_dialog_unPaciente::OnClick_ver_turnos ), NULL, this );
}

Base_GUI_dialog_unPaciente::~Base_GUI_dialog_unPaciente()
{
	// Disconnect Events
	m_button18->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Base_GUI_dialog_unPaciente::OnClick_borrar_diagnostico ), NULL, this );
	m_button19->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Base_GUI_dialog_unPaciente::OnClick_agregar_diagnostico ), NULL, this );
	m_button25->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Base_GUI_dialog_unPaciente::OnClick_ver_turnos ), NULL, this );

}

Base_GUI_dialog_unPaciente_turnos::Base_GUI_dialog_unPaciente_turnos( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );

	wxBoxSizer* bSizer38;
	bSizer38 = new wxBoxSizer( wxVERTICAL );

	m_grilla_turnos_paciente = new wxGrid( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, 0 );

	// Grid
	m_grilla_turnos_paciente->CreateGrid( 0, 1 );
	m_grilla_turnos_paciente->EnableEditing( false );
	m_grilla_turnos_paciente->EnableGridLines( true );
	m_grilla_turnos_paciente->EnableDragGridSize( false );
	m_grilla_turnos_paciente->SetMargins( 0, 0 );

	// Columns
	m_grilla_turnos_paciente->SetColSize( 0, 617 );
	m_grilla_turnos_paciente->EnableDragColMove( false );
	m_grilla_turnos_paciente->EnableDragColSize( true );
	m_grilla_turnos_paciente->SetColLabelSize( 30 );
	m_grilla_turnos_paciente->SetColLabelAlignment( wxALIGN_CENTER, wxALIGN_CENTER );

	// Rows
	m_grilla_turnos_paciente->EnableDragRowSize( true );
	m_grilla_turnos_paciente->SetRowLabelSize( 80 );
	m_grilla_turnos_paciente->SetRowLabelAlignment( wxALIGN_CENTER, wxALIGN_CENTER );

	// Label Appearance

	// Cell Defaults
	m_grilla_turnos_paciente->SetDefaultCellAlignment( wxALIGN_LEFT, wxALIGN_TOP );
	bSizer38->Add( m_grilla_turnos_paciente, 1, wxALL|wxEXPAND, 5 );


	this->SetSizer( bSizer38 );
	this->Layout();

	this->Centre( wxBOTH );
}

Base_GUI_dialog_unPaciente_turnos::~Base_GUI_dialog_unPaciente_turnos()
{
}

Base_GUI_dialog_add_diagnostico::Base_GUI_dialog_add_diagnostico( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );

	wxBoxSizer* bSizer29;
	bSizer29 = new wxBoxSizer( wxHORIZONTAL );

	m_button18 = new wxButton( this, wxID_ANY, wxT("Agregar"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer29->Add( m_button18, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_textCtrl_add_diagnostico = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	bSizer29->Add( m_textCtrl_add_diagnostico, 1, wxALL|wxALIGN_CENTER_VERTICAL, 5 );


	this->SetSizer( bSizer29 );
	this->Layout();

	this->Centre( wxBOTH );

	// Connect Events
	m_button18->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Base_GUI_dialog_add_diagnostico::OnClick_aceptar_diagnostico ), NULL, this );
}

Base_GUI_dialog_add_diagnostico::~Base_GUI_dialog_add_diagnostico()
{
	// Disconnect Events
	m_button18->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Base_GUI_dialog_add_diagnostico::OnClick_aceptar_diagnostico ), NULL, this );

}

Base_GUI_dialog_busqueda_avanzada::Base_GUI_dialog_busqueda_avanzada( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );

	wxBoxSizer* bSizer37;
	bSizer37 = new wxBoxSizer( wxVERTICAL );

	wxBoxSizer* bSizer38;
	bSizer38 = new wxBoxSizer( wxHORIZONTAL );

	m_grilla_turnos_adv = new wxGrid( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, 0 );

	// Grid
	m_grilla_turnos_adv->CreateGrid( 0, 1 );
	m_grilla_turnos_adv->EnableEditing( false );
	m_grilla_turnos_adv->EnableGridLines( true );
	m_grilla_turnos_adv->EnableDragGridSize( false );
	m_grilla_turnos_adv->SetMargins( 0, 0 );

	// Columns
	m_grilla_turnos_adv->SetColSize( 0, 419 );
	m_grilla_turnos_adv->EnableDragColMove( false );
	m_grilla_turnos_adv->EnableDragColSize( true );
	m_grilla_turnos_adv->SetColLabelSize( 30 );
	m_grilla_turnos_adv->SetColLabelAlignment( wxALIGN_CENTER, wxALIGN_CENTER );

	// Rows
	m_grilla_turnos_adv->EnableDragRowSize( true );
	m_grilla_turnos_adv->SetRowLabelSize( 80 );
	m_grilla_turnos_adv->SetRowLabelAlignment( wxALIGN_CENTER, wxALIGN_CENTER );

	// Label Appearance

	// Cell Defaults
	m_grilla_turnos_adv->SetDefaultCellAlignment( wxALIGN_LEFT, wxALIGN_TOP );
	bSizer38->Add( m_grilla_turnos_adv, 1, wxALL|wxEXPAND, 5 );


	bSizer37->Add( bSizer38, 1, wxEXPAND, 5 );

	wxBoxSizer* bSizer39;
	bSizer39 = new wxBoxSizer( wxVERTICAL );

	wxBoxSizer* bSizer32;
	bSizer32 = new wxBoxSizer( wxHORIZONTAL );

	m_staticText44 = new wxStaticText( this, wxID_ANY, wxT("Buscar turnos por mes:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText44->Wrap( -1 );
	bSizer32->Add( m_staticText44, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_staticText45 = new wxStaticText( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText45->Wrap( -1 );
	bSizer32->Add( m_staticText45, 0, wxALL, 5 );

	wxArrayString m_choice_year_adv1Choices;
	m_choice_year_adv1 = new wxChoice( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, m_choice_year_adv1Choices, 0 );
	m_choice_year_adv1->SetSelection( 0 );
	bSizer32->Add( m_choice_year_adv1, 0, wxALL, 5 );

	m_staticText84 = new wxStaticText( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText84->Wrap( -1 );
	bSizer32->Add( m_staticText84, 0, wxALL, 5 );

	wxString m_choice_mes_adv1Choices[] = { wxT("Elija un mes"), wxT("Enero"), wxT("Febrero"), wxT("Marzo"), wxT("Abril"), wxT("Mayo"), wxT("Junio"), wxT("Julio"), wxT("Agosto"), wxT("Septiembre"), wxT("Octubre"), wxT("Noviembre"), wxT("Diciembre") };
	int m_choice_mes_adv1NChoices = sizeof( m_choice_mes_adv1Choices ) / sizeof( wxString );
	m_choice_mes_adv1 = new wxChoice( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, m_choice_mes_adv1NChoices, m_choice_mes_adv1Choices, 0 );
	m_choice_mes_adv1->SetSelection( 0 );
	bSizer32->Add( m_choice_mes_adv1, 0, wxALL, 5 );

	m_staticText49 = new wxStaticText( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText49->Wrap( -1 );
	bSizer32->Add( m_staticText49, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_boton_buscar_mes = new wxButton( this, wxID_ANY, wxT("Buscar"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer32->Add( m_boton_buscar_mes, 0, wxALL, 5 );


	bSizer39->Add( bSizer32, 0, wxEXPAND, 5 );

	m_staticline19 = new wxStaticLine( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL );
	bSizer39->Add( m_staticline19, 0, wxEXPAND | wxALL, 5 );

	wxBoxSizer* bSizer33;
	bSizer33 = new wxBoxSizer( wxHORIZONTAL );

	m_staticText48 = new wxStaticText( this, wxID_ANY, wxT("Buscar turnos por dia:  "), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText48->Wrap( -1 );
	bSizer33->Add( m_staticText48, 0, wxALL, 5 );

	m_staticText451 = new wxStaticText( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText451->Wrap( -1 );
	bSizer33->Add( m_staticText451, 0, wxALL, 5 );

	wxArrayString m_choice_year_adv2Choices;
	m_choice_year_adv2 = new wxChoice( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, m_choice_year_adv2Choices, 0 );
	m_choice_year_adv2->SetSelection( 0 );
	bSizer33->Add( m_choice_year_adv2, 0, wxALL, 5 );

	m_staticText85 = new wxStaticText( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText85->Wrap( -1 );
	bSizer33->Add( m_staticText85, 0, wxALL, 5 );

	wxString m_choice_mes_adv2Choices[] = { wxT("Elija un mes"), wxT("Enero"), wxT("Febrero"), wxT("Marzo"), wxT("Abril"), wxT("Mayo"), wxT("Junio"), wxT("Julio"), wxT("Agosto"), wxT("Septiembre"), wxT("Octubre"), wxT("Noviembre"), wxT("Diciembre") };
	int m_choice_mes_adv2NChoices = sizeof( m_choice_mes_adv2Choices ) / sizeof( wxString );
	m_choice_mes_adv2 = new wxChoice( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, m_choice_mes_adv2NChoices, m_choice_mes_adv2Choices, 0 );
	m_choice_mes_adv2->SetSelection( 0 );
	bSizer33->Add( m_choice_mes_adv2, 0, wxALL, 5 );

	m_staticText491 = new wxStaticText( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText491->Wrap( -1 );
	bSizer33->Add( m_staticText491, 0, wxALL, 5 );

	wxArrayString m_choice_dia_advChoices;
	m_choice_dia_adv = new wxChoice( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, m_choice_dia_advChoices, 0 );
	m_choice_dia_adv->SetSelection( 0 );
	bSizer33->Add( m_choice_dia_adv, 0, wxALL, 5 );

	m_staticText60 = new wxStaticText( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText60->Wrap( -1 );
	bSizer33->Add( m_staticText60, 0, wxALL, 5 );

	m_boton_buscar_dia = new wxButton( this, wxID_ANY, wxT("Buscar"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer33->Add( m_boton_buscar_dia, 0, wxALL, 5 );


	bSizer39->Add( bSizer33, 0, wxEXPAND, 5 );

	m_staticline22 = new wxStaticLine( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL );
	bSizer39->Add( m_staticline22, 0, wxEXPAND | wxALL, 5 );

	wxBoxSizer* bSizer371;
	bSizer371 = new wxBoxSizer( wxHORIZONTAL );

	m_staticText58 = new wxStaticText( this, wxID_ANY, wxT("Buscar turnos por obra social:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText58->Wrap( -1 );
	bSizer371->Add( m_staticText58, 0, wxALL, 5 );

	m_staticText59 = new wxStaticText( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText59->Wrap( -1 );
	bSizer371->Add( m_staticText59, 0, wxALL, 5 );

	wxArrayString m_choice_obra_social_advChoices;
	m_choice_obra_social_adv = new wxChoice( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, m_choice_obra_social_advChoices, 0 );
	m_choice_obra_social_adv->SetSelection( 0 );
	bSizer371->Add( m_choice_obra_social_adv, 0, wxALL, 5 );

	m_staticText61 = new wxStaticText( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText61->Wrap( -1 );
	bSizer371->Add( m_staticText61, 0, wxALL, 5 );

	m_boton_buscar_obra_social = new wxButton( this, wxID_ANY, wxT("Buscar"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer371->Add( m_boton_buscar_obra_social, 0, wxALL, 5 );


	bSizer39->Add( bSizer371, 1, wxEXPAND, 5 );


	bSizer37->Add( bSizer39, 1, wxEXPAND, 5 );


	this->SetSizer( bSizer37 );
	this->Layout();

	this->Centre( wxBOTH );

	// Connect Events
	m_boton_buscar_mes->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Base_GUI_dialog_busqueda_avanzada::OnClick_buscar_mes ), NULL, this );
	m_choice_mes_adv2->Connect( wxEVT_COMMAND_CHOICE_SELECTED, wxCommandEventHandler( Base_GUI_dialog_busqueda_avanzada::OnChoice_mes_adv2 ), NULL, this );
	m_boton_buscar_dia->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Base_GUI_dialog_busqueda_avanzada::OnClick_buscar_dia ), NULL, this );
	m_boton_buscar_obra_social->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Base_GUI_dialog_busqueda_avanzada::OnClick_buscar_obra_social ), NULL, this );
}

Base_GUI_dialog_busqueda_avanzada::~Base_GUI_dialog_busqueda_avanzada()
{
	// Disconnect Events
	m_boton_buscar_mes->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Base_GUI_dialog_busqueda_avanzada::OnClick_buscar_mes ), NULL, this );
	m_choice_mes_adv2->Disconnect( wxEVT_COMMAND_CHOICE_SELECTED, wxCommandEventHandler( Base_GUI_dialog_busqueda_avanzada::OnChoice_mes_adv2 ), NULL, this );
	m_boton_buscar_dia->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Base_GUI_dialog_busqueda_avanzada::OnClick_buscar_dia ), NULL, this );
	m_boton_buscar_obra_social->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Base_GUI_dialog_busqueda_avanzada::OnClick_buscar_obra_social ), NULL, this );

}
