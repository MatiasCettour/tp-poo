///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Oct 26 2018)
// http://www.wxformbuilder.org/
//
// PLEASE DO *NOT* EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#include "GUI_login.h"

///////////////////////////////////////////////////////////////////////////

Base_GUI_login::Base_GUI_login( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxFrame( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );

	wxBoxSizer* bSizer25;
	bSizer25 = new wxBoxSizer( wxVERTICAL );

	m_staticText27 = new wxStaticText( this, wxID_ANY, wxT("Ingrese sus datos"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText27->Wrap( -1 );
	bSizer25->Add( m_staticText27, 0, wxALL|wxALIGN_CENTER_HORIZONTAL, 5 );

	wxBoxSizer* bSizer26;
	bSizer26 = new wxBoxSizer( wxHORIZONTAL );

	m_staticText32 = new wxStaticText( this, wxID_ANY, wxT("DNI               "), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText32->Wrap( -1 );
	bSizer26->Add( m_staticText32, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_textCtrl_DNI = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	bSizer26->Add( m_textCtrl_DNI, 1, wxALL, 5 );


	bSizer25->Add( bSizer26, 0, wxEXPAND, 5 );

	wxBoxSizer* bSizer28;
	bSizer28 = new wxBoxSizer( wxHORIZONTAL );

	m_staticText33 = new wxStaticText( this, wxID_ANY, wxT("Contraseña"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText33->Wrap( -1 );
	bSizer28->Add( m_staticText33, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_textCtrl_pass = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_PASSWORD );
	bSizer28->Add( m_textCtrl_pass, 1, wxALL, 5 );


	bSizer25->Add( bSizer28, 0, wxEXPAND, 5 );

	wxBoxSizer* bSizer5;
	bSizer5 = new wxBoxSizer( wxHORIZONTAL );

	m_staticText10 = new wxStaticText( this, wxID_ANY, wxT("Soy"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText10->Wrap( -1 );
	bSizer5->Add( m_staticText10, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_staticText12 = new wxStaticText( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText12->Wrap( -1 );
	bSizer5->Add( m_staticText12, 0, wxALL, 5 );

	wxString m_choice_rolChoices[] = { wxT("Paciente"), wxT("Profesional") };
	int m_choice_rolNChoices = sizeof( m_choice_rolChoices ) / sizeof( wxString );
	m_choice_rol = new wxChoice( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, m_choice_rolNChoices, m_choice_rolChoices, 0 );
	m_choice_rol->SetSelection( 1 );
	bSizer5->Add( m_choice_rol, 0, wxALL, 5 );


	bSizer25->Add( bSizer5, 0, wxEXPAND, 5 );

	m_staticline18 = new wxStaticLine( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL );
	bSizer25->Add( m_staticline18, 0, wxEXPAND | wxALL, 5 );

	wxBoxSizer* bSizer12;
	bSizer12 = new wxBoxSizer( wxHORIZONTAL );

	m_boton_ingresar = new wxButton( this, wxID_ANY, wxT("Ingresar"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer12->Add( m_boton_ingresar, 0, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5 );

	m_staticText20 = new wxStaticText( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText20->Wrap( -1 );
	bSizer12->Add( m_staticText20, 0, wxALL, 5 );

	m_boton_crear_cuenta = new wxButton( this, wxID_ANY, wxT("Crear cuenta"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer12->Add( m_boton_crear_cuenta, 0, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5 );

	m_staticText21 = new wxStaticText( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText21->Wrap( -1 );
	bSizer12->Add( m_staticText21, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_button9 = new wxButton( this, wxID_ANY, wxT("Cambiar contraseña"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer12->Add( m_button9, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );


	bSizer25->Add( bSizer12, 1, wxEXPAND, 5 );


	this->SetSizer( bSizer25 );
	this->Layout();

	this->Centre( wxBOTH );

	// Connect Events
	m_boton_ingresar->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Base_GUI_login::OnClick_ingresar ), NULL, this );
	m_boton_crear_cuenta->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Base_GUI_login::OnClick_crear ), NULL, this );
	m_button9->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Base_GUI_login::OnClick_cambiar_pass ), NULL, this );
}

Base_GUI_login::~Base_GUI_login()
{
	// Disconnect Events
	m_boton_ingresar->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Base_GUI_login::OnClick_ingresar ), NULL, this );
	m_boton_crear_cuenta->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Base_GUI_login::OnClick_crear ), NULL, this );
	m_button9->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Base_GUI_login::OnClick_cambiar_pass ), NULL, this );

}

Base_GUI_sign_up::Base_GUI_sign_up( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );

	wxBoxSizer* bSizer4;
	bSizer4 = new wxBoxSizer( wxVERTICAL );

	m_staticText4 = new wxStaticText( this, wxID_ANY, wxT("DNI"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText4->Wrap( -1 );
	bSizer4->Add( m_staticText4, 0, wxALL, 5 );

	m_textCtrl_dni = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	bSizer4->Add( m_textCtrl_dni, 0, wxALL|wxEXPAND, 5 );

	m_staticline2 = new wxStaticLine( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL );
	bSizer4->Add( m_staticline2, 0, wxEXPAND | wxALL, 5 );

	m_staticText7 = new wxStaticText( this, wxID_ANY, wxT("Rol"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText7->Wrap( -1 );
	bSizer4->Add( m_staticText7, 0, wxALL, 5 );

	wxString m_choice_rolChoices[] = { wxT("Paciente"), wxT("Profesional") };
	int m_choice_rolNChoices = sizeof( m_choice_rolChoices ) / sizeof( wxString );
	m_choice_rol = new wxChoice( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, m_choice_rolNChoices, m_choice_rolChoices, 0 );
	m_choice_rol->SetSelection( 0 );
	bSizer4->Add( m_choice_rol, 0, wxALL|wxEXPAND, 5 );

	m_staticline3 = new wxStaticLine( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL );
	bSizer4->Add( m_staticline3, 0, wxEXPAND | wxALL, 5 );

	wxGridSizer* gSizer1;
	gSizer1 = new wxGridSizer( 0, 2, 0, 0 );

	m_staticText121 = new wxStaticText( this, wxID_ANY, wxT("Nombre"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText121->Wrap( -1 );
	gSizer1->Add( m_staticText121, 0, wxALL, 5 );

	m_staticText13 = new wxStaticText( this, wxID_ANY, wxT("Apellido"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText13->Wrap( -1 );
	gSizer1->Add( m_staticText13, 0, wxALL, 5 );

	m_textCtrl_nombre = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	gSizer1->Add( m_textCtrl_nombre, 0, wxALL|wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );

	m_textCtrl_apellido = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	gSizer1->Add( m_textCtrl_apellido, 0, wxALL|wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );


	bSizer4->Add( gSizer1, 0, wxEXPAND, 5 );

	m_staticline4 = new wxStaticLine( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL );
	bSizer4->Add( m_staticline4, 0, wxEXPAND | wxALL, 5 );

	m_staticText20 = new wxStaticText( this, wxID_ANY, wxT("Fecha de nacimiento"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText20->Wrap( -1 );
	bSizer4->Add( m_staticText20, 0, wxALL, 5 );

	wxBoxSizer* bSizer42;
	bSizer42 = new wxBoxSizer( wxHORIZONTAL );


	bSizer4->Add( bSizer42, 1, wxEXPAND, 5 );

	wxBoxSizer* bSizer41;
	bSizer41 = new wxBoxSizer( wxHORIZONTAL );

	m_staticText61 = new wxStaticText( this, wxID_ANY, wxT("Año"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText61->Wrap( -1 );
	bSizer41->Add( m_staticText61, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	wxArrayString m_choice_yearChoices;
	m_choice_year = new wxChoice( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, m_choice_yearChoices, 0 );
	m_choice_year->SetSelection( 0 );
	bSizer41->Add( m_choice_year, 0, wxALL, 5 );

	m_staticText71 = new wxStaticText( this, wxID_ANY, wxT("Mes"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText71->Wrap( -1 );
	bSizer41->Add( m_staticText71, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	wxArrayString m_choice_mesChoices;
	m_choice_mes = new wxChoice( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, m_choice_mesChoices, 0 );
	m_choice_mes->SetSelection( 0 );
	bSizer41->Add( m_choice_mes, 0, wxALL, 5 );

	m_staticText8 = new wxStaticText( this, wxID_ANY, wxT("Día"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText8->Wrap( -1 );
	bSizer41->Add( m_staticText8, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	wxArrayString m_choice_diaChoices;
	m_choice_dia = new wxChoice( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, m_choice_diaChoices, 0 );
	m_choice_dia->SetSelection( 0 );
	bSizer41->Add( m_choice_dia, 0, wxALL, 5 );


	bSizer4->Add( bSizer41, 0, 0, 5 );

	m_staticline8 = new wxStaticLine( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL );
	bSizer4->Add( m_staticline8, 0, wxEXPAND | wxALL, 5 );

	m_staticText51 = new wxStaticText( this, wxID_ANY, wxT("Ingrese contraseña"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText51->Wrap( -1 );
	bSizer4->Add( m_staticText51, 0, wxALL, 5 );

	m_textCtrl_pass1 = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_PASSWORD );
	bSizer4->Add( m_textCtrl_pass1, 0, wxALL|wxEXPAND, 5 );

	m_staticText6 = new wxStaticText( this, wxID_ANY, wxT("Confirme contraseña"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText6->Wrap( -1 );
	bSizer4->Add( m_staticText6, 0, wxALL, 5 );

	m_textCtrl_pass2 = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_PASSWORD );
	bSizer4->Add( m_textCtrl_pass2, 0, wxALL|wxEXPAND, 5 );

	m_staticText12 = new wxStaticText( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText12->Wrap( -1 );
	bSizer4->Add( m_staticText12, 0, wxALL, 5 );

	m_boton_crear = new wxButton( this, wxID_ANY, wxT("Crear cuenta"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer4->Add( m_boton_crear, 0, wxALL, 5 );


	this->SetSizer( bSizer4 );
	this->Layout();

	this->Centre( wxBOTH );

	// Connect Events
	m_choice_year->Connect( wxEVT_COMMAND_CHOICE_SELECTED, wxCommandEventHandler( Base_GUI_sign_up::OnChoice_year ), NULL, this );
	m_choice_mes->Connect( wxEVT_COMMAND_CHOICE_SELECTED, wxCommandEventHandler( Base_GUI_sign_up::OnChoice_mes ), NULL, this );
	m_boton_crear->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Base_GUI_sign_up::OnClick_crear_cuenta ), NULL, this );
}

Base_GUI_sign_up::~Base_GUI_sign_up()
{
	// Disconnect Events
	m_choice_year->Disconnect( wxEVT_COMMAND_CHOICE_SELECTED, wxCommandEventHandler( Base_GUI_sign_up::OnChoice_year ), NULL, this );
	m_choice_mes->Disconnect( wxEVT_COMMAND_CHOICE_SELECTED, wxCommandEventHandler( Base_GUI_sign_up::OnChoice_mes ), NULL, this );
	m_boton_crear->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Base_GUI_sign_up::OnClick_crear_cuenta ), NULL, this );

}

Base_GUI_newpass::Base_GUI_newpass( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );

	wxBoxSizer* bSizer12;
	bSizer12 = new wxBoxSizer( wxVERTICAL );

	wxBoxSizer* bSizer18;
	bSizer18 = new wxBoxSizer( wxHORIZONTAL );

	m_staticText37 = new wxStaticText( this, wxID_ANY, wxT("Nueva contraseña"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText37->Wrap( -1 );
	bSizer18->Add( m_staticText37, 0, wxALL, 5 );

	m_staticText38 = new wxStaticText( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText38->Wrap( -1 );
	bSizer18->Add( m_staticText38, 0, wxALL, 5 );

	m_textCtrl_new_pass1 = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_PASSWORD );
	bSizer18->Add( m_textCtrl_new_pass1, 1, wxALL, 5 );

	wxBoxSizer* bSizer20;
	bSizer20 = new wxBoxSizer( wxVERTICAL );


	bSizer18->Add( bSizer20, 1, wxEXPAND, 5 );


	bSizer12->Add( bSizer18, 0, wxEXPAND, 5 );

	wxBoxSizer* bSizer21;
	bSizer21 = new wxBoxSizer( wxHORIZONTAL );

	m_staticText40 = new wxStaticText( this, wxID_ANY, wxT("Reingrese nueva contraseña"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText40->Wrap( -1 );
	bSizer21->Add( m_staticText40, 0, wxALL, 5 );

	m_staticText41 = new wxStaticText( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText41->Wrap( -1 );
	bSizer21->Add( m_staticText41, 0, wxALL, 5 );

	m_textCtrl_new_pass2 = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_PASSWORD );
	bSizer21->Add( m_textCtrl_new_pass2, 1, wxALL, 5 );


	bSizer12->Add( bSizer21, 1, wxEXPAND, 5 );

	wxBoxSizer* bSizer22;
	bSizer22 = new wxBoxSizer( wxHORIZONTAL );

	m_button20 = new wxButton( this, wxID_ANY, wxT("Cambiar contraseña"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer22->Add( m_button20, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_staticText42 = new wxStaticText( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText42->Wrap( -1 );
	bSizer22->Add( m_staticText42, 0, wxALL, 5 );

	m_button21 = new wxButton( this, wxID_ANY, wxT("Cancelar"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer22->Add( m_button21, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );


	bSizer12->Add( bSizer22, 1, wxEXPAND, 5 );


	this->SetSizer( bSizer12 );
	this->Layout();

	this->Centre( wxBOTH );

	// Connect Events
	m_button20->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Base_GUI_newpass::OnClick_aceptar_newpass ), NULL, this );
	m_button21->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Base_GUI_newpass::OnClick_cancelar_newpass ), NULL, this );
}

Base_GUI_newpass::~Base_GUI_newpass()
{
	// Disconnect Events
	m_button20->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Base_GUI_newpass::OnClick_aceptar_newpass ), NULL, this );
	m_button21->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( Base_GUI_newpass::OnClick_cancelar_newpass ), NULL, this );

}
