///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Oct 26 2018)
// http://www.wxformbuilder.org/
//
// PLEASE DO *NOT* EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#pragma once

#include <wx/artprov.h>
#include <wx/xrc/xmlres.h>
#include <wx/string.h>
#include <wx/stattext.h>
#include <wx/gdicmn.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/grid.h>
#include <wx/statline.h>
#include <wx/bitmap.h>
#include <wx/image.h>
#include <wx/icon.h>
#include <wx/button.h>
#include <wx/sizer.h>
#include <wx/textctrl.h>
#include <wx/frame.h>
#include <wx/choice.h>
#include <wx/dialog.h>

///////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////
/// Class Base_main_GUI_profesional
///////////////////////////////////////////////////////////////////////////////
class Base_main_GUI_profesional : public wxFrame
{
	private:

	protected:
		wxStaticText* m_staticText3;
		wxGrid* m_grilla_turnos;
		wxStaticLine* m_staticline10;
		wxStaticText* m_staticText43;
		wxButton* m_boton_cancelar_turno;
		wxStaticText* m_staticText57;
		wxButton* m_button27;
		wxStaticLine* m_staticline23;
		wxButton* m_button15;
		wxTextCtrl* m_textCtrl_dni_paciente;
		wxButton* m_button19;
		wxButton* m_boton_cerrar_sesion;
		wxStaticText* m_staticText62;
		wxButton* m_boton_config_perfil;
		wxStaticText* m_staticText63;
		wxButton* m_boton_horarios;

		// Virtual event handlers, overide them in your derived class
		virtual void OnClick_cancelar_turno( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnClick_confirmar_turno( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnClick_buscar_paciente( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnClick_busqueda_avanzada( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnClick_cerrar_sesion( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnClick_config_perfil( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnClick_config_horarios( wxCommandEvent& event ) { event.Skip(); }


	public:

		Base_main_GUI_profesional( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxEmptyString, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 802,427 ), long style = wxDEFAULT_FRAME_STYLE|wxTAB_TRAVERSAL );

		~Base_main_GUI_profesional();

};

///////////////////////////////////////////////////////////////////////////////
/// Class Base_GUI_horarios_profesional
///////////////////////////////////////////////////////////////////////////////
class Base_GUI_horarios_profesional : public wxDialog
{
	private:

	protected:
		wxStaticText* m_staticText2;
		wxStaticText* m_staticText4;
		wxChoice* m_choice_dia_semana;
		wxStaticLine* m_staticline22;
		wxStaticText* m_staticText6;
		wxStaticText* m_staticText7;
		wxChoice* m_choice_horas1;
		wxStaticText* m_staticText8;
		wxChoice* m_choice_horas2;
		wxStaticText* m_staticText28;
		wxButton* m_button15;
		wxStaticLine* m_staticline7;
		wxStaticText* m_staticText11;
		wxChoice* m_choice_dias_libres;
		wxStaticText* m_staticText23;
		wxButton* m_button_agregar_dia_no_laboral;
		wxStaticText* m_staticText58;
		wxButton* m_button26;
		wxStaticLine* m_staticline13;
		wxGrid* m_grilla_horarios;
		wxButton* m_button7;

		// Virtual event handlers, overide them in your derived class
		virtual void OnChoice_dia_semana( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnChoice_horas1( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnClick_guardar( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnButton_agregar_dia_no_laboral( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnClick_eliminar_dia( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnClick_cerrar( wxCommandEvent& event ) { event.Skip(); }


	public:

		Base_GUI_horarios_profesional( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxT("Cambiar horarios"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 896,501 ), long style = wxDEFAULT_DIALOG_STYLE );
		~Base_GUI_horarios_profesional();

};

///////////////////////////////////////////////////////////////////////////////
/// Class Base_GUI_perfil_profesional
///////////////////////////////////////////////////////////////////////////////
class Base_GUI_perfil_profesional : public wxDialog
{
	private:

	protected:
		wxStaticText* m_staticText2;
		wxStaticText* m_staticText13;
		wxTextCtrl* m_textCtrl_nombre;
		wxStaticLine* m_staticline2;
		wxStaticText* m_staticText3;
		wxTextCtrl* m_textCtrl_apellido;
		wxStaticLine* m_staticline13;
		wxStaticText* m_staticText15;
		wxChoice* m_choice_sexo;
		wxStaticLine* m_staticline3;
		wxStaticText* m_staticText4;
		wxStaticText* m_staticText6;
		wxChoice* m_choice_year;
		wxStaticText* m_staticText7;
		wxChoice* m_choice_mes;
		wxStaticText* m_staticText8;
		wxChoice* m_choice_dia;
		wxStaticLine* m_staticline5;
		wxStaticText* m_staticText12;
		wxStaticLine* m_staticline51;
		wxChoice* m_choice_obras_sociales;
		wxButton* m_boton_eliminar_pbra_social;
		wxButton* m_boton_agregar_obra_social;
		wxStaticLine* m_staticline8;
		wxStaticText* m_staticText57;
		wxTextCtrl* m_textCtrl_universidad;
		wxStaticLine* m_staticline14;
		wxStaticText* m_staticText26;
		wxTextCtrl* m_textCtrl_matricula;
		wxStaticLine* m_staticline17;
		wxStaticText* m_staticText29;
		wxChoice* m_choice_especialdiades;
		wxButton* m_boton_eliminar_especialidad;
		wxButton* m_boton_agregar_especialidad;
		wxStaticLine* m_staticline19;
		wxButton* m_button24;

		// Virtual event handlers, overide them in your derived class
		virtual void Onbutton_eliminar_obra_social( wxCommandEvent& event ) { event.Skip(); }
		virtual void Onbutton_agregar_obra_social( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnChoice_eliminar_especialidad( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnChoice_agregar_especialidad( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnClick_aceptar_perfil( wxCommandEvent& event ) { event.Skip(); }


	public:

		Base_GUI_perfil_profesional( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxT("Configuración de perfil"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 594,675 ), long style = wxDEFAULT_DIALOG_STYLE );
		~Base_GUI_perfil_profesional();

};

///////////////////////////////////////////////////////////////////////////////
/// Class Base_GUI_dialog_especialdiad
///////////////////////////////////////////////////////////////////////////////
class Base_GUI_dialog_especialdiad : public wxDialog
{
	private:

	protected:
		wxStaticText* m_staticText22;
		wxChoice* m_choice_all_especialdiades;
		wxStaticText* m_staticText23;
		wxButton* m_boton_ingresar_especialdiad;

		// Virtual event handlers, overide them in your derived class
		virtual void OnClick_aceptar_especialidad( wxCommandEvent& event ) { event.Skip(); }


	public:

		Base_GUI_dialog_especialdiad( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxEmptyString, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 492,121 ), long style = wxDEFAULT_DIALOG_STYLE );
		~Base_GUI_dialog_especialdiad();

};

///////////////////////////////////////////////////////////////////////////////
/// Class Base_GUI_dialog_obra_social
///////////////////////////////////////////////////////////////////////////////
class Base_GUI_dialog_obra_social : public wxDialog
{
	private:

	protected:
		wxStaticText* m_staticText22;
		wxStaticText* m_staticText23;
		wxChoice* m_choice_all_obras_sociales;
		wxButton* m_boton_ingresar_obra_social;

		// Virtual event handlers, overide them in your derived class
		virtual void OnClick_ingresar_obra_social( wxCommandEvent& event ) { event.Skip(); }


	public:

		Base_GUI_dialog_obra_social( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxEmptyString, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 492,121 ), long style = wxDEFAULT_DIALOG_STYLE );
		~Base_GUI_dialog_obra_social();

};

///////////////////////////////////////////////////////////////////////////////
/// Class Base_GUI_dialog_dias_libres
///////////////////////////////////////////////////////////////////////////////
class Base_GUI_dialog_dias_libres : public wxDialog
{
	private:

	protected:
		wxStaticText* m_staticText23;
		wxChoice* m_choice_mes;
		wxStaticText* m_staticText28;
		wxStaticText* m_staticText27;
		wxChoice* m_choice_dia;
		wxStaticText* m_staticText29;
		wxButton* m_button17;

		// Virtual event handlers, overide them in your derived class
		virtual void OnChoice_mes( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnClick_add_free_dia( wxCommandEvent& event ) { event.Skip(); }


	public:

		Base_GUI_dialog_dias_libres( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxEmptyString, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 473,127 ), long style = wxDEFAULT_DIALOG_STYLE );
		~Base_GUI_dialog_dias_libres();

};

///////////////////////////////////////////////////////////////////////////////
/// Class Base_GUI_dialog_unPaciente
///////////////////////////////////////////////////////////////////////////////
class Base_GUI_dialog_unPaciente : public wxDialog
{
	private:

	protected:
		wxStaticText* m_static_Text;
		wxTextCtrl* m_textCtrl_nombre_paciente;
		wxStaticLine* m_staticline6;
		wxStaticText* m_staticText11;
		wxTextCtrl* m_textCtrl_apellido_paciente;
		wxStaticLine* m_staticline7;
		wxStaticText* m_staticText12;
		wxTextCtrl* m_textCtrl_sexo_paciente;
		wxStaticLine* m_staticline9;
		wxStaticText* m_staticText14;
		wxStaticText* m_staticText15;
		wxTextCtrl* m_textCtrl_dia_pacietne;
		wxStaticText* m_staticText17;
		wxTextCtrl* m_textCtrl_mes_paciente;
		wxStaticText* m_staticText18;
		wxTextCtrl* m_textCtrl_year_paciente;
		wxStaticLine* m_staticline10;
		wxStaticText* m_staticText27;
		wxTextCtrl* m_textCtrl_obra_social_paciente;
		wxStaticLine* m_staticline12;
		wxStaticText* m_staticText26;
		wxTextCtrl* m_textCtrl_credencial_paciente;
		wxStaticText* m_staticText23;
		wxGrid* m_grilla_diagnosticos;
		wxButton* m_button18;
		wxStaticText* m_staticText40;
		wxButton* m_button19;
		wxStaticText* m_staticText57;
		wxButton* m_button25;

		// Virtual event handlers, overide them in your derived class
		virtual void OnClick_borrar_diagnostico( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnClick_agregar_diagnostico( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnClick_ver_turnos( wxCommandEvent& event ) { event.Skip(); }


	public:

		Base_GUI_dialog_unPaciente( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxEmptyString, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 931,637 ), long style = wxDEFAULT_DIALOG_STYLE );
		~Base_GUI_dialog_unPaciente();

};

///////////////////////////////////////////////////////////////////////////////
/// Class Base_GUI_dialog_unPaciente_turnos
///////////////////////////////////////////////////////////////////////////////
class Base_GUI_dialog_unPaciente_turnos : public wxDialog
{
	private:

	protected:
		wxGrid* m_grilla_turnos_paciente;

	public:

		Base_GUI_dialog_unPaciente_turnos( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxT("Turnos"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 697,466 ), long style = wxDEFAULT_DIALOG_STYLE );
		~Base_GUI_dialog_unPaciente_turnos();

};

///////////////////////////////////////////////////////////////////////////////
/// Class Base_GUI_dialog_add_diagnostico
///////////////////////////////////////////////////////////////////////////////
class Base_GUI_dialog_add_diagnostico : public wxDialog
{
	private:

	protected:
		wxButton* m_button18;
		wxTextCtrl* m_textCtrl_add_diagnostico;

		// Virtual event handlers, overide them in your derived class
		virtual void OnClick_aceptar_diagnostico( wxCommandEvent& event ) { event.Skip(); }


	public:

		Base_GUI_dialog_add_diagnostico( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxEmptyString, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 875,113 ), long style = wxDEFAULT_DIALOG_STYLE );
		~Base_GUI_dialog_add_diagnostico();

};

///////////////////////////////////////////////////////////////////////////////
/// Class Base_GUI_dialog_busqueda_avanzada
///////////////////////////////////////////////////////////////////////////////
class Base_GUI_dialog_busqueda_avanzada : public wxDialog
{
	private:

	protected:
		wxGrid* m_grilla_turnos_adv;
		wxStaticText* m_staticText44;
		wxStaticText* m_staticText45;
		wxChoice* m_choice_year_adv1;
		wxStaticText* m_staticText84;
		wxChoice* m_choice_mes_adv1;
		wxStaticText* m_staticText49;
		wxButton* m_boton_buscar_mes;
		wxStaticLine* m_staticline19;
		wxStaticText* m_staticText48;
		wxStaticText* m_staticText451;
		wxChoice* m_choice_year_adv2;
		wxStaticText* m_staticText85;
		wxChoice* m_choice_mes_adv2;
		wxStaticText* m_staticText491;
		wxChoice* m_choice_dia_adv;
		wxStaticText* m_staticText60;
		wxButton* m_boton_buscar_dia;
		wxStaticLine* m_staticline22;
		wxStaticText* m_staticText58;
		wxStaticText* m_staticText59;
		wxChoice* m_choice_obra_social_adv;
		wxStaticText* m_staticText61;
		wxButton* m_boton_buscar_obra_social;

		// Virtual event handlers, overide them in your derived class
		virtual void OnClick_buscar_mes( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnChoice_mes_adv2( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnClick_buscar_dia( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnClick_buscar_obra_social( wxCommandEvent& event ) { event.Skip(); }


	public:

		Base_GUI_dialog_busqueda_avanzada( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxT("Búsqueda avanzada"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 732,371 ), long style = wxDEFAULT_DIALOG_STYLE );
		~Base_GUI_dialog_busqueda_avanzada();

};

