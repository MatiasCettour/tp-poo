///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Oct 26 2018)
// http://www.wxformbuilder.org/
//
// PLEASE DO *NOT* EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#pragma once

#include <wx/artprov.h>
#include <wx/xrc/xmlres.h>
#include <wx/string.h>
#include <wx/stattext.h>
#include <wx/gdicmn.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/textctrl.h>
#include <wx/sizer.h>
#include <wx/choice.h>
#include <wx/statline.h>
#include <wx/bitmap.h>
#include <wx/image.h>
#include <wx/icon.h>
#include <wx/button.h>
#include <wx/frame.h>
#include <wx/dialog.h>

///////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////
/// Class Base_GUI_login
///////////////////////////////////////////////////////////////////////////////
class Base_GUI_login : public wxFrame
{
	private:

	protected:
		wxStaticText* m_staticText27;
		wxStaticText* m_staticText32;
		wxTextCtrl* m_textCtrl_DNI;
		wxStaticText* m_staticText33;
		wxTextCtrl* m_textCtrl_pass;
		wxStaticText* m_staticText10;
		wxStaticText* m_staticText12;
		wxChoice* m_choice_rol;
		wxStaticLine* m_staticline18;
		wxButton* m_boton_ingresar;
		wxStaticText* m_staticText20;
		wxButton* m_boton_crear_cuenta;
		wxStaticText* m_staticText21;
		wxButton* m_button9;

		// Virtual event handlers, overide them in your derived class
		virtual void OnClick_ingresar( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnClick_crear( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnClick_cambiar_pass( wxCommandEvent& event ) { event.Skip(); }


	public:

		Base_GUI_login( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxT("TP-POO"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 500,331 ), long style = wxDEFAULT_FRAME_STYLE|wxTAB_TRAVERSAL );

		~Base_GUI_login();

};

///////////////////////////////////////////////////////////////////////////////
/// Class Base_GUI_sign_up
///////////////////////////////////////////////////////////////////////////////
class Base_GUI_sign_up : public wxDialog
{
	private:

	protected:
		wxStaticText* m_staticText4;
		wxTextCtrl* m_textCtrl_dni;
		wxStaticLine* m_staticline2;
		wxStaticText* m_staticText7;
		wxChoice* m_choice_rol;
		wxStaticLine* m_staticline3;
		wxStaticText* m_staticText121;
		wxStaticText* m_staticText13;
		wxTextCtrl* m_textCtrl_nombre;
		wxTextCtrl* m_textCtrl_apellido;
		wxStaticLine* m_staticline4;
		wxStaticText* m_staticText20;
		wxStaticText* m_staticText61;
		wxChoice* m_choice_year;
		wxStaticText* m_staticText71;
		wxChoice* m_choice_mes;
		wxStaticText* m_staticText8;
		wxChoice* m_choice_dia;
		wxStaticLine* m_staticline8;
		wxStaticText* m_staticText51;
		wxTextCtrl* m_textCtrl_pass1;
		wxStaticText* m_staticText6;
		wxTextCtrl* m_textCtrl_pass2;
		wxStaticText* m_staticText12;
		wxButton* m_boton_crear;

		// Virtual event handlers, overide them in your derived class
		virtual void OnChoice_year( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnChoice_mes( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnClick_crear_cuenta( wxCommandEvent& event ) { event.Skip(); }


	public:

		Base_GUI_sign_up( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxEmptyString, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 505,616 ), long style = wxDEFAULT_DIALOG_STYLE );
		~Base_GUI_sign_up();

};

///////////////////////////////////////////////////////////////////////////////
/// Class Base_GUI_newpass
///////////////////////////////////////////////////////////////////////////////
class Base_GUI_newpass : public wxDialog
{
	private:

	protected:
		wxStaticText* m_staticText37;
		wxStaticText* m_staticText38;
		wxTextCtrl* m_textCtrl_new_pass1;
		wxStaticText* m_staticText40;
		wxStaticText* m_staticText41;
		wxTextCtrl* m_textCtrl_new_pass2;
		wxButton* m_button20;
		wxStaticText* m_staticText42;
		wxButton* m_button21;

		// Virtual event handlers, overide them in your derived class
		virtual void OnClick_aceptar_newpass( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnClick_cancelar_newpass( wxCommandEvent& event ) { event.Skip(); }


	public:

		Base_GUI_newpass( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxT("Cambiar contraseña"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 530,164 ), long style = wxDEFAULT_DIALOG_STYLE );
		~Base_GUI_newpass();

};

