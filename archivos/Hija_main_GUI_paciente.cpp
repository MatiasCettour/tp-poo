#include "wx/msgdlg.h"
#include "Utils/string_conv.h"
#include "Utils/fecha.h"
#include "paciente.h"
#include "profesional.h"
#include "server.h"
#include "Hija_main_GUI_paciente.h"
#include "Hija_GUI_turnos_paciente.h"
#include "Hija_GUI_perfil_paciente.h"
#include "Hija_GUI_perfil_unProfesional.h"

Hija_main_GUI_paciente::Hija_main_GUI_paciente (Server *server, Paciente *paciente) 
	: Base_main_GUI_paciente(nullptr), m_server(server), m_paciente(paciente)
{
	//wxString ch[] = { wxT("Paciente"), wxT("Profesional") };
	m_especialidades_etiquetadas = m_server->request_especialidades_con_dni();
	m_profesionales  = m_server->request_profesionales(); 

		for (auto una_especialidad : m_especialidades_etiquetadas)
		{
			m_choice_especialidades->Append (una_especialidad.first);
		}
				m_choice_especialidades->SetSelection(0);
	//m_choice_especialidades->Append(wxT("Infectologia"));
	
	m_turnos = m_server->request_turnos(m_paciente->get_dni());
}

void Hija_main_GUI_paciente::OnChoice_especialidades( wxCommandEvent& event )
{
	m_choice_profesionales->Clear();
	m_choice_year->Clear(); 
	m_choice_mes->Clear();
	m_choice_dia->Clear();
	m_choice_hora->Clear();
	
		std::string seleccion = wx_to_std(m_choice_especialidades->GetString(m_choice_especialidades->GetSelection()));
		
			std::vector<long> dnies = m_especialidades_etiquetadas[seleccion];
		//m_choice_profesionales->Append(wxT("Rodolfo"));
		
			for (long& un_dni : dnies)
			{
				if (un_dni != m_paciente->get_dni())
				{	
					m_choice_profesionales->Append (std_to_wx( m_profesionales[un_dni].get_apellido() + " " + 
													m_profesionales[un_dni].get_nombre()   +
													". DNI: " + std::to_string(un_dni) ));
				}
			}
}

void Hija_main_GUI_paciente::OnChoice_profesionales( wxCommandEvent& event )
{
	m_choice_year->Clear(); m_choice_year->Append("Seleccione un a�o"); m_choice_year->SetSelection(0);
	m_choice_mes->Clear();
	m_choice_dia->Clear();
	m_choice_hora->Clear();

		std::string str = wx_to_std(m_choice_profesionales->GetString(m_choice_profesionales->GetSelection()));
		long dni_profesional = std::stol(str.substr(str.find("DNI: ")+5));
		
		m_profesional = &m_profesionales[dni_profesional];
		
			int year_actual = get_year_now();
			m_choice_year->Append(std::to_string(year_actual));
			m_choice_year->Append(std::to_string(year_actual+1));
}

void Hija_main_GUI_paciente::OnChoice_year( wxCommandEvent& event )
{
	m_choice_mes->Clear(); m_choice_mes->Append("Seleccione un mes"); m_choice_mes->SetSelection(0);
	m_choice_dia->Clear();
	m_choice_hora->Clear();
	
		int year_actual = get_year_now();
		
		if (m_choice_year->GetSelection() > 1 and m_choice_year->GetSelection() != 0) //si a�o que viene
		{
			for (int i = 1; i <= 12; ++i)
			{
				m_choice_mes->Append(std::to_string(i));
			}
		}
		else if (m_choice_year->GetSelection() != 0) //si mismo a�o
		{
			int mes_actual = get_mes_now();
			
			for (int i = mes_actual; i <= 12; ++i)
			{
				m_choice_mes->Append(std::to_string(i));
			}
		}
}

void Hija_main_GUI_paciente::OnChoice_mes( wxCommandEvent& event )
{
	m_choice_hora->Clear();
	m_choice_dia->Clear();
	
		if (m_choice_mes->GetSelection() > 0)
		{
		m_choice_dia->Append("Seleccione un dia"); m_choice_dia->SetSelection(0);
		
			int year_actual = get_year_now();
			int mes_actual = get_mes_now();
			int dia_actual = get_dia_now();
			
			int year_seleccionado = get_year_now();
			if (m_choice_year->GetSelection() > 1) ++year_seleccionado; //es el a�o que sigue
			
			int mes_seleccionado = std::stoi(wx_to_std((m_choice_mes->GetString(m_choice_mes->GetSelection()))));

			std::vector<int> dias_disponibles = m_server->request_dias_disponibles(m_profesional->get_dni(), mes_seleccionado, year_seleccionado);

				for (auto& un_dia : dias_disponibles)
				{
					if (year_seleccionado == year_actual and mes_seleccionado == mes_actual) 
					{
						if (un_dia > dia_actual) m_choice_dia->Append(std::to_string(un_dia));
					}
					else m_choice_dia->Append(std::to_string(un_dia)); //agrega todos los dias suponiendo que se agregaron meses futuros
				}
		}
}
void Hija_main_GUI_paciente::OnChoice_dia( wxCommandEvent& event )
{
m_choice_hora->Clear();
	
	if (m_choice_dia->GetSelection() > 0)
	{
	m_choice_hora->Clear();
			
		int year_actual = get_year_now();
		int mes_actual = get_mes_now();
		int dia_actual = get_dia_now();
		int hora_actual = get_hora_now();
		
			int year_seleccionado = get_year_now();
			if (m_choice_year->GetSelection() > 1) ++year_seleccionado; //es el a�o que sigue
			
			int mes_seleccionado = std::stoi(wx_to_std((m_choice_mes->GetString(m_choice_mes->GetSelection()))));
			
			int dia_seleccionado = std::stoi(wx_to_std( m_choice_dia->GetString(m_choice_dia->GetSelection())));
			
			int year_aux = year_seleccionado;
			int t[] = { 0, 3, 2, 5, 0, 3, 5, 1, 4, 6, 2, 4 }; 
			year_aux -= mes_seleccionado < 3; 
			int index_dia = ( year_aux + year_aux/4 - year_aux/100 + year_aux/400 + t[mes_seleccionado-1] + dia_seleccionado) % 7; 

				std::vector<int> horas = m_profesional->get_horarios(index_dia); 
				
				for (auto& una_hora : horas)
				{
					if (year_seleccionado == year_actual and mes_seleccionado == mes_actual and dia_seleccionado == dia_actual) 
					{
						if(una_hora >= hora_actual + 2) m_choice_hora->Append(std::to_string(una_hora)+"hs"); //umbral de dos horas antes
					}
					else m_choice_hora->Append(std::to_string(una_hora)+"hs"); //todas horas futuras
				}
	}
}

void Hija_main_GUI_paciente::OnClick_ver_perfil_profesional ( wxCommandEvent& event )
{
	if (m_profesional == nullptr) wxMessageBox("Seleccione un profesional");
	
	else
	{
		Hija_GUI_perfil_unProfesional win_perfil(this, m_profesional);
		win_perfil.ShowModal();
	}
}

void Hija_main_GUI_paciente::OnClick_reservar( wxCommandEvent& event )
{
	if (m_choice_mes->GetSelection() > 0 and m_choice_dia->GetSelection() > 0 and m_choice_hora->GetSelection() > -1)
	{
		int year = get_year_now();
		if (m_choice_year->GetSelection() > 1) ++year; //es el a�o que sigue
		int mes = std::stoi(wx_to_std((m_choice_mes->GetString(m_choice_mes->GetSelection()))));
		int dia = std::stoi(wx_to_std( m_choice_dia->GetString(m_choice_dia->GetSelection())));

		std::string str = wx_to_std(m_choice_hora->GetString(m_choice_hora->GetSelection()));
		int hora = std::stoi(str.substr(0, str.find("hs")));
			
			if (m_server->check_turno(m_paciente->get_dni(), m_profesional->get_dni(), hora, dia, mes, year) == true)
			{
				m_server->add_turno(m_paciente->get_dni(), m_profesional->get_dni(), hora, dia, mes, year);
				
					wxMessageBox("Turno reservado");
				
						m_choice_especialidades->SetSelection(0);
						m_choice_profesionales->Clear();
						m_choice_year->Clear();
						m_choice_mes->Clear();
						m_choice_dia->Clear();
						m_choice_hora->Clear();
			}
			else wxMessageBox("Turno no disponible");
	}
	else wxMessageBox("Por favor, seleccione las opciones faltantes");
}

void Hija_main_GUI_paciente::OnClick_ver_turnos( wxCommandEvent& event )
{
	Hija_GUI_turnos_paciente win_turnos(this, m_server, m_paciente, m_turnos);
	win_turnos.ShowModal();
}
void Hija_main_GUI_paciente::OnClick_configurar_perfil( wxCommandEvent& event )
{
	Hija_GUI_perfil_paciente win_conf(this, m_server, m_paciente);
	win_conf.ShowModal();
}
void Hija_main_GUI_paciente::OnClick_cerrar_sesion( wxCommandEvent& event )
{
	Close();
}

Hija_main_GUI_paciente::~Hija_main_GUI_paciente()
{
	delete m_paciente;
}
