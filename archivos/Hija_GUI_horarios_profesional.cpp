#include <wx/msgdlg.h>
#include "Utils/string_conv.h"
#include "Hija_GUI_horarios_profesional.h"
#include "Hija_GUI_dialog_dias_libres.h"

Hija_GUI_horarios_profesional::Hija_GUI_horarios_profesional(wxWindow* parent, Server* server, Profesional* profesional)
	: Base_GUI_horarios_profesional(parent), m_server(server), m_profesional(profesional)
{
	refresh();
}

void Hija_GUI_horarios_profesional::OnButton_agregar_dia_no_laboral ( wxCommandEvent& event )
{
	Hija_GUI_dialog_dias_libres win_dias_libres(this, m_server, m_profesional);
	win_dias_libres.ShowModal();
	refresh();
}

void Hija_GUI_horarios_profesional::OnClick_guardar ( wxCommandEvent& event )
{
	if (m_choice_dia_semana->GetSelection() == 0)
		wxMessageBox("Seleccione un dia");
	else
	{
		int index_dia = m_choice_dia_semana->GetSelection();  // 0: domingo, 1: lunes ...
		
		if (m_choice_horas1->GetSelection() != -1 and m_choice_horas2->GetSelection() != -1)
		{
			int hora_inicial = std::stoi( wx_to_std( m_choice_horas1->GetString (m_choice_horas1->GetSelection())) );
			int hora_final   = std::stoi( wx_to_std( m_choice_horas2->GetString (m_choice_horas2->GetSelection())) );
			
				std::vector<int> horas;
				for (int h = hora_inicial; h <= hora_final; ++h)
					horas.push_back(h);
			
				m_profesional->add_horario (index_dia, horas);
				
					wxMessageBox("Horario agregado");
						//m_choice_dia_semana->SetSelection(1);
						refresh();
		}
		else wxMessageBox("Seleccione un horario");
	}
}

void Hija_GUI_horarios_profesional::OnChoice_horas1 ( wxCommandEvent& event )
{
	int hora_inicial = std::stoi( wx_to_std( m_choice_horas1->GetString (m_choice_horas1->GetSelection())) );
	
	m_choice_horas2->Clear();
	for (int una_hora = hora_inicial+1; una_hora <= 21; ++una_hora)
	{
		m_choice_horas2->Append(std::to_string(una_hora));
	}
}

void Hija_GUI_horarios_profesional::OnClick_eliminar_dia ( wxCommandEvent& event )
{
	int seleccion = m_choice_dias_libres->GetSelection();
	if (seleccion > -1) 
	{
		m_profesional->eliminar_dia_no_laboral(seleccion);
		wxMessageBox("Dia eliminado");
		this->refresh();
	}
	else wxMessageBox("Seleccione una fecha");
}

void Hija_GUI_horarios_profesional::OnClick_cerrar	( wxCommandEvent& event )
{
	EndModal(0);
}

void Hija_GUI_horarios_profesional::refresh()
{
	m_choice_horas1->Clear();
	m_choice_horas2->Clear();
	m_choice_dias_libres->Clear();
	
		for (int una_hora = 6; una_hora <= 20; ++una_hora)
		{
			m_choice_horas1->Append(std::to_string(una_hora));
		}
		
		std::vector<Dia_libre> dias_libres = m_profesional->get_dias_no_laborales();
		
		for (auto& dia : dias_libres)
		{
			m_choice_dias_libres->Append(std::to_string(dia.dia) +"/"+ std::to_string(dia.mes));
		}
	
		m_grilla_horarios->SetRowLabelValue(0,"Horas");
		
		m_grilla_horarios->SetColLabelValue(0, "Domingo");
		m_grilla_horarios->SetColLabelValue(1, "Lunes");
		m_grilla_horarios->SetColLabelValue(2, "Martes");
		m_grilla_horarios->SetColLabelValue(3, _T("Mi\u00E9rcoles"));
		m_grilla_horarios->SetColLabelValue(4, "Jueves");
		m_grilla_horarios->SetColLabelValue(5, "Viernes");
		m_grilla_horarios->SetColLabelValue(6, _T("S\u00E1bado"));
			 
		std::vector<std::vector<int>> horarios = m_profesional->get_horarios();
		
		for (size_t i = 0; i < horarios.size(); ++i)
		{
			if (horarios[i].size() == 1) m_grilla_horarios->SetCellValue(0,i, std_to_wx(std::to_string( horarios[i][0])+" hs"));
			else if (horarios[i].size() > 1) m_grilla_horarios->SetCellValue(0,i, "De " + std_to_wx(std::to_string(horarios[i][0]) +" hs a " + std::to_string(*(horarios[i].end()-1)) +" hs"));                                 
		}
}









