#ifndef HIJA_GUI_PROFESIONAL_H
#define HIJA_GUI_PROFESIONAL_H
#include "Bases_GUI/GUI_profesional.h"

class Server;
class Profesional;

class Hija_main_GUI_profesional : public Base_main_GUI_profesional 
{
public:
	Hija_main_GUI_profesional ( Server* server, Profesional *profesional );
	
	void OnClick_cancelar_turno    ( wxCommandEvent& event );
	void OnClick_confirmar_turno   ( wxCommandEvent& event );
	void OnClick_cerrar_sesion     ( wxCommandEvent& event );
	void OnClick_config_perfil     ( wxCommandEvent& event );
	void OnClick_config_horarios   ( wxCommandEvent& event );
	void OnClick_buscar_paciente   ( wxCommandEvent& event );
	void OnClick_busqueda_avanzada ( wxCommandEvent& event );
	
	~Hija_main_GUI_profesional();
	
private:
	void refresh();
	Server*  	 m_server      = nullptr;
	Profesional* m_profesional = nullptr;
	std::vector<Dia_libre> 					 m_dias_no_laborales;
	std::vector<Turno>                       m_turnos;
};

#endif

